package services;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Actor;
import domain.Ranger;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/datasource.xml",
		"classpath:spring/config/packages.xml" })
@Transactional
public class RangerServiceTest extends AbstractTest {
	@Autowired
	private RangerService rangerService;

	@Autowired
	private ActorService actorService;

	@Test
	public void testSaveNewRanger() {
		Ranger ranger, result;
		UserAccount userAccount;
		List<Actor> actors;
		Authority auth;
		List<Authority> auths;

		auth = new Authority();
		userAccount = new UserAccount();
		auths = new ArrayList<Authority>();
		auth.setAuthority(Authority.RANGER);
		auths.add(auth);
		userAccount.setAuthorities(auths);
		userAccount.setUsername("rangerTest");
		userAccount.setPassword("rangerTest");
		ranger = rangerService.create();
		ranger.setUserAccount(userAccount);
		ranger.setName("RangerTest");
		ranger.setSurname("RangerTest");
		ranger.setEmail("rangertest@email.com");

		result = rangerService.save(ranger);
		actors = new ArrayList<Actor>(actorService.findAll());
		Assert.isTrue(actors.contains(result));
		Assert.isTrue(result.getFolders().size() == 5);
	}

	@Test
	public void testSaveExistingRanger(){
			Ranger ranger, result;
			List<Actor> actors;
			List<Ranger> rangers;
			
			rangers = (List<Ranger>) rangerService.findAll();
			ranger = rangers.get(0);
			
			super.authenticate(ranger.getUserAccount().getUsername());
			
			
			ranger.setAddress("Calle prueba");
			ranger.setName("tesRangerNombre");
			ranger.setSurname("tesRangerApellido");
			
			result= rangerService.save(ranger);
			
			actors = new ArrayList<Actor>(actorService.findAll());
			Assert.isTrue(actors.contains(result));
			Assert.isTrue(result.getAddress().equals("Calle prueba"));
			Assert.isTrue(result.getName().equals("tesRangerNombre"));
			
			super.authenticate(null);
	}
	

	@Test(expected = IllegalArgumentException.class)	
	public void testSaveExistingRangerBadAuth(){
			Ranger ranger, result;
			List<Actor> actors;
			List<Ranger> rangers;
			
			rangers = (List<Ranger>) rangerService.findAll();
			ranger = rangers.get(0);
			
			super.authenticate("manager1");
			
			
			ranger.setAddress("Calle prueba");
			ranger.setName("tesRangerNombre");
			ranger.setSurname("tesRangerApellido");
			
			result= rangerService.save(ranger);
			
			actors = new ArrayList<Actor>(actorService.findAll());
			Assert.isTrue(actors.contains(result));
			Assert.isTrue(result.getAddress().equals("Calle prueba"));
			Assert.isTrue(result.getName().equals("tesRangerNombre"));
			
			super.authenticate(null);
	}


	@Test
	public void testFindOneRanger() {
		Ranger ranger;
		List<Ranger> rangers;

		rangers = (List<Ranger>) rangerService.findAll();
		ranger = rangers.get(0);

		ranger = rangerService.findOne(ranger.getId());
		Assert.isTrue(rangers.contains(ranger));

	}

	@Test
	public void testFindAll() {
		List<Ranger> rangers;

		rangers = (List<Ranger>) rangerService.findAll();
		Assert.isTrue(rangers.size() == 5);
	}

}
