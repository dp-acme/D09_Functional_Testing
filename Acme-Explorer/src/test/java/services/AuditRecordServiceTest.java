package services;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.LoginService;
import utilities.AbstractTest;
import domain.AuditRecord;
import domain.Auditor;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class AuditRecordServiceTest extends AbstractTest{

	@Autowired 
	private AuditRecordService auditRecordService;
	
	@Autowired 
	private TripService tripService;
	
	@Autowired 
	private AuditorService auditorService;
	
	@Autowired 
	private ActorService actorService;
	
	@Test
	public void saveAuditRecordTestAuth(){
		List<Trip> trips;
		List<Auditor> auditors;
		Trip trip;
		Auditor auditor;
		AuditRecord result;
		
		this.authenticate("auditor1");
		trips = (List<Trip>) tripService.findAll();
		auditors = (List<Auditor>) auditorService.findAll();
		
		trip = trips.get(0);
		auditor = auditors.get(0);

		result = auditRecordService.create(auditor, trip);
		
		result.setTitle("Title...");
		result.setDescription("Description...");
		
		result = auditRecordService.save(result);
		
		Assert.isTrue(auditRecordService.findAll().contains(result));
		Assert.isTrue(auditorService.findOne(auditor.getId()).getAuditRecords().contains(result));
	}
	
	@Test
	public void editAuditRecordTestAuth(){
		Auditor auditor;
		AuditRecord result;
		
		this.authenticate("auditor1");
		
		auditor = (Auditor) actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		result = null;
		
		for(AuditRecord record : auditor.getAuditRecords()){
			if(record.isDraft()){
				result = record;
				break;
			}
		}

		
		result.setTitle("Title...");
		result.setDescription("Description...");
		
		result = auditRecordService.save(result);
		
		Assert.isTrue(auditRecordService.findAll().contains(result));
		Assert.isTrue(auditorService.findOne(auditor.getId()).getAuditRecords().contains(result));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void editAuditRecordTestBadAuth(){
		AuditRecord result;
		List<AuditRecord> records;
		
		this.authenticate("auditor2");
		
		records = (List<AuditRecord>) auditRecordService.findAll();
		
		result = null;
		
		for(AuditRecord record: records){
			if((!record.getAuditor().getUserAccount().equals(LoginService.getPrincipal())) && record.isDraft()){
				result = record; 
				break;
			}
		}
				
		result.setTitle("Title...");
		result.setDescription("Description...");
		
		result = auditRecordService.save(result);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void saveAuditRecordTestBadAuth(){
		List<Trip> trips;
		List<Auditor> auditors;
		Trip trip;
		Auditor auditor;
		AuditRecord result;
		
		this.authenticate("admin");
		trips = (List<Trip>) tripService.findAll();
		auditors = (List<Auditor>) auditorService.findAll();
		trip = trips.get(0);
		auditor = auditors.get(0);
	
		result = auditRecordService.create(auditor, trip);
		
		result.setTitle("Title...");
		result.setDescription("Description...");
		
		result = auditRecordService.save(result);
		
		Assert.isTrue(auditRecordService.findAll().contains(result));
		Assert.isTrue(auditorService.findOne(auditor.getId()).getAuditRecords().contains(result));
	}
	
	@Test
	public void deleteAuditRecordTestDraft(){
		List<AuditRecord> auditRecords;
		AuditRecord result;
		Auditor auditor;
		
		this.authenticate("auditor1");

		auditRecords = (List<AuditRecord>) auditRecordService.findAll();
		Assert.notEmpty(auditRecords);
		result = auditRecords.get(1); //Es draft
		auditor = result.getAuditor();
		
		auditRecordService.delete(result);
		
		Assert.isTrue(!auditRecordService.findAll().contains(result));
		Assert.isTrue(!auditorService.findOne(auditor.getId()).getAuditRecords().contains(result));
		
	}
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void deleteAuditRecordTestNonDraft(){
		List<AuditRecord> auditRecords;
		AuditRecord result;
		Auditor auditor;
		
		this.authenticate("auditor1");

		auditRecords = (List<AuditRecord>) auditRecordService.findAll();
		Assert.notEmpty(auditRecords);
		result = auditRecords.get(0); //No es draft, no se puede borrar
		auditor = result.getAuditor();
		
		auditRecordService.delete(result);
		
		Assert.isTrue(!auditRecordService.findAll().contains(result));
		Assert.isTrue(!auditorService.findOne(auditor.getId()).getAuditRecords().contains(result));
	}
	
	@Test
	public void findAllAuditRecordTest(){
		List<AuditRecord> auditRecords;
		
		auditRecords = (List<AuditRecord>) auditRecordService.findAll();

		Assert.notNull(auditRecords);
		
	}
	
	@Test
	public void findOneAuditRecordTest(){
		List<AuditRecord> auditRecords;
		AuditRecord record;
		
		auditRecords = (List<AuditRecord>) auditRecordService.findAll();
		Assert.notNull(auditRecords);
		record = auditRecords.get(0);

		Assert.isTrue(auditRecordService.findAll().contains(auditRecordService.findOne(record.getId())));
	}
	
	
}
