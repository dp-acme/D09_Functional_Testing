package services;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.LoginService;
import utilities.AbstractTest;
import domain.Curriculum;
import domain.Education;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class EducationServiceTest extends AbstractTest{
	
	@Autowired 
	private EducationService educationService;
	
	@Autowired 
	private CurriculumService curriculumService;
	

	@Test
	public void saveNewEducationRecordAuth(){
		List<Curriculum> curriculums;
		Curriculum curriculum;
		Education result;
		Date startDate;
		
		startDate = new Date(System.currentTimeMillis() - 1);
		this.authenticate("ranger1");
		curriculums = (List<Curriculum>) curriculumService.findAll();
		curriculum = null;
		for(Curriculum c : curriculums){
			if(c.getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				curriculum = c;
			}
		}
		
		result = educationService.create(curriculum);
		
		result.setTitle("Title...");
		result.setInstitution("Institution...");
		result.setStudiesStart(startDate);
		
		result = educationService.save(result);
		
		Assert.isTrue(educationService.findAll().contains(result));
		Assert.isTrue(curriculumService.findOne(curriculum.getId()).getEducationRecords().contains(result));
		
		this.authenticate(null);

	}
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void saveNewEducationRecordBadAuth1(){
		List<Curriculum> curriculums;
		Curriculum curriculum;
		Education result;
		Date startDate;
		
		startDate = new Date(System.currentTimeMillis() - 1);
		this.authenticate("ranger1");
		curriculums = (List<Curriculum>) curriculumService.findAll();
		curriculum = null;
		
		for(Curriculum c : curriculums){
			if(!c.getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				curriculum = c;
			}
		}
		
		result = educationService.create(curriculum);
		result.setTitle("Title...");
		result.setInstitution("Institution...");
		result.setStudiesStart(startDate);
		
		result = educationService.save(result);
		
		Assert.isTrue(educationService.findAll().contains(result));
		Assert.isTrue(curriculumService.findOne(curriculum.getId()).getEducationRecords().contains(result));
		
		this.authenticate(null);
	}
	
	@SuppressWarnings("unused")
	@Test
	public void editEducationRecordAuth(){
		List<Curriculum> curriculums;
		List<Education> educations;
		Curriculum curriculum;
		Education result;
	
		this.authenticate("ranger1");
		curriculums = (List<Curriculum>) curriculumService.findAll();
		educations = (List<Education>) educationService.findAll();
		
		curriculum = null;
		for(Curriculum c : curriculums){
			if(c.getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				curriculum = c;
			}
		}
		
		result = curriculum.getEducationRecords().iterator().next(); //Pertenece a ranger1
		
		result.setTitle("Edited title...");
		
		result = educationService.save(result);
		
		Assert.isTrue(educationService.findAll().contains(result));
		Assert.isTrue(curriculumService.findOne(curriculum.getId()).getEducationRecords().contains(result));
		
		this.authenticate(null);
	}
	
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void editEducationRecordBadAuth(){
		List<Education> educations;
		Education result;
	
		this.authenticate("ranger1");
		educations = (List<Education>) educationService.findAll();
		
		result  = null;
		for(Education e : educations){
			if(!e.getCurriculum().getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				result = e;
			}
		}
				
		result = educationService.save(result);
		
		this.authenticate(null);
	}
	
	
	@Test
	public void deleteEducationRecordAuth(){
		List<Curriculum> curriculums;
		Curriculum curriculum;
		Education result;
		
		this.authenticate("ranger1");
		curriculums = (List<Curriculum>) curriculumService.findAll();
		
		curriculum = null;
		for(Curriculum c : curriculums){
			if(c.getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				curriculum = c;
			}
		}
		
		result = curriculum.getEducationRecords().iterator().next();
		
		educationService.delete(result);
		
		Assert.isTrue(!educationService.findAll().contains(result));
		Assert.isTrue(!curriculumService.findOne(curriculum.getId()).getEducationRecords().contains(result));
		
		this.authenticate(null);
	}
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void deleteEducationRecordBadAuth(){
		List<Education> educations;
		Education result;
		
		this.authenticate("ranger1");
		educations = (List<Education>) educationService.findAll();
		result  = null;
		
		for(Education e : educations){
			if(!e.getCurriculum().getRanger().getUserAccount().equals(LoginService.getPrincipal())){
				result = e;
			}
		}
		
		educationService.delete(result);
		
		this.authenticate(null);
	}
		
	@Test
	public void findAllEducationRecordTest(){
		List<Education> auditRecords;
		
		auditRecords = (List<Education>) educationService.findAll();

		Assert.notNull(auditRecords);
		
	}
	
	@Test
	public void findOneEducationRecordTest(){
		List<Education> educations;
		Education education;
		
		educations = (List<Education>) educationService.findAll();
		Assert.notNull(educations);
		education = educations.get(0);

		Assert.isTrue(educationService.findAll().contains(educationService.findOne(education.getId())));
	}
	

}
