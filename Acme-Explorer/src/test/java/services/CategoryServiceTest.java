package services;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Category;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/datasource.xml",
		"classpath:spring/config/packages.xml" })
@Transactional
public class CategoryServiceTest extends AbstractTest {

	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private TripService tripService;
	
	@Test
	public void testSaveNewCategory(){
		Category category, result, parent;
		
 		super.authenticate("admin");
		category = categoryService.create();
		category.setName("CarpetaTest1");
		parent = categoryService.findRootcategory();
		
		result = categoryService.addChildCategory(parent, category);
				
		Assert.isTrue(result.getChildrenCategories().size()==0);
		Assert.isTrue(categoryService.findAll().contains(result));
		
		super.authenticate(null);
	}
	
	@Test
	public void testFindOneCategory(){
		Category category, result;
		List<Category> categories;

		categories = (List<Category>)categoryService.findAll();
		category = categories.get(1);
		result = categoryService.findOne(category.getId());
		Assert.isTrue(categoryService.findAll().contains(result));
	}
	
	@Test
	public void testFindAllCategories(){
		List<Category> categories;
		categories = (List<Category>)categoryService.findAll();
		Assert.isTrue(categories.size()==11);
	}
	
	@Test
	public void testSaveExistingCategory(){
		List<Trip> trips;
		Trip trip;
		Category category, result;
		
		trips = (List<Trip>)tripService.findAll();
		trip = trips.get(1);
		
		super.authenticate("admin");
		category= trip.getCategory();
		category.setName("testName1");
		result = categoryService.save(category);
		Assert.isTrue(trip.getCategory().getName().equals(result.getName()));
		Assert.isTrue(categoryService.findAll().contains(result));
		super.authenticate(null);
	}
	
	@Test
	public void testDeleteCategory(){
		List<Trip> trips;

		Trip trip;
		Category category;
		
		trips = (List<Trip>)tripService.findAll();
		trip = trips.get(1);
		
		super.authenticate("admin");
		category = trip.getCategory();
		Assert.isTrue(categoryService.findAll().contains(category));

		categoryService.delete(category);
		
		Assert.isTrue(!categoryService.findAll().contains(category));
		
		super.authenticate(null);
	}
}
