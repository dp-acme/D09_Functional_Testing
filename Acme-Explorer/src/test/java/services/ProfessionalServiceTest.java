
package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Curriculum;
import domain.Professional;
import domain.Ranger;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class ProfessionalServiceTest extends AbstractTest {

	//Service under test
	@Autowired
	private ProfessionalService	professionalService;

	// Servicios usados

	@Autowired
	private RangerService		rangerService;

	@Autowired
	private CurriculumService	curriculumService;


	@Test
	public void testCreate() {
		Professional professional;
		List<Ranger> rangers;
		List<Curriculum> curriculums;
		Curriculum curriculum;
		Ranger ranger;
		// Logeamos un ranger
		this.authenticate("ranger1");

		rangers = (List<Ranger>) this.rangerService.findAll();
		ranger = rangers.get(0);

		Assert.isTrue(ranger.getUserAccount().getUsername().equals("ranger1"));

		curriculums = (List<Curriculum>) curriculumService.findAll();
		curriculum = curriculums.get(0);
		curriculum.setRanger(ranger);// le paso el ranger por que tiene que estar logeado

		// hemos cogido el curriculum que tenia asociado el ranger 
		professional = professionalService.create(curriculum);

		professional.setCurriculum(curriculum);
	}

	@Test
	public void testDelete() {
		List<Professional> listProfessionals;

		listProfessionals = (List<Professional>) this.professionalService.findAll();
		this.professionalService.delete(listProfessionals.get(0));
	}

	@Test
	public void testFindOne() {
		// Meto todos los professional en una lista:
		List<Professional> professionals;
		professionals = (List<Professional>) this.professionalService.findAll();
		this.professionalService.findOne(professionals.get(0).getId());
	}

	@Test
	public void testFindAll() {
		Collection<Professional> result;

		result = this.professionalService.findAll();

		Assert.notNull(result);
	}

	@SuppressWarnings("deprecation")
	@Test
	//@Test(expected = IllegalArgumentException.class)
	public void testSave() {
		// le metemos una inicializacion de todos los atributos
		Professional professional, saved;
		Collection<Professional> professionals;
		Ranger ranger;
		List<Ranger> rangers;
		Curriculum curriculum;
		List<Curriculum> curriculums;

		// Logeamos un ranger
		this.authenticate("ranger1");
		rangers = (List<Ranger>) this.rangerService.findAll();

		ranger = rangers.get(0);
		Assert.isTrue(ranger.getUserAccount().getUsername().equals("ranger1"));

		curriculums = (List<Curriculum>) this.curriculumService.findAll();
		curriculum = curriculums.get(0);
		curriculum.setRanger(ranger);// le paso el ranger por que tiene que estar logeado

		// hemos cogido el curriculum que tenia asociado el ranger 
		professional = this.professionalService.create(curriculum);

		//seteamos los atributos

		professional.setCompanyName("Endesa");

		professional.setProfessionalStart(new Date("10/10/2016 16:40")); // Es una fecha que es anterior a la fecha actual y tambien anterior a la end
		professional.setProfessionalEnd(new Date("10/11/2016 16:40"));

		professional.setRole("rol");
		professional.setAttachment("https://www.google.es/");
		professional.setComments("Estoy probando");

		saved = this.professionalService.save(professional);
		professionals = this.professionalService.findAll();

		Assert.isTrue(professionals.contains(saved));
	}
}
