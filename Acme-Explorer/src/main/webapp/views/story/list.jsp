<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<display:table name="stories" id="row" pagesize="5">

	<spring:message code="story.author" var="autor" />
	<display:column property="explorer.name" title="${autor}"
		sortable="false" />

	<spring:message code="story.title" var="titulo" />
	<display:column property="title" title="${titulo}" sortable="false" />

	<spring:message code="story.text" var="text" />
	<display:column property="text" title="${text}" sortable="false" />

	<spring:message code="story.attachments" var="attachments" />
	<display:column property="attachments" title="${attachments}" sortable="false" />


	<security:authorize access="hasRole('EXPLORER')">
		<jstl:choose>
			<jstl:when test="${myStories == true}">
				<display:column title="" sortable="false">
					<a href="story/explorer/edit.do?storyId=${row.id}"> <spring:message
							code="editText" />
					</a>
				</display:column>
			</jstl:when>
		</jstl:choose>
	</security:authorize>


</display:table>

<input type="button" value="<spring:message code="backText" />"
	onClick="javascript: relativeRedir('trip/list.do');" />
