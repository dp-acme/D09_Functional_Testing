<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<jsp:useBean id="time" class="java.util.Date" />

<jstl:set var="uri" value="legalText/list.do" />


<display:table name="legalTexts" id="row" requestURI="${uri}"
	pagesize="5">

	<spring:message code="legalText.list.timeHeader" var="timeHeader" />
	<display:column title="${timeHeader}" sortable="true">
		<spring:message code="legalText.formatDate" var="formatDate" />
		<fmt:formatDate value="${row.getMoment()}" pattern="${formatDate}" />
	</display:column>

	<spring:message code="legalText.list.titleHeader" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />

	<spring:message code="legalText.list.bodyHeader"
		var="bodyHeader" />
	<display:column property="body" title="${bodyHeader}"
		sortable="false" />

	<spring:message code="legalText.list.lawsHeader"
		var="lawsHeader" />
	<display:column property="laws" title="${lawsHeader}"
		sortable="false" />

	<security:authorize access="hasRole('ADMINISTRATOR')">
		<spring:message code="legalText.list.editHeader" var="editHeader" />
		<display:column title="" sortable="false">
			<jstl:if test="${!row.isFinalMode()}">
				<spring:url
					value="legalText/administrator/edit.do?legalTextId=${row.getId()}"
					var="editLink" />
				<a href="${editLink}"> 
					<jstl:out value="${editHeader}" />
				</a>
			</jstl:if>
		</display:column>
	</security:authorize>
</display:table>

<spring:message code="legalText.list.back" var="listBack" />
<input type="button" value="${listBack}"
	onClick="javascript:relativeRedir('trip/list.do');" />
	
<jstl:if test="${empty param.tripId}">
<security:authorize access="hasRole('ADMINISTRATOR')">
	<spring:message code="legalText.list.create" var="listCreate" />
	<input type="button" value="${listCreate}"
		onClick="javascript: relativeRedir('legalText/administrator/create.do');" />
</security:authorize>
	</jstl:if>



