<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ page import="java.text.DecimalFormat" %>

<h2><spring:message code="dashboard.statistics" /></h2>

<ul>
	<li>	
		<b><spring:message code="dashboard.ratioCancelledTrips" />:</b>
		<jstl:out value="${ratioOfCancelledTrips}" />
	</li>
	
	<li>	
		<b><spring:message code="dashboard.ratioOfTripsWithAuditRecords" />:</b>
		<jstl:out value="${ratioOfTripsWithAuditRecords}" />
	</li>
	
	<li>	
		<b><spring:message code="dashboard.ratioOfRangersWithCurriculum" />:</b>
		<jstl:out value="${ratioOfRangersWithCurriculum}" />
	</li>
	
	<li>	
		<b><spring:message code="dashboard.ratioOfRangersWithEndorsedCurriculum" />:</b>
		<jstl:out value="${ratioOfRangersWithEndorsedCurriculum}" />
	</li>
	
	<li>	
		<b><spring:message code="dashboard.ratioOfSuspiciousManagers" />:</b>
		<jstl:out value="${ratioOfSuspiciousManagers}" />
	</li>
	
	<li>	
		<b><spring:message code="dashboard.ratioOfSuspiciousRangers" />:</b>
		<jstl:out value="${ratioOfSuspiciousRangers}" />
	</li>
</ul>

<table> 
	<tr style="background-color: yellow;">
		<th><spring:message code="dashboard.table.property" /></th>
		<th><spring:message code="dashboard.table.average" /></th>
		<th><spring:message code="dashboard.table.maximum" /></th>
		<th><spring:message code="dashboard.table.minimum" /></th>
		<th><spring:message code="dashboard.table.sd" /></th>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.applicationOfTrips" /></td>
		<td><jstl:out value="${avgMaxMinSdApplicationOfTrips[0]}" /></td>
		<td><jstl:out value="${avgMaxMinSdApplicationOfTrips[1]}" /></td>
		<td><jstl:out value="${avgMaxMinSdApplicationOfTrips[2]}" /></td>
		<td><jstl:out value="${avgMaxMinSdApplicationOfTrips[3]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.managersOfTrips" /></td>
		<td><jstl:out value="${avgMaxMinSdManagersOfTrips[0]}" /></td>
		<td><jstl:out value="${avgMaxMinSdManagersOfTrips[1]}" /></td>
		<td><jstl:out value="${avgMaxMinSdManagersOfTrips[2]}" /></td>
		<td><jstl:out value="${avgMaxMinSdManagersOfTrips[3]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.priceOfTrips" /></td>
		<td><jstl:out value="${avgMaxMinSdPriceOfTrips[0]}" /></td>
		<td><jstl:out value="${avgMaxMinSdPriceOfTrips[1]}" /></td>
		<td><jstl:out value="${avgMaxMinSdPriceOfTrips[2]}" /></td>
		<td><jstl:out value="${avgMaxMinSdPriceOfTrips[3]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.rangersOfTrips" /></td>
		<td><jstl:out value="${avgMaxMinSdRangersOfTrips[0]}" /></td>
		<td><jstl:out value="${avgMaxMinSdRangersOfTrips[1]}" /></td>
		<td><jstl:out value="${avgMaxMinSdRangersOfTrips[2]}" /></td>
		<td><jstl:out value="${avgMaxMinSdRangersOfTrips[3]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.auditRecordsOfTrips" /></td>
		<td><jstl:out value="${avgMaxMinSdAuditRecordsOfTrips[0]}" /></td>
		<td><jstl:out value="${avgMaxMinSdAuditRecordsOfTrips[1]}" /></td>
		<td><jstl:out value="${avgMaxMinSdAuditRecordsOfTrips[2]}" /></td>
		<td><jstl:out value="${avgMaxMinSdAuditRecordsOfTrips[3]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.notesOfTrips" /></td>
		<td><jstl:out value="${avgMaxMinSdNotesOfTrips[0]}" /></td>
		<td><jstl:out value="${avgMaxMinSdNotesOfTrips[1]}" /></td>
		<td><jstl:out value="${avgMaxMinSdNotesOfTrips[2]}" /></td>
		<td><jstl:out value="${avgMaxMinSdNotesOfTrips[3]}" /></td>
	</tr>
</table>

<table> 
	<tr>
		<th colspan="4" style="text-align: center; background-color: yellow;"><spring:message code="dashboard.table.ratioApplications" /></th>
	</tr>
	
	<tr>
		<th><spring:message code="dashboard.table.pending" /></th>
		<th><spring:message code="dashboard.table.due" /></th>
		<th><spring:message code="dashboard.table.accepted" /></th>
		<th><spring:message code="dashboard.table.cancelled" /></th>
	</tr>
	
	<tr>
		<td><jstl:out value="${ratioOfPendingApplications}" /></td>
		<td><jstl:out value="${ratioOfDueApplications}" /></td>
		<td><jstl:out value="${ratioOfAcceptedApplications}" /></td>
		<td><jstl:out value="${ratioOfCancelledApplications}" /></td>
	</tr>
</table>

<br>
<h2><spring:message code="dashboard.tripsOver10PercentOverAvgApplications" /></h2>

<spring:message code="dashboard.table.trip.ticker" var="tickerHeader" />
<spring:message code="dashboard.table.trip.title" var="titleHeader" />
<spring:message code="dashboard.table.trip.description" var="DescriptionHeader" />
<spring:message code="dashboard.table.trip.publicationDate" var="publicationDateHeader" />

<display:table id="row" name="tripsOver10PercentOverAvgApplications" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="ticker" title="${tickerHeader}" sortable="true" />
	<display:column property="title" title="${titleHeader}" sortable="true" />
	<display:column property="description" title="${DescriptionHeader}" sortable="true" />
	<display:column title="${publicationDateHeader}" sortable="true" > 
		<spring:message code="dashboard.trip.dateformat" var = "datePattern" />
		<fmt:formatDate value="${row.getPublicationDate()}" pattern="${datePattern}"/>
	</display:column>
</display:table>

<br>
<h2><spring:message code="dashboard.legalTextNumberOfTrips" /></h2>

<spring:message code="dashboard.table.legaltext.title" var="titleHeader" />
<spring:message code="dashboard.table.legaltext.moment" var="momentHeader" />
<spring:message code="dashboard.table.legaltext.references" var="referencesHeader" />

<display:table id="row" name="legalTextNumberOfTrips" requestURI="dashboard/display.do" pagesize="5">
	<display:column title="${titleHeader}" sortable="true" >
		<jstl:out value="${row[0].getTitle()}" />		
	</display:column>
	
	<display:column title="${momentHeader}" sortable="true" >
		<spring:message code="dashboard.legaltext.dateformat" var = "datePattern" />
		<fmt:formatDate value="${row[0].getMoment()}" pattern="${datePattern}"/>
	</display:column>
	
	<display:column title="${referencesHeader}" sortable="true" >
		<jstl:out value="${row[1]}" />		
	</display:column>
</display:table>
