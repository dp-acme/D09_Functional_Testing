<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<form:form action="socialIdentity/create.do" modelAttribute="socialIdentity">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="actor" />
	
	<form:label path="nick">
		<spring:message code="socialIdentity.edit.nick" />
	</form:label>
	<form:input path="nick" />
	<form:errors cssClass="error" path="nick" /><br>

	<form:label path="netName">
		<spring:message code="socialIdentity.edit.netName" />
	</form:label>
	<form:input path="netName" />
	<form:errors cssClass="error" path="netName" /><br>

	<form:label path="link">
		<spring:message code="socialIdentity.edit.link" />
	</form:label>
	<form:input path="link" />
	<form:errors cssClass="error" path="link" /><br>

	<form:label path="photo">
		<spring:message code="socialIdentity.edit.photo" />
	</form:label>
	<form:input path="photo" />
	<form:errors cssClass="error" path="photo" /><br>

	<input type="submit" name="save"
		value="<spring:message code= "socialIdentity.edit.save"/>"/>
	<input type="button" name="cancel"
		value="<spring:message code= "socialIdentity.edit.cancel"/>"
		onClick="javascript: relativeRedir('');" />

</form:form>

