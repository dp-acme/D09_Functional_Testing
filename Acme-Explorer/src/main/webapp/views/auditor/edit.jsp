<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<input type="hidden" id="deleteMessage"
	value="<spring:message code="ranger.delete.message"/>" />
<input type="hidden" id="messagePhone"
	value="<spring:message code="auditor.phone.message"/>" />
<input type="hidden" id="countryCode"
	value="${countryCode}" />

<script type="text/javascript">
	function checkPhone() {
		phone = document.getElementById("phoneInput");
		res = true;
		
		if (!/^(\+[0-9]{1,3}\s)?(\([0-9]{1,3}\))?\s?[0-9]{4,}$/.test(phone.value)&&phone.value) {
			res = confirm(document.getElementById("messagePhone").value);
		}else if (/^[0-9]{4,}$/.test(phone.value)&&phone.value) {
			phoneCountry = document.getElementById("countryCode").value+ ' ' + document.getElementById("phoneInput").value ;
	 		newPhone = document.getElementById("phoneInput");
			newPhone.value = phoneCountry;
			
		} 
		return res;
	}
</script>

<security:authorize access="hasRole('ADMINISTRATOR')">
			<spring:url value="auditor/administrator/create.do" var="formEdit" />
</security:authorize>

<security:authorize access="hasRole('AUDITOR')">
			<spring:url value="auditor/auditor/edit.do" var="formEdit" />
</security:authorize>

<jstl:if test="${not empty messageSocialId}">
<span class="error">
	<spring:message code="${messageSocialId}" />
	</span>
</jstl:if>

<form:form action="${formEdit}" modelAttribute="auditor">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="suspicious" />
	<form:hidden path="socialIdentities" />
	<form:hidden path="folders" />
	<form:hidden path="auditRecords" />
	<form:hidden path="notes" />

	<security:authorize access="hasRole('AUDITOR')">
		<form:hidden path="userAccount"/>
	</security:authorize>
	
	<security:authorize access="hasRole('ADMINISTRATOR')">
		<form:label path="userAccount.username">
			<spring:message code="auditor.create.username" />
		</form:label>
		<form:input path="userAccount.username" />
		<form:errors cssClass="error" path="userAccount.username" /><br>

		<form:label path="userAccount.password">
			<spring:message code="auditor.create.password" />
		</form:label>
		<form:password path="userAccount.password" />
		<form:errors cssClass="error" path="userAccount.password" />
		<br>

		<input type="hidden" name="userAccount.authorities" value="AUDITOR"/>
	</security:authorize>
		

	<form:label path="name">
		<spring:message code="auditor.edit.name" />
	</form:label>
	<form:input path="name" />
	<form:errors cssClass="error" path="name" /><br>

	<form:label path="surname">
		<spring:message code="auditor.edit.surname" />
	</form:label>
	<form:input path="surname" />
	<form:errors cssClass="error" path="surname" /><br>

	<form:label path="email">
		<spring:message code="auditor.edit.email" />
	</form:label>
	<spring:message code= "auditor.edit.emailPlaceholder" var="emailPlaceholder"/>
	<form:input path="email"
		placeholder="${emailPlaceholder }" />
	<form:errors cssClass="error" path="email" /><br>


	<form:label path="phone">
		<spring:message code="auditor.edit.phone" />
	</form:label>
	<form:input path="phone" placeholder="+999 (999) 9999" id="phoneInput" /><br>

	<form:label path="address">
		<spring:message code="auditor.edit.address" />
	</form:label>
	<form:input path="address" /><br>
	
			<jstl:if test="${actorId!=null }" >

	<display:table name="socialIdentities" id="row" requestURI="${formEdit}"
		pagesize="5">
		<spring:message code="auditor.socialidentity.nick" var="nick" />
		<display:column property="nick" title="${nick}" sortable="true" />
	
		<spring:message code="auditor.socialidentity.netName" var="netName" />
		<display:column property="netName" title="${netName}" sortable="true" />
		
		<spring:message code="auditor.socialidentity.link" var="link" />
					<spring:url value="${row.getLink()}" var="rowLink" />
		<display:column title="${link}" sortable="true" >	
					<a href="${rowLink}"> 
				<jstl:out value="${rowLink}" />
					</a>
		</display:column>
		
		<spring:message code="auditor.socialidentity.image" var="photo" />
		<display:column title="${photo }" sortable="false">
			<spring:url value="${row.getPhoto()}" var="imageLink" />
				<img src="${imageLink}" />
		</display:column><br>
		
		<display:column title="" sortable="false">
			<spring:message code="auditor.socialidentity.delete" var="delete" />
			<spring:url value="/socialIdentity/delete.do?socialIdentityId=${row.getId()}" var="deleteLink" />
			<a href="${deleteLink}" onclick="javascript: return confirm(document.getElementById('deleteMessage').value);"> 
				<jstl:out value="${delete}" />
			</a>
		</display:column><br>
	</display:table>
	
	
	<spring:message code="auditor.socialidentity.create" var="createSocialIdentity" />	
	<spring:url value="socialIdentity/create.do?actorId=${actorId}" var="createSocialIdentityLink" />
			<a href="${createSocialIdentityLink}"> 
				<jstl:out value="${createSocialIdentity}" /><br>
			</a>
</jstl:if>
	

	<input type="submit" name="save"
		value="<spring:message code= "auditor.edit.save"/>"
		onClick="javascript: return checkPhone();" />
	<input type="button" name="cancel"
		value="<spring:message code= "auditor.edit.cancel"/>"
		onClick="javascript: relativeRedir('');" />

</form:form>

