<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<h2>
	<spring:message code="emergencyContact.list.explorer" /><jstl:out value="${explorer.getName()} ${explorer.getSurname()}" />
</h2>

<br />

<jstl:set value="emergencyContact/list.do?explorerId=${explorer.getId()}" var="uri" />

<display:table name="emergencyContacts" id="row" requestURI="${uri}" pagesize="5">

	<display:column titleKey="emergencyContact.list.title.name" property="name" sortable="true" />
	
	<display:column titleKey="emergencyContact.list.title.email" property="email" sortable="false" />
	
	<display:column titleKey="emergencyContact.list.title.phone" property="phone" sortable="false" />
	
	<security:authorize access="hasRole('EXPLORER')">
		<display:column>
			<a href="emergencyContact/edit.do?emergencyContactId=${row.getId()}"><spring:message code="emergencyContact.list.edit" /></a>
		</display:column>
	</security:authorize>

</display:table>

<br />

<security:authorize access="hasRole('EXPLORER')">
	<spring:message code="emergencyContact.list.newEmergencyContact" var="newEmergencyContact" />
	<input type="button" value="${newEmergencyContact}" onClick="javascript: relativeRedir('emergencyContact/create.do');" />
</security:authorize>