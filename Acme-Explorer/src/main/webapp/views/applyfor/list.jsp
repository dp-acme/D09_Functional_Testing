<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@page import="org.joda.time.DateTime"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="domain.ApplyFor"%>
<%@page import="java.util.Calendar" %>
<%@page import="java.util.Date"%>

<security:authorize access="hasRole('EXPLORER')">
	<jstl:set var="uri" value="applyfor/explorer/list.do" />
</security:authorize>
<security:authorize access="hasRole('MANAGER')">
	<jstl:set var="uri" value="applyfor/manager/list.do" />
</security:authorize>

<display:table name="applications" id="row" requestURI="${uri}"
	pagesize="5">

	<display:column titleKey="applyfor.list.tripHeader" sortable="false"
		style="">
		<security:authorize access="hasRole('EXPLORER')">
			<a
				href="<spring:url value="trip/explorer/display.do?tripId=${row.getTrip().getId()}" />">
				<jstl:out value="${row.getTrip().getTicker()} " />
			</a>
		</security:authorize>
		<security:authorize access="hasRole('MANAGER')">
			<a
				href="<spring:url value="trip/manager/display.do?tripId=${row.getTrip().getId()}" />">
				<jstl:out value="${row.getTrip().getTicker()}" />
			</a>
			<display:column property="explorer.name" titleKey="applyfor.list.explorer" sortable="true" />
		</security:authorize>
	</display:column>

	<security:authorize access="hasRole('EXPLORER')">
		<spring:message code="applyfor.list.startDate" var="startDate" />
		<display:column title="${startDate}" sortable="true">
			<spring:message code="applyfor.date.format" var="formatDate" />
			<fmt:formatDate value="${row.getTrip().getStartDate()}" pattern="${formatDate}" />
		</display:column>
	</security:authorize>

	<spring:message code="applyfor.list.momentHeader" var="moment" />
	<display:column title="${moment}" sortable="true">
		<spring:message code="applyfor.date.format" var="formatDate" />
		<fmt:formatDate value="${row.getMoment()}" pattern="${formatDate}" />
	</display:column>

	<%
		Timestamp date = new Timestamp(System.currentTimeMillis());
		Calendar datePlusMonth = Calendar.getInstance();
		Timestamp currentDatePlusMonth;
		datePlusMonth.add(Calendar.MONTH, 1);
		currentDatePlusMonth = new java.sql.Timestamp(datePlusMonth.getTimeInMillis());
		request.setAttribute("currentDatePlusMonth", currentDatePlusMonth);
		request.setAttribute("date", date);
	%>

	<display:column titleKey="applyfor.list.statusHeader" sortable="true">
		<jstl:choose>
			<jstl:when
				test="${row.getStatus().getStatus() == \"PENDING\" && row.getTrip().getStartDate().after(currentDatePlusMonth)}">
				<span style="background-color: white"><spring:message
						code="applyfor.status.pending" /></span>
			</jstl:when>
			<jstl:when test="${row.getStatus().getStatus() == \"PENDING\"}">
				<span style="background-color: red"><spring:message
						code="applyfor.status.pending" /></span>
			</jstl:when>
			<jstl:when test="${row.getStatus().getStatus() == \"REJECTED\"}">
				<span style="background-color: grey"><spring:message
						code="applyfor.status.rejected" /></span>
			</jstl:when>
			<jstl:when test="${row.getStatus().getStatus() == \"DUE\"}">
				<span style="background-color: yellow"><spring:message
						code="applyfor.status.due" /></span>
			</jstl:when>
			<jstl:when test="${row.getStatus().getStatus() == \"ACCEPTED\"}">
				<span style="background-color: green"><spring:message
						code="applyfor.status.accepted" /></span>
			</jstl:when>
			<jstl:when test="${row.getStatus().getStatus() == \"CANCELLED\"}">
				<span style="background-color: cyan"><spring:message
						code="applyfor.status.cancelled" /></span>
			</jstl:when>
		</jstl:choose>
	</display:column>

	<security:authorize access="hasRole('MANAGER')">
		<display:column property="comments"
			titleKey="applyfor.list.commentsHeader" sortable="false" />
	</security:authorize>

	<display:column title="" sortable="false">
		<security:authorize access="hasRole('EXPLORER')">
			<a
				href="<spring:url value="applyfor/explorer/display.do?applyForId=${row.getId()}" />">
				<spring:message code="applyfor.list.show" />
			</a>
		</security:authorize>
		<security:authorize access="hasRole('MANAGER')">
			<a
				href="<spring:url value="applyfor/manager/display.do?applyForId=${row.getId()}" />">
				<spring:message code="applyfor.list.show" />
			</a>
		</security:authorize>
	</display:column>

	<security:authorize access="hasRole('EXPLORER')">
		<display:column sortable="false">
				<jstl:choose>
					<jstl:when
						test="${row.getTrip().getStartDate().after(date) && row.getStatus().getStatus()== \"DUE\"}">
						<a
							href=<spring:url value="applyfor/explorer/edit.do?applyForId=${row.getId()}" />>
							<spring:message code="applyfor.list.enterCreditCard" />
						</a>
					</jstl:when>
					<jstl:when
						test="${row.getTrip().getStartDate().after(date) && row.getStatus().getStatus() == \"ACCEPTED\"}">
						<a
							href=<spring:url value="applyfor/explorer/cancel.do?applyForId=${row.getId()}"/>
							onClick="javascript: return confirm('<spring:message code="applyfor.list.cancelConfirm" />');">
							<spring:message code="applyfor.list.cancel" />
						</a>
					</jstl:when>
				</jstl:choose>
		</display:column>
	</security:authorize>
	
	<security:authorize access="hasRole('MANAGER')">
		<display:column sortable="false">
			<jstl:if test="${row.getStatus().getStatus() == \"PENDING\"}">
				<a style="float:right;"
					href="<spring:url value="applyfor/manager/edit.do?applyForId=${row.getId()}" />">
					<spring:message code="applyfor.list.reject" />
				</a>
				<a style="float:left;"
					href="<spring:url value="applyfor/manager/due.do?applyForId=${row.getId()}" />">
					<spring:message code="applyfor.list.dueManager" />
				</a>
			</jstl:if>
		</display:column>
	</security:authorize>
</display:table>

<security:authorize access="hasRole('EXPLORER')">
	<input type="button"
		value="<spring:message code="applyfor.list.back" />"
		onClick="javascript: relativeRedir('trip/list.do');" />
</security:authorize>
<security:authorize access="hasRole('MANAGER')">
	<input type="button"
		value="<spring:message code="applyfor.list.back" />"
		onClick="javascript: relativeRedir('trip/manager/list.do');" />
</security:authorize>
