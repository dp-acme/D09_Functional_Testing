<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<security:authorize access="hasRole('EXPLORER')">
	<h1>
		<jstl:set var="uri" value="applyfor/explorer/edit.do" />
		<jstl:if test="${applyfor.getId() == 0}">
			<spring:message code="applyfor.edit.create.title" />
		</jstl:if>
		<jstl:if test="${applyfor.getId() != 0}">
			<spring:message code="applyfor.edit.enterCreditCard.title" />
		</jstl:if>
	</h1>
</security:authorize>


<security:authorize access="hasRole('MANAGER')">
	<h1>
		<jstl:set var="uri" value="applyfor/manager/edit.do" />
		<spring:message code="applyfor.display.rejectTitle" />
	</h1>
</security:authorize>


<form:form method="POST" action="${uri}" modelAttribute="applyfor">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="explorer" />
	<form:hidden path="trip" />
	<form:hidden path="status" />
	<form:hidden path="moment" />

	<jstl:if test="${applyfor.getId() == 0}">
		<form:hidden path="creditCard.holderName" />
		<form:hidden path="creditCard.brandName" />
		<form:hidden path="creditCard.number" />
		<form:hidden path="creditCard.expirationMonth" />
		<form:hidden path="creditCard.expirationYear" />
		<form:hidden path="creditCard.cvv" />

		<form:label path="comments">
			<spring:message code="applyfor.edit.create.comments" />
		</form:label>
		<form:textarea path="comments" />
		<form:errors cssClass="error" path="comments" />
	</jstl:if>

	<security:authorize access="hasRole('MANAGER')">
		<form:label path="rejectedReason">
			<spring:message code="applyfor.display.rejectedReason" />
		</form:label>
		<form:input path="rejectedReason" />
		<br />
		<form:errors path="rejectedReason" />
		<br />
		<input type="submit" name="reject"
			value="<spring:message code="applyfor.list.reject" />" />

		<input type="button"
			value="<spring:message code="applyfor.edit.cancel" />"
			onClick="javascript: relativeRedir('applyfor/manager/list.do');" />
	</security:authorize>


	<security:authorize access="hasRole('EXPLORER')">
		<form:hidden path="rejectedReason" />
		<jstl:if test="${applyfor.getId() != 0}">
			<form:hidden path="comments" />

		<form:label path="creditCard.holderName">
		<spring:message code="sponsorship.creditCard.holderName" />
	</form:label>
	<form:input path="creditCard.holderName"  />
	<form:errors cssClass="error" path="creditCard.holderName"></form:errors>
	<br />

	<form:label path="creditCard.brandName">
		<spring:message code="sponsorship.creditCard.brandName" />
	</form:label>
	<form:input path="creditCard.brandName" />
	<form:errors cssClass="error" path="creditCard.brandName"></form:errors>
	<br />

	<form:label path="creditCard.number">
		<spring:message code="sponsorship.creditCard.numberHeader" />
	</form:label>
	<form:input type="number" path="creditCard.number" />
	<form:errors cssClass="error" path="creditCard.number"></form:errors>
	<br />

	<form:label path="creditCard.expirationMonth">
		<spring:message code="sponsorship.creditCard.expirationMonth" />
	</form:label>
	<form:input type="number" path="creditCard.expirationMonth"/>
	<form:errors cssClass="error" path="creditCard.expirationMonth"></form:errors>
	<br />

	<form:label path="creditCard.expirationYear">
		<spring:message code="sponsorship.creditCard.expirationYear" />
	</form:label>
	<form:input  type="number" path="creditCard.expirationYear" />
	<form:errors cssClass="error" path="creditCard.expirationYear"></form:errors>
	<br />

	<form:label path="creditCard.cvv">
		<spring:message code="sponsorship.creditCard.cvv" />
	</form:label>
	<form:input  type="number" path="creditCard.cvv"/>
	<form:errors cssClass="error" path="creditCard.cvv"></form:errors>
	<br />
		</jstl:if>
		<br />
		<input type="submit" name="save"
			value="<spring:message code="applyfor.edit.save" />" />

		<input type="button"
			value="<spring:message code="applyfor.edit.cancel" />"
			onClick="javascript: relativeRedir('applyfor/explorer/list.do');" />

	</security:authorize>

</form:form>