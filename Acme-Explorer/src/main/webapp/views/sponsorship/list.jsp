<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<security:authorize access="hasRole('SPONSOR')">
	<spring:url var="requestURI" value="sponsorship/sponsor/list.do" />
</security:authorize>

<security:authorize access="hasRole('MANAGER')">
	<spring:url var="requestURI" value="sponsorship/manager/list.do?tripId=${trip.id}" />
</security:authorize>

<display:table name="sponsorships" id="row" requestURI="${requestURI}"
	pagesize="5">

	<spring:message code="sponsorship.bannerHeader" var="bannerHeader" />
	<display:column title="${bannerHeader}" >
		<img src="<jstl:url value='${row.banner}' />" style="width:100px" />
	</display:column>

	<spring:message code="sponsorship.infoHeader" var="infoHeader" />
	<display:column property="info" title="${infoHeader}" sortable="false" />

	<!-- ATRIBUTOS DE CREDICARD-->

	<spring:message code="sponsorship.creditCard.numberHeader"
		var="creditCardNumberHeader" />
	<display:column property="creditCard.number"
		title="${creditCardNumberHeader}" sortable="false" />

	<security:authorize access="hasRole('SPONSOR')">
		<display:column title="" sortable="false">
			<a href="sponsorship/sponsor/edit.do?sponsorshipId=${row.id}"> <spring:message
					code="editText" />
			</a>
		</display:column>
	</security:authorize>

</display:table>

<input type="button" value="<spring:message code="backText" />"
	onClick="javascript: relativeRedir('trip/list.do');" />
