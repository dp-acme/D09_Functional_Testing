/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.sponsor;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.ConfigurationService;
import services.SponsorService;
import domain.Sponsor;

@Controller
@RequestMapping("/sponsor/administrator")
public class SponsorAdministratorController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private SponsorService sponsorService;

	@Autowired
	private ConfigurationService configurationService;
	// Constructors -----------------------------------------------------------

	public SponsorAdministratorController() {
		super();
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		Sponsor sponsor;
		
		sponsor = this.sponsorService.create();
		result = this.createEditModelAndView(sponsor);

		
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView saveCreate(@Valid Sponsor sponsor, BindingResult binding){
		ModelAndView result;
		if(binding.hasErrors()){
			result = createEditModelAndView(sponsor);
		} else { 
			try {
				sponsorService.save(sponsor);
				result = new ModelAndView("redirect:/");
			} catch (Throwable oops) {
				result = createEditModelAndView(sponsor, "sponsor.commit.error");
			}
		}
		return result;
	}
	
	
	protected ModelAndView createEditModelAndView(Sponsor sponsor) {
		ModelAndView result; 
		
		result = createEditModelAndView(sponsor, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(Sponsor sponsor,
			String messageCode) {
		ModelAndView result;
		result = new ModelAndView("sponsor/administrator/create");
		result.addObject("sponsor", sponsor);
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());
		
		result.addObject("message", messageCode);
		
		return result;
	}

}
