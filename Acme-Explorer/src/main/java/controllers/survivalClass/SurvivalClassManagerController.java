
package controllers.survivalClass;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.SurvivalClassService;
import services.TripService;
import controllers.AbstractController;
import domain.Manager;
import domain.SurvivalClass;
import domain.Trip;

@Controller
@RequestMapping("/survivalClass")
public class SurvivalClassManagerController extends AbstractController {

	//Servicios-----------------------------------------------------------------
	@Autowired
	private SurvivalClassService	survivalClassService;
	@Autowired
	private TripService				tripService;

	@Autowired
	private ActorService			actorService;


	//Contructor-------------------------------------------------------------------
	public SurvivalClassManagerController() {
		super();
	}

	//List--------------------------------------------------------------------
	@RequestMapping("/manager/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<SurvivalClass> survivalClasses;
		Collection<Trip> trips;
		Manager manager;

		survivalClasses = new ArrayList<SurvivalClass>();
		manager = (Manager) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		trips = manager.getTrips();
		
		for(Trip t : trips){
			survivalClasses.addAll(survivalClassService.getSurvivalClassesbyTripId(t.getId()));
		}

		result = new ModelAndView("survivalClass/manager/list");
		result.addObject("survivalClasses", survivalClasses);

		return result;

	}
	@RequestMapping("/manager/edit")
	public ModelAndView edit(@RequestParam int survivalClassId) {
		ModelAndView result;
		SurvivalClass survivalClass;
		
		survivalClass = survivalClassService.findOne(survivalClassId);

		result = createEditModelAndView(survivalClass);

		return result;
	}
	//Edit-----------------------------------------------------------------------

	@RequestMapping(value = "/manager/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid SurvivalClass survivalClass, BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = createEditModelAndView(survivalClass);
		} else {
			try {
				this.survivalClassService.save(survivalClass);
				result = new ModelAndView("redirect:list.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(survivalClass, "survivalClass.commit.error");
			}
		}

		return result;
	}

	//Create-----------------------------------------------------------------------
	@RequestMapping("/manager/create")
	public ModelAndView create(@RequestParam int tripId) {
		ModelAndView result;
		SurvivalClass survivalClass;
		Collection<Trip> trips;
		Trip trip;

		trips = this.tripService.findByManager();
		trip = tripService.findOne(tripId);
		result = new ModelAndView("/survivalClass/manager/create");

		for (Trip t : trips) {
			if (t.getId() == (trip.getId())) {
				survivalClass = survivalClassService.create(trip);
				result = createEditModelAndView(survivalClass);
			}
		}

		return result;
	}
	//Delete--------------------------------------------------------
	@RequestMapping(value = "/manager/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final SurvivalClass survivalClass, final BindingResult binding) {
		ModelAndView result;

		try {
			this.survivalClassService.delete(survivalClass);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(survivalClass, "survivalClass.commit.error");
		}

		return result;
	}

	//Aux--------------------------------------------------------

	private ModelAndView createEditModelAndView(SurvivalClass survivalClass) {
		ModelAndView result;

		result = createEditModelAndView(survivalClass, null);

		return result;
	}

	private ModelAndView createEditModelAndView(SurvivalClass survivalClass, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("survivalClass/manager/edit");

		result.addObject("survivalClass", survivalClass);
		result.addObject("message", messageCode);

		return result;
	}

}
