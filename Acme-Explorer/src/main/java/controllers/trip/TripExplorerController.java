package controllers.trip;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ApplyForService;
import services.ExplorerService;
import services.FinderService;
import services.TripService;
import controllers.AbstractController;
import domain.Actor;
import domain.ApplyFor;
import domain.Explorer;
import domain.Finder;
import domain.Sponsorship;
import domain.Trip;

@Controller
@RequestMapping("/trip/explorer")
public class TripExplorerController extends AbstractController{
	// Services------------------------------
	@Autowired
	private TripService tripService;
	
	@Autowired
	private FinderService finderService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ExplorerService explorerService;
	
	@Autowired
	private ApplyForService applyForService;
	
	// Constructor---------------------------
	public TripExplorerController() {
		super();
	}

	//Listing--------------------------------
	@RequestMapping(value = "/list")
	public ModelAndView list(@ModelAttribute("finder") @Valid Finder finder, BindingResult binding){
		ModelAndView result;
		Actor principal;
		Finder principalFinder;
		
		principal = actorService.findPrincipal();
		
		Assert.isTrue(principal instanceof Explorer);
		
		explorerService.createExplorerFinder((Explorer)principal);
		
		principalFinder = ((Explorer)principal).getFinder();
		
		if(binding.hasErrors()){
			result = new ModelAndView("trip/list");
			
			result.addObject("trips", principalFinder.getTrips());
			result.addObject("requestURI", "trip/explorer/list.do");
			result.addObject("finder", principalFinder);
			
		}else{
			try{				
				principalFinder = finderService.updateFinder(principalFinder, finder);
				
				result = new ModelAndView("trip/list");

				result.addObject("trips", principalFinder.getTrips());
				result.addObject("requestURI", "trip/explorer/list.do");
				result.addObject("finder", finder);

			}catch(Throwable oops){
				result = new ModelAndView("trip/list");
				
				result.addObject("trips", tripService.filterTripsByCriteria(null, null, null, null, null));
				result.addObject("requestURI", "trip/explorer/list.do");
				result.addObject("finder", principalFinder);
			}
		}		
		
		return result;
	}
	
	@RequestMapping(value = "/display", method=RequestMethod.GET)
	public ModelAndView display(@RequestParam("tripId") int tripId){
		ModelAndView result;
		Trip trip;
		Sponsorship sponsorship;
		String bannerAux;
		Explorer explorer;
		ApplyFor apply;
		
		trip = tripService.findOne(tripId);
		explorer = (Explorer) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		result = new ModelAndView("trip/display");
		
		Assert.notNull(trip);
		Assert.isTrue(!trip.getPublicationDate().after(new Date()));
		
		sponsorship = tripService.getRandomSponsorship(trip.getId());
		bannerAux = "";
		apply = applyForService.getApplicationFromExplorerAndTrip(explorer, trip);
		
		result.addObject("trip", trip);
		result.addObject("requestURI", "trip/explorer/display.do");
		result.addObject("applyfor", apply);
		
		if(sponsorship != null){
			result.addObject("bannerSponsor", sponsorship.getBanner());
			result.addObject("infoSponsor", sponsorship.getInfo());
		}
		else{
			result.addObject("bannerSponsor", bannerAux);
		}
		
		return result;
	}
}
