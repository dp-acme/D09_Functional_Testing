
package controllers.curriculum;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.MiscellaneousService;
import controllers.AbstractController;
import domain.Curriculum;
import domain.Miscellaneous;
import domain.Ranger;

@Controller
@RequestMapping("/miscellaneous")
public class MiscellaneousRangerController extends AbstractController {

	//Services------------------------------------------------------------
	@Autowired
	private MiscellaneousService	miscellaneousService;

	@Autowired
	private ActorService			actorService;


	//Constructors------------------------------------------------------------
	private MiscellaneousRangerController() {
		super();
	}

	//Display-------------------------------------------------------------------------------
	@RequestMapping(value = "/ranger/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam final int miscellaneousId) {
		ModelAndView result;
		Miscellaneous miscellaneous;

		miscellaneous = this.miscellaneousService.findOne(miscellaneousId);
		Assert.notNull(miscellaneous);
		result = new ModelAndView("miscellaneous/ranger/display");
		result.addObject("miscellaneous", miscellaneous);

		return result;
	}

	//List -------------------------------------------------------------------------
	@RequestMapping("/ranger/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Miscellaneous> miscellaneousRecords;
		Ranger ranger;
		ranger = (Ranger) this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		miscellaneousRecords = ranger.getCurriculum().getMiscellaneousRecords();

		result = new ModelAndView("miscellaneous/ranger/list");
		result.addObject("miscellaneousRecords", miscellaneousRecords);

		return result;
	}

	@RequestMapping("/ranger/edit")
	public ModelAndView edit(@RequestParam int miscellaneousId) {
		ModelAndView result;
		Miscellaneous miscellaneous;

		miscellaneous = miscellaneousService.findOne(miscellaneousId);

		Assert.notNull(miscellaneous);

		result = createEditModelAndView(miscellaneous);

		return result;
	}

	@RequestMapping(value = "/ranger/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Miscellaneous miscellaneous, final BindingResult binding) {
		ModelAndView result;

		try {
			this.miscellaneousService.delete(miscellaneous);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(miscellaneous, "miscellaneous.commit.error");
		}

		return result;
	}

	@RequestMapping(value = "/ranger/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Miscellaneous miscellaneous, BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = createEditModelAndView(miscellaneous);
		} else {
			try {
				this.miscellaneousService.save(miscellaneous);
				result = new ModelAndView("redirect:list.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(miscellaneous, "miscellaneous.commit.error");
			}
		}

		return result;
	}

	@RequestMapping("/ranger/create")
	public ModelAndView create() {
		ModelAndView result;
		Miscellaneous miscellaneous;
		Ranger ranger;
		Curriculum curriculum;

		
		ranger = (Ranger) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		curriculum = ranger.getCurriculum();
		miscellaneous = miscellaneousService.create(curriculum);

		result = createEditModelAndView(miscellaneous);

		return result;
	}

	private ModelAndView createEditModelAndView(Miscellaneous miscellaneous) {
		ModelAndView result;

		result = createEditModelAndView(miscellaneous, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Miscellaneous miscellaneous, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("miscellaneous/ranger/edit");

		result.addObject("miscellaneous", miscellaneous);
		result.addObject("message", messageCode);

		return result;
	}
}
