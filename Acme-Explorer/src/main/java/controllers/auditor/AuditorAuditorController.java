/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.auditor;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.ActorService;
import services.AuditorService;
import services.ConfigurationService;
import domain.Actor;
import domain.Auditor;

@Controller
@RequestMapping("/auditor/auditor")
public class AuditorAuditorController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private AuditorService auditorService;
	
	@Autowired
	private ActorService actorService;
	

	@Autowired
	private ConfigurationService configurationService;
	
	// Constructors -----------------------------------------------------------

	public AuditorAuditorController() {
		super();
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.GET)
	public ModelAndView edit(){
		ModelAndView result;
		Actor principal;
		Auditor auditor;
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Auditor);
		auditor = (Auditor) principal;
		Assert.notNull(auditor);
		result = this.createEditModelAndView(auditor);
		
		return result;
	}
	
	
	
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="save")
	public ModelAndView save(@Valid Auditor auditor, BindingResult binding){
		ModelAndView result;
		if(binding.hasErrors()){
			result = createEditModelAndView(auditor);
		} else { 
			try {
				auditorService.save(auditor);
				result = new ModelAndView("redirect:/");
			} catch (Throwable oops) {
				result = createEditModelAndView(auditor, "auditor.commit.error");
			}
		}
		return result;
	}
	
	protected ModelAndView createEditModelAndView(Auditor auditor) {
		ModelAndView result; 
		
		result = createEditModelAndView(auditor, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(Auditor auditor,
			String messageCode) {
		ModelAndView result;
		
		result = new ModelAndView("auditor/auditor/edit");
		result.addObject("auditor", auditor);
		result.addObject("actorId", auditor.getId());
		result.addObject("socialIdentities", auditor.getSocialIdentities());
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());
		
		result.addObject("message", messageCode);
		
		return result;
	}

}
