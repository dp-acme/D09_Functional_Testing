/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.explorer;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ConfigurationService;
import services.ExplorerService;
import controllers.AbstractController;
import domain.Actor;
import domain.Explorer;

@Controller
@RequestMapping("/explorer/explorer")
public class ExplorerExplorerController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private ExplorerService explorerService;
	
	@Autowired
	private ActorService actorService;

	@Autowired
	private ConfigurationService configurationService;
	
	// Constructors -----------------------------------------------------------

	public ExplorerExplorerController() {
		super();
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.GET)
	public ModelAndView edit(){
		ModelAndView result;
		Actor principal;
		Explorer explorer;
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Explorer);
		explorer = (Explorer) principal;
		Assert.notNull(explorer);
		result = this.createEditModelAndView(explorer);
		
		return result;
	}
	
	
	
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="save")
	public ModelAndView save(@Valid Explorer explorer, BindingResult binding){
		ModelAndView result;
		if(binding.hasErrors()){
			result = createEditModelAndView(explorer);
		} else { 
			try {
				explorerService.save(explorer);
				result = new ModelAndView("redirect:/");
			} catch (Throwable oops) {
				result = createEditModelAndView(explorer, "explorer.commit.error");
			}
		}
		return result;
	}
	
	protected ModelAndView createEditModelAndView(Explorer explorer) {
		ModelAndView result; 
		
		result = createEditModelAndView(explorer, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(Explorer explorer,
			String messageCode) {
		ModelAndView result;
		result = new ModelAndView("explorer/explorer/edit");
		result.addObject("explorer", explorer);
		result.addObject("actorId", explorer.getId());
		result.addObject("socialIdentities", explorer.getSocialIdentities());
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());

		result.addObject("message", messageCode);
		
		return result;
	}

}
