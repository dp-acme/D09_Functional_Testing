package controllers.stage;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.StageService;
import services.TripService;
import domain.Actor;
import domain.Manager;
import domain.Stage;
import domain.Trip;

@Controller
@RequestMapping("/stage/manager")
public class StageManagerController extends AbstractController{
	// Services------------------------------
		@Autowired
		private TripService tripService;
		
		@Autowired
		private StageService stageService;
		
		@Autowired
		private ActorService actorService;
		
		// Constructor---------------------------
		public StageManagerController() {
			super();
		}
		
		@RequestMapping(value = "/list", method=RequestMethod.GET)
		public ModelAndView list(@RequestParam("tripId") int tripId){
			ModelAndView result;
			Collection<Stage> stages;
			Trip trip;
			
			UserAccount userAccount;
			Actor actor;

			userAccount = LoginService.getPrincipal();
			actor = actorService.findByUserAccountId(userAccount.getId());
			Assert.isInstanceOf(Manager.class, actor);
			
			trip = tripService.findOne(tripId);
			stages = trip.getStages();
			
			Assert.notNull(trip);
			Assert.isTrue(!(trip.getPublicationDate().after(new Date()) && !trip.getManager().equals(actor)));
			
			result = new ModelAndView("stage/manager/list");
			result.addObject("trip", trip);
			result.addObject("stages", stages);
			result.addObject("requestUri", "stage/manager/list.do");
			
			return result;
		}
		
		@RequestMapping(value = "/create", method=RequestMethod.GET)
		public ModelAndView create(@RequestParam("tripId") int tripId){
			ModelAndView result;
			Trip trip;
			Stage stage;
			
			trip = tripService.findOne(tripId);
			stage = this.stageService.create(trip);

			result = new ModelAndView("stage/manager/create");
			result.addObject("stage", stage);
			result.addObject("trip", trip);
			
			return result;
		}
		
		@RequestMapping(value = "/create", method=RequestMethod.POST, params="save")
		public ModelAndView save(@Valid Stage stage, BindingResult binding){
			ModelAndView result;
			Trip trip;
			Collection<Stage> stages;
			
			if(binding.hasErrors()){
				result = createEditModelAndView(stage);
			}else{
				try{
					trip=stage.getTrip();
					stages = trip.getStages();
					stages.add(stage);
					trip.setStages(stages);
					tripService.save(trip);
					result = new ModelAndView("redirect:list.do?tripId="+trip.getId());
				}catch(Throwable oops){
					result = createEditModelAndView(stage, oops.getMessage());
				}
			}
			
			return result;
		}
		
		@RequestMapping(value = "/delete", method=RequestMethod.GET)
		public ModelAndView delete(@RequestParam("stageId") int stageId){
			ModelAndView result;
			Stage stage;
			
			stage = stageService.findOne(stageId);
			stageService.delete(stage);
			
			result = new ModelAndView("redirect:list.do?tripId="+stage.getTrip().getId());
		
			
			return result;
		}
		
		protected ModelAndView createEditModelAndView(Stage stage){
			ModelAndView result;
			
			result = createEditModelAndView(stage, null);
			
			return result;
		}
		
		protected ModelAndView createEditModelAndView(Stage  stage, String messageCode){
			ModelAndView result;
			Trip trip;
			
			trip = stage.getTrip();
			
			result = new ModelAndView("stage/manager/create");
			result.addObject("stage", stage);
			result.addObject("trip", trip);
			result.addObject("message", messageCode);
			
			
			return result;
	 	}
}
