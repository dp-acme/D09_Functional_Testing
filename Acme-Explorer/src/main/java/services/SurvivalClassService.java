package services;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.SurvivalClassRepository;
import security.LoginService;
import security.UserAccount;
import domain.ApplyFor;
import domain.Explorer;
import domain.Manager;
import domain.Status;
import domain.SurvivalClass;
import domain.Trip;

@Service
@Transactional

public class SurvivalClassService {

	// Managed repository --------------------------------------

	@Autowired
	private SurvivalClassRepository	survivalClassRepository;

	// Supporting services -------------------------------------
	
	@Autowired
	private ExplorerService explorerService;
	
	@Autowired
	private ApplyForService applyForService;
	
	// Constructors --------------------------------------------
	
	public SurvivalClassService(){
		super();
	}
	
	// Simple CRUD methods -------------------------------------

	public SurvivalClass create(Trip trip){
		SurvivalClass result;
		
		result = new SurvivalClass();
		
		result.setTrip(trip);
		result.setExplorers(new ArrayList<Explorer>());
		
		return result;
	}
	
	public SurvivalClass findOne(int survivalClassId){
		Assert.isTrue(survivalClassId != 0);
		
		SurvivalClass result = survivalClassRepository.findOne(survivalClassId);
		
		return result;
	}
	
	public Collection<SurvivalClass> findAll(){
		Collection<SurvivalClass> result;
		
		result = this.survivalClassRepository.findAll();
		Assert.notNull(result);

		return result;
	}
	
	public SurvivalClass save(SurvivalClass survivalClass){
		Assert.notNull(survivalClass);
		Assert.isTrue(survivalClass.getTrip().getStartDate().after(new Date()));
		
		SurvivalClass result;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(survivalClass.getTrip().getManager().getUserAccount().equals(principal));		
						
		result = survivalClassRepository.save(survivalClass);
		
		return result;
	}
	
	public void delete(SurvivalClass survivalClass) {
		Assert.notNull(survivalClass);
		
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		
		Assert.isTrue(survivalClass.getTrip().getManager().getUserAccount().equals(principal));		
		Assert.notNull(this.findOne(survivalClass.getId()));

		survivalClassRepository.delete(survivalClass);
	}
	
	public void deleteInBatch(Collection<SurvivalClass> survival, Manager manager) {
		Assert.notNull(survival);
		Assert.notEmpty(survival);		
		Assert.notNull(manager);		
		
		List<SurvivalClass> survivalList;
		
		survivalList = (List<SurvivalClass>) survival;
		
		Assert.isTrue(survivalList.get(0).getTrip().getManager().equals(manager));
		
		survivalClassRepository.deleteInBatch(survival);
	}
	
	public SurvivalClass joinSurvivalClass(Explorer explorer, SurvivalClass survivalClass){
		Assert.notNull(explorer);
		Assert.notNull(survivalClass);
		Assert.isTrue(survivalClass.getTrip().getStartDate().after(new Date()));
		
		SurvivalClass result;
		ApplyFor applyFor;
		
		applyFor = applyForService.getApplicationFromExplorerAndTrip(explorer, survivalClass.getTrip());
		
		Assert.notNull(applyFor);
		Assert.isTrue(applyFor.getStatus().getStatus().equals(Status.ACCEPTED));
		
		survivalClass.getExplorers().add(explorer);
		result = survivalClassRepository.save(survivalClass);
		explorer.getSurvivalClasses().add(result);
		
		explorerService.save(explorer);
		
		return result;
	}
	
	// Other business methods -----------------------------------
	
	public Collection<SurvivalClass> getSurvivalClassesbyTripId(int tripId){
		Collection<SurvivalClass> result;
		
		result = survivalClassRepository.getSurvivalClassesbyTripId(tripId);
		Assert.notNull(result);
		
		return result;
	}
	
	public void dropSurvival(Explorer explorer, SurvivalClass survival){
		explorer.getSurvivalClasses().remove(survival);
		survival.getExplorers().remove(explorer);
		
		survivalClassRepository.save(survival);
		explorerService.save(explorer);
		
	}
}
