package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.LegalTextRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.LegalText;

@Service
@Transactional
public class LegalTextService {

	// MANAGED REPOSITORY ------------------------------------
	
	@Autowired
	private LegalTextRepository legalTextRepository;

	// SUPPORTING SERVICES -----------------------------------

	// Constructor ---------------------------------------------------

	public LegalTextService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public LegalText create() {
		LegalText result;
		
		result = new LegalText();
		result.setMoment(new Date(System.currentTimeMillis()-1));
		
		return result;
	}

	public LegalText findOne(int legalTextId) {
		Assert.isTrue(legalTextId != 0);

		LegalText result;

		result = legalTextRepository.findOne(legalTextId);

		return result;
	}

	public Collection<LegalText> findAll() {
		Collection<LegalText> result;
		UserAccount userAccount; 
		Authority aut;
		Authentication auths;
		
		auths = SecurityContextHolder.getContext().getAuthentication();
		if(auths!=null && auths.getAuthorities().toArray().length>0 &&auths.getAuthorities().toArray()[0].toString().equals("ROLE_ANONYMOUS")){
			result = legalTextRepository.getLegalTextFinalMode();
		}else{
			userAccount= LoginService.getPrincipal();
			aut = new Authority();
			aut.setAuthority(Authority.ADMINISTRATOR);
			if(userAccount.getAuthorities().contains(aut)){
				result = legalTextRepository.findAll();
			}else{
				result = legalTextRepository.getLegalTextFinalMode();
			}
		}
		Assert.notNull(result);
		
		return result;
	}
	
	// Todas las comprobaciones necesarias se har�n en los m�todos auxiliares
	public LegalText save(LegalText legalText) {
		Assert.notNull(legalText);
		LegalText result;
		
		result = legalTextRepository.save(legalText);
		
		return result;
	}

	public void delete(LegalText legalText) {
		Assert.notNull(legalText);
		
		UserAccount userAccount; 
		Authority aut;
		
		userAccount= LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);
		Assert.isTrue(userAccount.getAuthorities().contains(aut));
		
		Assert.isTrue(!legalText.isFinalMode());
		Assert.isTrue(legalTextRepository.exists(legalText.getId()));
		
		legalTextRepository.delete(legalText);
	}
	
	// Other business methods ---------------------------------------------------

	// Este m�todo se utiliza para editar legalText que est�n en draftMode
	public LegalText editAttributes(LegalText legalText) {
		Assert.notNull(legalText);
		
		UserAccount userAccount;
		Authority aut;
		LegalText result, legalTextOld;
		
		userAccount= LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);
		Assert.isTrue(userAccount.getAuthorities().contains(aut));
		
		if (legalText.getId() != 0) {
			legalTextOld = findOne(legalText.getId());
			
			Assert.isTrue(!legalTextOld.isFinalMode());
		}
		
		result = save(legalText);
		
		return result;
	}

	public Collection<LegalText> getLegalTextFinalMode() {
		Collection<LegalText> legalTexts;
		
		legalTexts = this.legalTextRepository.getLegalTextFinalMode();
		
		Assert.notNull(legalTexts);
		return legalTexts;
	}

}