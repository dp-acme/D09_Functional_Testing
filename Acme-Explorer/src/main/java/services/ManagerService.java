package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ManagerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Folder;
import domain.Manager;
import domain.SocialIdentity;
import domain.Trip;

@Service
@Transactional
public class ManagerService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private ManagerRepository managerRepository;

	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private ActorService actorService;
		
	// Constructor ---------------------------------------------------
	
	public ManagerService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Manager create() {
		
		Manager result;
		
		result = new Manager();
		result.setBanned(false);
		result.setSuspicious(false);
		result.setTrips(new ArrayList<Trip>());
		result.setFolders(new ArrayList<Folder>());
		result.setSocialIdentities(new ArrayList<SocialIdentity>());
		
		return result;
	}
	
	public Manager findOne(int managerId) {
		Assert.isTrue(managerId != 0);
		
		Manager result;
		
		result = managerRepository.findOne(managerId);
		
		return result;
	}
	
	public Collection<Manager> findAll() {		
		Collection<Manager> result;
		
		result = managerRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Manager save(Manager manager) {
		Assert.notNull(manager);
		
		Manager result;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		
		if (manager.getId() == 0) {
			Authority auth;
			Md5PasswordEncoder encoder;

			auth = new Authority();
			auth.setAuthority(Authority.ADMINISTRATOR);
			Assert.isTrue(principal.getAuthorities().contains(auth));
			
			encoder = new Md5PasswordEncoder();
			manager.getUserAccount().setPassword(encoder.encodePassword(manager.getUserAccount().getPassword(),null));
			manager.getUserAccount().setAccountNonLocked(true);

		} else {
			Assert.isTrue(manager.getUserAccount().equals(principal));
		}
		
		result = managerRepository.save(manager);
		
		if (manager.getId() == 0) {
			result = (Manager)actorService.addSystemFolders(result);
			
		} else{
			result.setSuspicious(configurationService.hasSpam((Actor) result));
		}

		result = managerRepository.save(result);
		
		return result;
	}
	
	
	// Other business methods ---------------------------------------------------
	

}
