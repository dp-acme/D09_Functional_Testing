package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.StageRepository;
import security.LoginService;
import security.UserAccount;
import domain.Stage;
import domain.Trip;

@Service
@Transactional
public class StageService {
	
	// Managed repository --------------------------------------

	@Autowired
	private StageRepository	stageRepository;
	
	@Autowired
	private TripService	tripService;
		
	// Supporting services -------------------------------------
	
	// Constructors --------------------------------------------
	
	public StageService(){
		super();
	}
	
	// Simple CRUD methods -------------------------------------
	
	public Stage create(Trip trip){
		Assert.notNull(trip);
		
		Stage result;
		
		result = new Stage();
		result.setTrip(trip);
						
		return result;
	}
	
	public Collection<Stage> findAll(){
		Collection<Stage> result;
		result = this.stageRepository.findAll();
		Assert.notNull(result);

		return result;
	}
	
	public Stage findOne(int stageId){
		Assert.isTrue(stageId != 0);
		
		Stage result = stageRepository.findOne(stageId);
		Assert.notNull(result);
		
		return result;
	}
	
	public void delete(Stage stage) {
		Assert.notNull(stage);
		
		UserAccount principal;
		Trip trip;
		
		principal = LoginService.getPrincipal();
		
		Assert.isTrue(stage.getTrip().getManager().getUserAccount().equals(principal));		
		Assert.notNull(this.findOne(stage.getId()));
		trip = stage.getTrip();
		trip.getStages().remove(stage);
		
		tripService.save(trip);
		
		stageRepository.delete(stage);
	}
}
