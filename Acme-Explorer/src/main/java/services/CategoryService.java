package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.CategoryRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Category;
import domain.Trip;

@Service
@Transactional
public class CategoryService {
	@Autowired
	TripService tripService;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	public CategoryService() {
		super();
	}

	public Category create() {
		Category category;

		category = new Category();
		category.setChildrenCategories(new ArrayList<Category>());

		return category;
	}

	public Collection<Category> findAll() {
		Collection<Category> result;

		result = categoryRepository.findAll();

		return result;
	}

	public Category findOne(int categoryId) {
		Assert.isTrue(categoryId != 0);

		Category result = categoryRepository.findOne(categoryId);

		return result;
	}

	public Category save(Category category) {
		Authority aut;
		UserAccount user;
		Category result;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));
		Assert.notNull(category);
		
		if (category.getId() == 0) { //No se pueden crear categor�as con hijos
			Assert.isTrue(category.getChildrenCategories().size() == 0);			
			Assert.isTrue(!category.getName().equals("CATEGORY")); //No se pueden crear categor�as que se llamen CATEGORY	
		}
		
		result = categoryRepository.save(category);

		return result;
	}
	
	private void eraseChildrenReferences(Category category, Collection<Trip> trips, Set<Category> categoriesToDelete){
		Collection<Category> children = category.getChildrenCategories();
		
		for(Trip t : trips){
			tripService.deleteCategory(t.getId());			
		}
		
		categoriesToDelete.addAll(children);
		
		for(Category c : children){
			eraseChildrenReferences(c, trips, categoriesToDelete);
		}
	}
	
	private void eraseChildren(Category category){
		Category parent = findParentCategory(category);
		
		parent.getChildrenCategories().remove(category);
		
		categoryRepository.save(parent);

		for(Category c : category.getChildrenCategories()){
			delete(c);
		}
	}
	
	public void delete(Category category) {
		Authority aut;
		UserAccount user;
		Category parentCategory;
		Set<Category> categoriesToDelete;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));
		Assert.notNull(category);
		
		Assert.isTrue(!category.getName().equals("CATEGORY")); //No se pueden borrar categor�as que se llamen CATEGORY	
		
		parentCategory = categoryRepository.findParentCategory(category);
		categoriesToDelete = new HashSet<>();
		categoriesToDelete.add(category);
		
		Assert.notNull(parentCategory);
		
		eraseChildrenReferences(category, tripService.findAll(), categoriesToDelete);
		eraseChildren(category);
		
		categoryRepository.flush();
		
		categoryRepository.deleteInBatch(categoriesToDelete);
	}

	public Category addChildCategory(Category parent, Category child){ //A�adir un hijo a una categor�a
		Category result;
		
		Assert.notNull(parent);
		Assert.notNull(child);
		
		Assert.notNull(categoryRepository.findOne(parent.getId()));
		
		for(Category c : parent.getChildrenCategories()){ //No se pueden crear categor�as con el mismo nombre y el mismo padre
			Assert.isTrue(!c.getName().equals(child.getName()));
		}
		
		result = save(child);
		
		parent.getChildrenCategories().add(result);
		save(parent);
		
		return result;
	}
	
	public Category findRootcategory() {
		Category result;

		result = categoryRepository.findRootCategory();

		return result;
	}

	public Category findParentCategory(Category rootCategory) {
		Category result;

		result = categoryRepository.findParentCategory(rootCategory);

		return result;
	}
	
}
