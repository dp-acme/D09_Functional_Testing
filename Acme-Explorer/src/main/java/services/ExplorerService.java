package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ExplorerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.ApplyFor;
import domain.EmergencyContact;
import domain.Explorer;
import domain.Finder;
import domain.Folder;
import domain.SocialIdentity;
import domain.Status;
import domain.Story;
import domain.SurvivalClass;
import domain.Trip;

@Service
@Transactional
public class ExplorerService {

	// Managed repository --------------------------------------
	
	@Autowired
	private ExplorerRepository explorerRepository;
	
	// Supporting services -------------------------------------

	@Autowired
	private ConfigurationService configurationService;

	@Autowired
	private ApplyForService applyForService;
	
	@Autowired
	private TripService tripService;
	
	@Autowired
	private FinderService finderService;
	
	@Autowired
	private ActorService actorService;

	// Constructor ---------------------------------------------------
	
	public ExplorerService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Explorer create() {
		
		Explorer result;
		
		result = new Explorer();
		result.setFolders(new ArrayList<Folder>());
		result.setSocialIdentities(new ArrayList<SocialIdentity>());
		result.setSuspicious(false);
		result.setBanned(false);
		result.setStories(new ArrayList<Story>());
		result.setApplications(new ArrayList<ApplyFor>());
		result.setEmergencyContacts(new ArrayList<EmergencyContact>());
		result.setSurvivalClasses(new ArrayList<SurvivalClass>());
		
		return result;
	}

	public Collection<Explorer> findAll() {
		Collection<Explorer> result;
		
		result = explorerRepository.findAll();

		return result;
	}

	public Explorer findOne(int explorer) {
		Explorer result;
	
		result = explorerRepository.findOne(explorer);
	
		return result;
	}

	public Explorer save(Explorer explorer) {
		Assert.notNull(explorer);
		Explorer result;
		UserAccount userAccount;
		
		if(explorer.getId()!=0){
			userAccount = LoginService.getPrincipal();
			Assert.isTrue(explorer.getUserAccount().equals(userAccount));
		}else{
			String role;
			Md5PasswordEncoder encoder;

			if(SecurityContextHolder.getContext().getAuthentication()!=null
		&&SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray().length>0){
				role = SecurityContextHolder.getContext()
					.getAuthentication().getAuthorities().toArray()[0].toString();
			Assert.isTrue(role.equals("ROLE_ANONYMOUS"));
			}
			encoder = new Md5PasswordEncoder();
			explorer.getUserAccount().setPassword(encoder.encodePassword(explorer.getUserAccount().getPassword(),null));
			explorer.getUserAccount().setAccountNonLocked(true);
		}
		
		result = explorerRepository.save(explorer);
		
		if (explorer.getId() == 0) {
			result = (Explorer)actorService.addSystemFolders(result);
			
		} else{
			result.setSuspicious(configurationService.hasSpam((Actor) result));
		}
		
		result = explorerRepository.save(result);

		return result;
	}

	//Other business methods -----------------------------------

	public ApplyFor cancelApplyFor(int tripId){ 
		Trip trip;
		UserAccount principal; 
		Authority auth; 
		Explorer explorer; 
		ApplyFor applyFor;
		ApplyFor result;
		
		auth = new Authority();
		auth.setAuthority(Authority.EXPLORER);
		principal = LoginService.getPrincipal();
		
		Assert.isTrue(principal.getAuthorities().contains(auth));
		
		explorer = (Explorer) actorService.findByUserAccountId(principal.getId());
		trip = tripService.findOne(tripId);
		
		Assert.notNull(trip);
		
		applyFor = applyForService.getApplicationFromExplorerAndTrip(explorer, trip);
		
		Assert.isTrue(applyFor.getStatus().getStatus().equals(Status.ACCEPTED));
		
		applyFor.getStatus().setStatus(Status.CANCELLED);
		
		result = applyForService.save(applyFor);
		
		return result;
	}

		public Collection<Explorer> findByTrip(int tripId) {
		UserAccount principal;
		Trip trip;
		Collection<Explorer> result;
		
		principal = LoginService.getPrincipal();
		trip = tripService.findOne(tripId);
		Assert.isTrue(trip.getManager().getUserAccount().equals(principal));

		result = explorerRepository.findByTrip(tripId);

		return result;
	}
		
	public void createExplorerFinder(Explorer explorer){ //Le asigna un Finder al explorer si no lo tiene
		if(explorer.getFinder() == null){
			Finder newFinder;
			
			newFinder = finderService.create();
			newFinder = finderService.save(newFinder);
			
			explorer.setFinder(newFinder);
			
			this.save(explorer);
		}
	}
}
