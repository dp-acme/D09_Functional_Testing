package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Category extends DomainEntity {

	// Constructor
	
	public Category() {
		super();
	}
	
	// Attributes
	
	private String name;

	@NotBlank
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	// Relationships
	
	private Collection<Category> childrenCategories;
	
	@NotNull
	@Valid
	@OneToMany
	public Collection<Category> getChildrenCategories() {
		return childrenCategories;
	}
	
	public void setChildrenCategories(Collection<Category> childrenCategories) {
		this.childrenCategories = childrenCategories;
	}
}
