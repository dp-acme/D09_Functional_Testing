package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class LegalText extends DomainEntity{

	// Constructor
	
	public LegalText() {
		super();
	}
	
	// Attributes
	
	private String title;
	private String body;
	private Date moment;
	private String laws;
	private boolean finalMode;

	@NotBlank
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	@NotBlank
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getMoment() {
		return moment;
	}
	public void setMoment(Date moment) {
		this.moment = moment;
	}

	@NotBlank
	public String getLaws() {
		return laws;
	}
	public void setLaws(String laws) {
		this.laws = laws;
	}

	public boolean isFinalMode() {
		return finalMode;
	}
	public void setFinalMode(boolean finalMode) {
		this.finalMode = finalMode;
	}
	
	// Relationships
}
