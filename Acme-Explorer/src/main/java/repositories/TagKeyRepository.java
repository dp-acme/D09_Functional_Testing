package repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import domain.TagKey;

public interface TagKeyRepository extends JpaRepository<TagKey, Integer>{

}
