
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Folder;

@Repository
public interface FolderRepository extends JpaRepository<Folder, Integer> {

	@Query("select f from Folder f where f.actor.id = ?1 and f.type.folderType = ?2")
	Folder getFolderByActorIdAndType(int actorId, String type);

	@Query("select f from Folder f join f.messages m where f.actor.id = ?1 and m.id = ?2")
	Folder getFolderByActorAndMessage(int actorId, int messageId);

	@Query("select f from Folder f where f.type = 'NOTIFICATIONBOX'")
	Collection<Folder> getAllNotificationFolders();

	@Query("select f from Folder f where f.type.folderType = 'USERBOX' AND f.actor.id = ?1 AND f.parentFolder = null")
	Collection<Folder> getUserParentFolders(int actorId);

	@Query("select f from Folder f where f.type.folderType = 'USERBOX' AND f.actor.id = ?1")
	Collection<Folder> getUserFolders(int actorId);

	@Query("select f from Folder f where f.parentFolder.id = ?1")
	Collection<Folder> getChildrenFolders(int folderId);

}
