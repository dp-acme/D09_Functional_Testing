package controlller;

import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import services.UserService;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/junitController.xml"})
@WebAppConfiguration
public class TestUserList {

	private MockMvc mockMvc;

	@Autowired
	private UserService userServiceMock;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setUp() {
		Mockito.reset(userServiceMock);

		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void findAll_FindAllUsers() throws Exception {
		User user1;
		User user2;
		
		user1 = new User();
		user2 = new User();		
		
		user1.setName("controllerUserTest1"); //Se presupone que definir m�s propiedades no cambia el resultado
		user2.setName("controllerUserTest2");
		user1.setSurname("controllerUserTest1");
		user2.setSurname("controllerUserTest1");
		
		when(userServiceMock.findAll()).thenReturn(new ArrayList<User>(Arrays.asList(user1, user2)));

		mockMvc.perform(get("/user/list.do")) //Se hace el get
			   .andExpect(status().isOk()) //Se comprueba que la petici�n fue correcta
			   .andExpect(view().name("user/list")) //Se comprueba el nombre de la vista
			   .andExpect(model().attribute("users", hasSize(2))) //Se comprueba que "users" tiene 2 elementos
			   .andExpect(model().attribute("users", hasItem(user1))) //Se comprueban ambos elementos
			   .andExpect(model().attribute("users", hasItem(user2)));
	}
}
