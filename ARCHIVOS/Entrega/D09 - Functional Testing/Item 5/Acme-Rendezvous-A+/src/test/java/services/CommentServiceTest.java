package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import services.ActorService;
import services.CommentService;
import services.RendezvousService;
import utilities.AbstractTest;
import domain.Actor;
import domain.Comment;
import domain.Rendezvous;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class CommentServiceTest extends AbstractTest{

	@Autowired
	private CommentService commentService;

	@Autowired
	private ActorService actorService;

	@Autowired
	private RendezvousService rendezvousService;
	
/*
	5. An actor who is authenticated as a user must be able to:
		- Comment on the rendezvouses that he or she has RSVPd.
*/
	@Test
	public void driverCreate(){
		Object[][] testingData = {
				//Positivos
				{"user1", "text", null, null, "rendezvous1", "user1", null}, //Creaci�n normal
				{"user1", "text", null, "comment1", "rendezvous1", "user1", null}, //Creaci�n con padre
				
				//Negativos
				{"user2", "text", null, null, "rendezvous1", "user1", IllegalArgumentException.class}, //Creaci�n con autenticaci�n inv�lida
				{"user1", "text", null, null, "rendezvous2", "user1", IllegalArgumentException.class}, //Creaci�n en rendezvous draft
				{"user1", "text", null, "comment3", "rendezvous1", "user1", IllegalArgumentException.class}, //Creaci�n con padre inv�lido (de otro rendezvous)
				{"user1", "text", null, "comment1", "rendezvous2", "user1", IllegalArgumentException.class}, //Creaci�n con rendezvous incorrecto
				
				{"user1", "", null, null, "rendezvous1", "user1", ConstraintViolationException.class}, //Restricciones del dominio
				{"user1", null, null, null, "rendezvous1", "user1", ConstraintViolationException.class},
				{"user1", "text", "notAnURL", null, "rendezvous1", "user1", ConstraintViolationException.class}
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2],
						   (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], 
						   (Class<?>) testingData[i][6]);
		}
	}
	

/*
	6. An actor who is authenticated as an administrator must be able to:
		- Remove a comment that he or she thinks is inappropriate.
*/	
	@Test
	public void driverDelete(){
		Object[][] testingData = {
				//Positivos
				{"admin", "comment1", null}, //Borrado normal
				
				//Negativos
				{"user1", "comment1", IllegalArgumentException.class} //Falta de credenciales
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateDelete((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void templateCreate(String username, String text, String picture, String parentBeanId, String rendezvousBeanId, String creatorBeanId, Class<?> expected){
		Class<?>  caught;
		Comment result;
		Comment parent;
		Actor user;
		Rendezvous rendezvous;

		int rendezvousId;		
		
		caught = null;
		
		try{			
			super.authenticate(username);
			
			user = actorService.findOne(super.getEntityId(creatorBeanId));
			
			Assert.notNull(user);
			Assert.isInstanceOf(User.class, user);
			
			rendezvousId = super.getEntityId(rendezvousBeanId);
			rendezvous = rendezvousService.findOne(rendezvousId);
			
			if(parentBeanId != null){
				parent = commentService.findOne(super.getEntityId(parentBeanId));
				Assert.notNull(parent);
				
			} else{
				parent = null;
			}
			
			result = commentService.create((User) user, parent, rendezvous);
			
			result.setText(text);
			result.setPicture(picture);
			
			commentService.save(result);
			commentService.flush();
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
	
	protected void templateDelete(String username, String commentBeanId, Class<?> expected){
		Class<?>  caught;
		
		Comment comment;
		
		caught = null;
		
		try{			
			super.authenticate(username);
			
			comment = commentService.findOne(super.getEntityId(commentBeanId));
			
			Assert.notNull(comment);
			
			commentService.flagAsDeleted(comment);
			commentService.flush();
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
}
