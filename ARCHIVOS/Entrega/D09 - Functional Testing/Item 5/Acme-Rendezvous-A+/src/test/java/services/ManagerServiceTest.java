package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import services.ManagerService;
import utilities.AbstractTest;
import domain.Manager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class ManagerServiceTest extends AbstractTest{

	@Autowired
	private ManagerService managerService;
	
/*
 	1. There’s a new kind of actor in the system: managers, who have a VAT number that is a string
of letters, numbers, and dashes. 
	
	3. An actor who is not authenticated must be able to:
		1. Register to the system as a manager
*/
	@Test
	public void driverCreate(){
		Calendar cal;
		Calendar futureCal;
		
		cal = Calendar.getInstance();
		futureCal = Calendar.getInstance();
		cal.set(1998, 11, 19);
		futureCal.set(2030, 11, 12);
		Object[][] testingData = {
				
				//Positivos
				{null, "usernameTest", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", "ABCD--1234", cal.getTime(), null},
				{null, "usernameTest1", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", "ABCD--1234", cal.getTime(), null},
				
				//Negativos

				{null, "user", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", "ABCD--1234", cal.getTime(), ConstraintViolationException.class},
				{null, "user1234123412341234123412341234", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", "ABCD--1234", cal.getTime(), ConstraintViolationException.class},
				{null, "userTest2", "pass", "nameTest", "surnameTest", "mail@test.test", "", "", "ABCD--1234", cal.getTime(), ConstraintViolationException.class},
				{null, "userTest2", "pass1234123412341234123412341234", "nameTest", "surnameTest", "mail@test.test", "", "", "ABCD--1234", cal.getTime(), ConstraintViolationException.class},
				{null, "userTest2", "passwordTest", "", "surnameTest", "mail@test.test", "", "", "ABCD--1234", cal.getTime(), ConstraintViolationException.class},
				{null, "userTest2", "passwordTest", "nameTest", "", "mail@test.test", "", "", "ABCD--1234", cal.getTime(), ConstraintViolationException.class},
				{null, "userTest2", "passwordTest", "nameTest", "surnameTest", "", "", "", "ABCD--1234", cal.getTime(), ConstraintViolationException.class},
				{null, "userTest2", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", "ABCD--1234", futureCal.getTime(), ConstraintViolationException.class},
				{null, "userTest2", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", "", futureCal.getTime(), ConstraintViolationException.class},
				{null, "userTest2", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", "[]***?", futureCal.getTime(), ConstraintViolationException.class},
				{null, "userTest2", "passwordTest", "nameTest", "surnameTest", "dklfjslkdjf", "", "", "ABCD--1234",futureCal.getTime(), ConstraintViolationException.class},
				{null, "userTest", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", "ABCD--1234",cal.getTime(), ConstraintViolationException.class},

		};
		
		for(int i = 0; i<testingData.length; i++){
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2],
						 (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5],
						 (String) testingData[i][6], (String) testingData[i][7], (String) testingData[i][8] ,(Date) testingData[i][9],(Class<?>) testingData[i][10]);
		}
	}

	protected void templateCreate(String username, String usernameToBeSetted, String password, String name,String surname, String email, String phone, String address, String vat, Date birthday, Class<?> expected){
		Class<?>  caught;
		
		Manager manager;
		UserAccount userAccount;
		Collection<Authority> auths;
		Authority auth;
	
		auth = new Authority();
		auths = new ArrayList<Authority>();
		auth.setAuthority(Authority.MANAGER);
		auths.add(auth);
		userAccount = new UserAccount();
		caught = null;
		
		try{			
			super.authenticate(username);
			
			manager = managerService.create();
			Assert.notNull(manager);

			manager.setName(name);
			manager.setSurname(surname);
			manager.setEmail(email);
			manager.setPhone(phone);
			manager.setVat(vat);
			manager.setAddress(address);
			manager.setBirthday(birthday);
			userAccount.setUsername(usernameToBeSetted);
			userAccount.setPassword(password);
			userAccount.setAuthorities(auths);
			manager.setUserAccount(userAccount);
				
			managerService.save(manager);
			managerService.flush();
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}	
}
