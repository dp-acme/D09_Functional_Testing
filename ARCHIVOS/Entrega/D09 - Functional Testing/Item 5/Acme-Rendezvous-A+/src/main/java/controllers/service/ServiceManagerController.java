package controllers.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.CategoryService;
import services.ServiceService;
import controllers.AbstractController;
import domain.Manager;
import domain.Request;
import domain.Service;

@Controller
@RequestMapping("/service/manager")
public class ServiceManagerController extends AbstractController {

	// Services------------------------------------------------------------

	@Autowired
	private ServiceService serviceService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ActorService actorService;

	// Constructor--------------------------------------------------------------------

	public ServiceManagerController() {
		super();
	}

	// List
	// -------------------------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list() {
		// Creamos el objeto a devolver
		ModelAndView result;

		Manager manager;

		manager = (Manager) actorService.findByUserAccountId(LoginService
				.getPrincipal().getId());

		// Creamos una colleccion para guardar los servicios
		Collection<Service> myServices;

		myServices = manager.getServices();

		result = new ModelAndView("service/manager/list");
		// Al modelo y la vista le a�adimos los siguientes atributos
		result.addObject("services", myServices);
		result.addObject("requestURI", "service/manager/list.do");

		return result;
	}

	// Edit------------------------------------------------------------
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam int serviceId) {
		ModelAndView result;
		Service service;
		Manager manager;

		manager = (Manager) actorService.findByUserAccountId(LoginService
				.getPrincipal().getId());
		// Comprobamos
		Assert.notNull(manager);

		service = serviceService.findOne(serviceId);

		Assert.notNull(service);
		Assert.isTrue(!service.isCancelled());
		// TODO: assert para cancelado

		Assert.isTrue(service.getManager().getUserAccount()
				.equals(LoginService.getPrincipal()));

		result = createEditModelAndView(service);

		// Introducimos en la vista de editar estos objetos
		Collection<Request> requests;
		requests = this.serviceService.getRequestsOfService(serviceId);

		result.addObject("requests", requests);
		return result;
	}

	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		Service service;
		Manager manager;

		manager = (Manager) actorService.findByUserAccountId(LoginService
				.getPrincipal().getId());
		// Comprobamos
		Assert.notNull(manager);
		service = serviceService.create();

		result = createEditModelAndView(service);
		result.addObject("service", service);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value="service") Service service, BindingResult binding) {
		ModelAndView result;
		Service serviceRes;

		serviceRes = serviceService.reconstruct(service, binding);

		if (binding.hasErrors()) {
			result = createEditModelAndView(serviceRes);
		} else {
			try {
				serviceRes = this.serviceService.save(serviceRes);
				result = new ModelAndView("redirect:/service/display.do?serviceId=" + serviceRes.getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(serviceRes, "service.commit.error");
			}
		}

		return result;
	}

	// Delete--------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(Service service, BindingResult binding) {
		ModelAndView result;
		Service serviceRes;
		// Solo puedo borrar si no tiene asociado ese servicio ningun rendezvous
		// ( si las requests de ese servicio estan vacias)
		// Cogemos las request de ese servicio
		Collection<Request> requests;
		requests = this.serviceService.getRequestsOfService(service.getId());

		// Assert para que no podamos borrar servicios que tengan request
		Assert.isTrue(requests.isEmpty());

		serviceRes = serviceService.findOne(service.getId());

		try {
			this.serviceService.delete(serviceRes);
			result = new ModelAndView("redirect:list.do");
		} catch (Throwable oops) {
			result = this.createEditModelAndView(serviceRes,
					"service.commit.error");
		}

		return result;
	}

	private ModelAndView createEditModelAndView(Service service) {
		ModelAndView result;

		result = createEditModelAndView(service, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Service service,
			String messageCode) {
		ModelAndView result;

		result = new ModelAndView("service/manager/edit");

		// Le a�adimos estos objetos a la vista
		result.addObject("allCategories", this.categoryService.findAll());
		result.addObject("service", service);
		result.addObject("message", messageCode);

		return result;
	}
}
