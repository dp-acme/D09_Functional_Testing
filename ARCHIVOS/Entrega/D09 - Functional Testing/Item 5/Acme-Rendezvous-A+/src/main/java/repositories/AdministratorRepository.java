package repositories;


import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Administrator;
import domain.Manager;
import domain.Rendezvous;
import domain.Service;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer>{

	//*****DASHBOARD*******
	
	@Query("select avg(u.myRendezvouses.size), sqrt(sum(u.myRendezvouses.size*u.myRendezvouses.size)/count(u.myRendezvouses.size)-avg(u.myRendezvouses.size)*avg(u.myRendezvouses.size)) from User u")
	Double[] getAvgSdRendezvousPerUser();
	
	@Query("select coalesce(((select count(*) from User u where u.myRendezvouses.size>0)/count(*)),-1) from User u where u.myRendezvouses.size=0")
	Double ratioCreatorsOverNonCreators();
	
	@Query("select avg((r.users.size)+1), sqrt(sum(((r.users.size)+1)*((r.users.size)+1))/(count(r.users.size))-avg((r.users.size)+1)*avg((r.users.size)+1)) from Rendezvous r")
	Double[] getAvgSdUsersPerRendezvous();
	
	@Query("select avg(u.myRSVPs.size), sqrt(sum(u.myRSVPs.size*u.myRSVPs.size)/count(u.myRSVPs.size)-avg(u.myRSVPs.size)*avg(u.myRSVPs.size)) from User u")
	Double[] getAvgSdRSVPsPerUser();

	@Query("select r from Rendezvous r order by r.users.size DESC")
	Page<Rendezvous> get10RendezvousesOrderedByUsers(Pageable pageable);
	
	@Query("select avg(r.announcements.size), sqrt(sum(r.announcements.size*r.announcements.size)/count(r.announcements.size)-avg(r.announcements.size)*avg(r.announcements.size)) from Rendezvous r")
	Double[] getAvgSdAnnoucementsPerRendezvous();
	
	@Query("select r from Rendezvous r where r.announcements.size > (select avg(rv.announcements.size) from Rendezvous rv)*0.75 order by r.announcements.size DESC")
	Collection<Rendezvous> getRendezvousWithOver75percentAnnoucements();
	
	@Query("select r from Rendezvous r where r.links.size > (select avg(coalesce(rv.links.size,0)) from Rendezvous rv)*1.1 order by r.links.size DESC")
	Collection<Rendezvous> getRendezvousLinkedOver10percentRendezvouses();
	
	@Query("select avg(r.questions.size), sqrt(sum(r.questions.size*r.questions.size)/count(r.questions.size)-avg(r.questions.size)*avg(r.questions.size)) from Rendezvous r")
	Double[] getAvgSdQuestionsPerRendezvous();
	
	@Query("select avg(c.replies.size), sqrt(sum(c.replies.size*c.replies.size)/count(c.replies.size)-avg(c.replies.size)*avg(c.replies.size)) from Comment c")
	Double[] getAvgSdRepliesPerComment();
	
	@Query("select avg(cast((select coalesce(sum(q.answers.size),0) from Question q where q.rendezvous = r)as float)), sqrt(sum(cast((select coalesce(sum(q.answers.size),0) from Question q where q.rendezvous = r)as float)*cast((select coalesce(sum(q.answers.size),0) from Question q where q.rendezvous = r)as float))/count(r)-avg(cast((select coalesce(sum(q.answers.size),0) from Question q where q.rendezvous = r)as float))*avg(cast((select coalesce(sum(q.answers.size),0) from Question q where q.rendezvous = r)as float))) from Rendezvous r")
	Double[] getAvgSdAnswersPerRendezvous();

// ------------- D09 ------------------
	
	@Query("select s from Service s order by coalesce((select count(r) from Request r where r.service=s),0) DESC")
	Collection<Service> getTopSellingServices();
	
	@Query("select s from Service s where (select count(r) from Request r where r.service=s)=(select max(coalesce((select count(r) from Request r where r.service=sv),0)) from Service sv)")
	Page<Service> getBestSellingServices(Pageable pageable);

	@Query("select m from Manager m where m.services.size > (select avg(coalesce(m.services.size,0))from Manager m)")
	Collection<Manager> getManagersWithMoreServicesThanAvg();
	
	@Query("select m from Manager m order by coalesce((select count(s) from Service s where s.manager=m AND s.cancelled=true),0) DESC")
	Page<Manager> getManagersWithMoreServicesCancelled(Pageable pageable);
	
	@Query("select avg(coalesce((select count(s) from Service s where c.id in elements(s.categories)),0)) from Category c")
	Double getAvgServicesPerCategory();
		
	@Query("select avg(coalesce((select count(r) from Request r where r.rendezvous=rv),0)), min(coalesce((select count(r) from Request r where r.rendezvous=rv),0)), max(coalesce((select count(r) from Request r where r.rendezvous=rv),0)),sqrt(sqrt(sum(coalesce((select count(r) from Request r where r.rendezvous=rv),0)*coalesce((select count(r) from Request r where r.rendezvous=rv),0))/(select count(rv2) from Rendezvous rv2)-avg(coalesce((select count(r) from Request r where r.rendezvous=rv),0))*avg(coalesce((select count(r) from Request r where r.rendezvous=rv),0)))) from Rendezvous rv")
	Double[] getAvgMaxMinSdRequestsPerRendezvous();

	@Query(value = "select avg(coalesce((select count(distinct c) from Category c, Request r where c in elements (r.service.categories) AND r.rendezvous=rv),0)) from Rendezvous rv")
	Double getAvgCategoriesPerRendezvous();
	
	
}