package services;

import java.text.DecimalFormat;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Administrator;
import domain.Manager;
import domain.Rendezvous;

@Service
@Transactional
public class AdministratorService {
	// Managed repository ---------------------------------------------------
	@Autowired
	private AdministratorRepository administratorRepository;
	
	// Supporting services ---------------------------------------------------
	
	// Constructor ---------------------------------------------------
	
	public AdministratorService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	public Administrator findOne(int administratorId) {
		Assert.isTrue(administratorId != 0);
		
		Administrator result;
		
		result = administratorRepository.findOne(administratorId);
		
		return result;
	}
	
	public Administrator save(Administrator administrator) {
		Assert.notNull(administrator);
		Administrator result;
		UserAccount userAccount;
		Md5PasswordEncoder encoder;
		Authority auth;
		
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(administrator.getUserAccount().equals(userAccount));
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		encoder = new Md5PasswordEncoder();
		administrator.getUserAccount().setPassword(encoder.encodePassword(administrator.getUserAccount().getPassword(),null));
		
		result = administratorRepository.save(administrator);

		return result;
	}
	
	// ***********DASHBOARD*************** TODO: Arreglar consultas
	
	public 	Double[] getAvgSdRendezvousPerUser(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgSdQuestionsPerRendezvous();
		
		return res;
	}
	
	public Double ratioCreatorsOverNonCreators(){
		Authority auth;
		UserAccount userAccount;
		Double res; 
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.ratioCreatorsOverNonCreators();
		
		return res;
	}
	
	public 	Double[] getAvgSdUsersPerRendezvous(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgSdUsersPerRendezvous();
		
		return res;
	}
	
	public 	Double[] getAvgSdRSVPsPerUser(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgSdRSVPsPerUser();
		
		return res;
	}
	
	public 	Double[] getAvgSdAnnoucementsPerRendezvous(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgSdAnnoucementsPerRendezvous();
		
		return res;
	}
	
	public 	Double[] getAvgSdQuestionsPerRendezvous(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgSdQuestionsPerRendezvous();
		
		return res;
	}
	
	public 	Double[] getAvgSdRepliesPerComment(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgSdRepliesPerComment();
		
		return res;
	}
	
	
	public 	Double[] getAvgSdAnswersPerRendezvous(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgSdAnswersPerRendezvous();
		
		return res;
	}
	
	
	public Collection<Rendezvous> get10RendezvousesOrderedByUsers(){
		Authority auth;
		UserAccount userAccount;
		Collection<Rendezvous> res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		Pageable pageable = new PageRequest(0,10);
		
		res = administratorRepository.get10RendezvousesOrderedByUsers(pageable).getContent();
		
		return res;
		
	}
	
	public Collection<Rendezvous> getRendezvousWithOver75percentAnnoucements(){
		Authority auth;
		UserAccount userAccount;
		Collection<Rendezvous> res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getRendezvousWithOver75percentAnnoucements();
		
		return res;
		
	}
	
	public Collection<Rendezvous> getRendezvousLinkedOver10percentRendezvouses(){
		Authority auth;
		UserAccount userAccount;
		Collection<Rendezvous> res;
	
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getRendezvousLinkedOver10percentRendezvouses();
		
		return res;
		
	}
	
// -------------- D09 ------------------------

	
	public Collection<domain.Service> getBestSellingServices(){
		Authority auth;
		UserAccount userAccount;
		Pageable pageable;
		Collection<domain.Service> res;
		
		pageable = new PageRequest(0,10);
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getBestSellingServices(pageable).getContent();
		
		return res;
		
	}
	
	public Collection<domain.Service> getTopSellingServices(){
		Authority auth;
		UserAccount userAccount;
		 Collection<domain.Service> res;
	
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getTopSellingServices();
		
		return res;
		
	}
	
	public Collection<Manager> getManagersWithMoreServicesThanAvg(){
		Authority auth;
		UserAccount userAccount;
		Collection<Manager> res;
	
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getManagersWithMoreServicesThanAvg();
		
		return res;
		
	}
	
	public Collection<Manager> getManagersWithMoreServicesCancelled(){
		Authority auth;
		UserAccount userAccount;
		Pageable pageable;
		Collection<Manager> res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		pageable = new PageRequest(0,10);
		
		res = administratorRepository.getManagersWithMoreServicesCancelled(pageable).getContent();
		
		return res;
		
	}
	
	public 	Double getAvgServicesPerCategory(){
		Authority auth;
		UserAccount userAccount;
		Double res;

		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgServicesPerCategory();
		
		return res;
	}
	
	public 	Double[] getAvgMaxMinSdRequestsPerRendezvous(){
		Authority auth;
		UserAccount userAccount;
		Double[] res;
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgMaxMinSdRequestsPerRendezvous();
		
		return res;
	}
	
	public 	Double getAvgCategoriesPerRendezvous(){
		Authority auth;
		UserAccount userAccount;
		Double res;

		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		userAccount = LoginService.getPrincipal();
		Assert.isTrue(userAccount.getAuthorities().contains(auth));
		
		res = administratorRepository.getAvgCategoriesPerRendezvous();
		
		return res;
	}
	
	
	
	
	
	
	
	//-----REDONDEO
	
	public String roundTo(Double n, Integer decimals){
		DecimalFormat format;
		String result;
		if(n==null){
			result = "No hay datos suficientes";
		}else{
		
			format = new DecimalFormat();
			format.setMaximumFractionDigits(decimals);
			result = format.format(n);
		}
		
		return result;
	}
	public String roundToRatio(Double n, Integer decimals){
		return n==-1 ? "No hay usuarios sin Rendezvous creados" : roundTo(n,decimals);
	}
	public String[] roundTo(Double[] ns, Integer decimals){
		String[] result;
		
		
		result = new String[ns.length];
		
		for(int i = 0; i < result.length; i ++){
			result[i] = roundTo(ns[i], decimals);
		}
		
		return result;
	}
	

	
	// Other business methods -------------------------------------------------

}
