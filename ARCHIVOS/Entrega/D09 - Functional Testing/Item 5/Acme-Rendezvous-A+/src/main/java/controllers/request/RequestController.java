package controllers.request;

import java.util.Collection;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.RendezvousService;
import services.RequestService;
import services.ServiceService;
import controllers.AbstractController;
import domain.Actor;
import domain.Manager;
import domain.Rendezvous;
import domain.Request;
import domain.Service;
import domain.User;

@Controller
@RequestMapping("/request")
public class RequestController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private RendezvousService rendezvousService;
	
	@Autowired
	private RequestService requestService;
	
	@Autowired
	private ServiceService serviceService;
	
	// Constructor -----------------------------------------------------------

	public RequestController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		Manager manager;
		Collection<Request> requests;
		boolean isAdult;
		
		manager = (Manager) actorService.findPrincipal();
		
		isAdult = actorService.isAdult(manager);

		requests = requestService.findRequestsByManager(manager.getId());
		
		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("isAdult", isAdult);

		return result;
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = true) int serviceId, HttpServletRequest httpRequest){
		ModelAndView result;
		Request request;
		Service service;
		Collection<Rendezvous> rendezvouses;
		User user;
		
		user = (User) actorService.findPrincipal();
		
		service = serviceService.findOne(serviceId);
		Assert.notNull(service);
		
		rendezvouses = rendezvousService.getMyRendezvousNoDraftNoDeletedNoService(user, service);
		Assert.isTrue(rendezvouses.size() > 0);
		
		request = requestService.create(service);
		
		result = this.createEditModelAndView(request, httpRequest);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@RequestParam(required = true) int serviceId , @ModelAttribute(value="request") Request request, BindingResult binding, 
			HttpServletResponse response, HttpServletRequest httpRequest){
		ModelAndView result;
		
		request = requestService.reconstruct(request, binding, serviceId);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(request, null);
		} else { 
			try {
				request = requestService.save(request);
				setCreditCardCookies(request, response);
				
				result = new ModelAndView("redirect:/service/display.do?serviceId="+request.getService().getId());
				
			} catch (Throwable oops) {
				result = createEditModelAndView(request, null, "request.commit.error");
			}
		}
		return result;
	}
	
	// Ancilliary methods -----------------------------------------------------------
	
	protected ModelAndView createEditModelAndView(Request request, HttpServletRequest httpRequest) {
		ModelAndView result; 
		
		result = createEditModelAndView(request, httpRequest, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(Request request, HttpServletRequest httpRequest, String messageCode) {
		ModelAndView result;
		
		Collection<Rendezvous> rendezvouses;
		Actor actor;
		User user;
		
		actor = actorService.findPrincipal();
		Assert.isInstanceOf(User.class, actor);
		
		user = (User) actor;
		
		rendezvouses = rendezvousService.getMyRendezvousNoDraftNoDeletedNoService(user, request.getService());
		
		if (httpRequest != null) {
			request = getCreditCardCookies(request, httpRequest);
		}
		
		result = new ModelAndView("request/create");
		result.addObject("request", request);
		result.addObject("message", messageCode);
		result.addObject("rendezvouses", rendezvouses);
		result.addObject("hasErrors", httpRequest == null);
		
		return result;
	}

	private void setCreditCardCookies(Request request, HttpServletResponse response) {
		Assert.notNull(request);
		
		response.addCookie(new Cookie("lastRequest_user"+String.valueOf(LoginService.getPrincipal().getId()), String.valueOf(request.getId())));
	}
	
	private Request getCreditCardCookies(Request request, HttpServletRequest httpRequest) {
		Assert.notNull(request);
		
		Request lastRequest;
		
		lastRequest = null;
		
		Cookie[] cookies = httpRequest.getCookies();
		
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("lastRequest_user"+String.valueOf(LoginService.getPrincipal().getId()))) {
				lastRequest = requestService.findOne(Integer.parseInt(cookie.getValue()));
				break;
			}
		}
		
		if (lastRequest != null) {
			Assert.isTrue(lastRequest.getRendezvous().getCreator().getUserAccount().getId() == LoginService.getPrincipal().getId());
			request.setHolderName(lastRequest.getHolderName());
			request.setBrandName(lastRequest.getBrandName());
			request.setCreditCardNumber(lastRequest.getCreditCardNumber());
			request.setCvv(lastRequest.getCvv());
			request.setExpirationMonth(lastRequest.getExpirationMonth());
			request.setExpirationYear(lastRequest.getExpirationYear());
		}
		
		return request;
	}
	
}
