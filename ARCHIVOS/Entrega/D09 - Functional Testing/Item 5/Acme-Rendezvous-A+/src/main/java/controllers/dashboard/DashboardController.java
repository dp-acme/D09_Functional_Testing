/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;
import controllers.AbstractController;

@Controller
@RequestMapping("/dashboard")
public class DashboardController extends AbstractController {

	// Services -----------------------------------------------------------
	
	@Autowired
	AdministratorService administratorService;
	
	// Constructors -----------------------------------------------------------

	public DashboardController() {
		super();
	}

	@RequestMapping("/display")
	public ModelAndView display() {
		String[]avgSdRendezvousPerUser, avgSdUsersPerRendezvous, 
			avgSdRSVPsPerUser, avgSdAnnoucementsPerRendezvous, 
			avgSdQuestionsPerRendezvous, avgSdRepliesPerComment, 
			avgSdAnswersPerRendezvous, avgMaxMinSdRequestsPerRendezvous;
		Integer round;
		
		round = 2;
		
		String ratioCreatorsOverNonCreators, avgCategoriesPerRendezvous,
				avgServicesPerCategory;
		
		ModelAndView result;

		result = new ModelAndView("dashboard");
		
		avgSdRendezvousPerUser = administratorService.roundTo(administratorService.getAvgSdRendezvousPerUser(), round);
		ratioCreatorsOverNonCreators = administratorService.roundToRatio(administratorService.ratioCreatorsOverNonCreators(), round);
		avgSdUsersPerRendezvous = administratorService.roundTo(administratorService.getAvgSdUsersPerRendezvous(), round);
		avgSdRSVPsPerUser = administratorService.roundTo(administratorService.getAvgSdRSVPsPerUser(), round);
		avgSdAnnoucementsPerRendezvous = administratorService.roundTo(administratorService.getAvgSdAnnoucementsPerRendezvous(), round);
		avgSdQuestionsPerRendezvous = administratorService.roundTo(administratorService.getAvgSdQuestionsPerRendezvous(), round);
		
		avgSdRepliesPerComment = administratorService.roundTo(administratorService.getAvgSdRepliesPerComment(), round);
		avgSdAnswersPerRendezvous = administratorService.roundTo(administratorService.getAvgSdAnswersPerRendezvous(), round);

		avgServicesPerCategory = administratorService.roundTo(administratorService.getAvgServicesPerCategory(),round);
		avgCategoriesPerRendezvous = administratorService.roundTo(administratorService.getAvgCategoriesPerRendezvous(), round);
		avgMaxMinSdRequestsPerRendezvous = administratorService.roundTo(administratorService.getAvgMaxMinSdRequestsPerRendezvous(),round);
		
		result.addObject("avgCategoriesPerRendezvous", avgCategoriesPerRendezvous);
		result.addObject("avgServicesPerCategory", avgServicesPerCategory);
		
		result.addObject("avgMaxMinSdRequestsPerRendezvous", avgMaxMinSdRequestsPerRendezvous);
		result.addObject("avgSdRendezvousPerUser", avgSdRendezvousPerUser);
		result.addObject("ratioCreatorsOverNonCreators", ratioCreatorsOverNonCreators);
		result.addObject("avgSdUsersPerRendezvous", avgSdUsersPerRendezvous);
		result.addObject("avgSdRSVPsPerUser", avgSdRSVPsPerUser);
		result.addObject("avgSdAnnoucementsPerRendezvous", avgSdAnnoucementsPerRendezvous);
		result.addObject("avgSdQuestionsPerRendezvous", avgSdQuestionsPerRendezvous);
		
		result.addObject("avgSdRepliesPerComment", avgSdRepliesPerComment);
		result.addObject("avgSdAnswersPerRendezvous", avgSdAnswersPerRendezvous);
		
		result.addObject("10RendezvousesOrderedByUsers", administratorService.get10RendezvousesOrderedByUsers());
		result.addObject("rendezvousWithOver75percentAnnoucements", administratorService.getRendezvousWithOver75percentAnnoucements());
		result.addObject("rendezvousLinkedOver10percentRendezvouses", administratorService.getRendezvousLinkedOver10percentRendezvouses());
		result.addObject("bestSellingServices",administratorService.getBestSellingServices());
		result.addObject("topSellingServices", administratorService.getTopSellingServices());
		result.addObject("managersWithMoreServicesThanAvg", administratorService.getManagersWithMoreServicesThanAvg());
		result.addObject("managersWithMoreServicesCancelled", administratorService.getManagersWithMoreServicesCancelled());
		
		return result;
	}

}