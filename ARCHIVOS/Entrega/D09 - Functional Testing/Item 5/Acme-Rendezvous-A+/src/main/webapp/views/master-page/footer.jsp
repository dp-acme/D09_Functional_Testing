<%--
 * footer.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:useBean id="date" class="java.util.Date" />

<br />
<hr />

<a href="welcome/faq.do"><spring:message code="master.page.faq" /></a> | 
<a href="welcome/aboutUs.do"><spring:message code="master.page.about" /></a> | 
<a href="welcome/termsAndConditions.do"><spring:message code="master.page.terms" /></a>

<br />

<b>Copyright &copy; <fmt:formatDate value="${date}" pattern="yyyy" /> Sample Co., Inc.</b>

<input type="hidden" id="cookieAlertMessage" value="<spring:message code="master.page.cookieAlertMessage"/>" />

<script>	
	if(isAlertSet()){
		console.log("a");
		if(confirm(document.getElementById("cookieAlertMessage").value)){
			turnAlertOff();
		} else{
			window.location.replace("http://www.google.com");
		}
	}
</script>