<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="org.springframework.security.core.GrantedAuthority"%>
<%@page import="security.Authority"%>
<%@page import="security.UserAccount"%>

<%@page
	import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="security.LoginService"%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<!-- Name -->
<h2>
	<jstl:out value="${service.getName()}" />
</h2>

<jstl:if test="${not empty service.getPicture()}" >
	<img src="${service.getPicture()}" width="300" />
	<br />
	<br />
</jstl:if>

<!-- Description -->
<spring:message code="service.display.description" />:
		<jstl:out value="${service.getDescription()}" />

<jstl:if test="${not empty service.getCategories()}">
<br/>
<br/>	
<spring:message code="service.display.category" />:
		<jstl:forEach items="${service.getCategories()}" var="cat">
		<jstl:out value="${cat.getName()}" />
		</jstl:forEach>
</jstl:if>


<security:authorize access="hasRole('USER')">

	<br/>
	<br/>
	<jstl:if test="${canRequest}">
		<acme:cancel url="request/create.do?serviceId=${service.getId()}" code="service.display.request"/>
	</jstl:if>
	
	<jstl:if test="${!canRequest}">
		<spring:message code="service.display.cantRequest" />
	</jstl:if>

</security:authorize>

