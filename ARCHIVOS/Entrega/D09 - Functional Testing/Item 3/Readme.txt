Para la configuraci�n del protocolo HTTPS
se debe de seguir la gu�a descrita en nuestro entregable D08 - Lessons Learnt.

Las modificaciones necesarias para implementar dicho protocolo
se realizan sobre los archivos de configuraci�n del servidor, 
debido a esto en el proyecto no se refleja ning�n cambio con respecto a la versi�n sin HTTPS.