package services;

import java.util.Collection;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import services.CategoryService;
import utilities.AbstractTest;
import domain.Category;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class CategoryServiceTest extends AbstractTest{

	@Autowired
	private CategoryService categoryService;
	
/*
	11. An actor who is authenticated as an administrator must be able to:
		- Manage the categories of services, which includes listing, creating, updating, deleting, and re-organising them in the category hierarchies.
*/
	@Test
	public void driverList(){
		Collection<Category> result;
		
		result = categoryService.findParentCategories();
		
		Assert.notNull(result);
		Assert.notEmpty(result);
	}
	
/*
	11. An actor who is authenticated as an administrator must be able to:
		- Manage the categories of services, which includes listing, creating, updating, deleting, and re-organising them in the category hierarchies.
*/
	@Test
	public void driverCreate(){
		Object[][] testingData = {
				//Positivos
				{"admin", "name", "description", null, null}, //Creaci�n normal
				{"admin", "name", "description", "category1", null},
				
				//Negativos
				{"admin", "category2", "description", "category1", IllegalArgumentException.class}, //Nombre repetido
				{"admin", "category1", "description", null, IllegalArgumentException.class}, //Nombre repetido (sin padre)
				{"user1", "name2", "description", null, IllegalArgumentException.class}, //Falta de credenciales	

				{"admin", "", "description", "category1", ConstraintViolationException.class}, //Restricciones del dominio
				{"admin", null, "description", "category1", ConstraintViolationException.class},
				{"admin", "name3", "", "category1", ConstraintViolationException.class},
				{"admin", "name4", null, "category1", ConstraintViolationException.class},
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2],
						   (String) testingData[i][3], (Class<?>) testingData[i][4]);
		}
	}
	
/*
	11. An actor who is authenticated as an administrator must be able to:
		- Manage the categories of services, which includes listing, creating, updating, deleting, and re-organising them in the category hierarchies.
*/
	@Test
	public void driverEdit(){
		Object[][] testingData = {
				//Positivos
				{"admin", "category1", "name2", "description", null}, //Edici�n normal
				
				//Negativos
				{"admin", "category2", "category3", "description", IllegalArgumentException.class}, //Nombre repetido
				{"admin", "category5", "name2", "description", IllegalArgumentException.class}, //Nombre repetido (sin padre)
				{"user1", "category1", "name3", "description", IllegalArgumentException.class}, //Falta de credenciales	

				{"admin", "category1", "", "description", ConstraintViolationException.class}, //Restricciones del dominio
				{"admin", "category1", null, "description", ConstraintViolationException.class},
				{"admin", "category1", "name4", "", ConstraintViolationException.class},
				{"admin", "category1", "name5", null, ConstraintViolationException.class},
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2],
						 (String) testingData[i][3], (Class<?>) testingData[i][4]);
		}
	}

	/*
	11. An actor who is authenticated as an administrator must be able to:
		- Manage the categories of services, which includes listing, creating, updating, deleting, and re-organising them in the category hierarchies.
*/
	@Test
	public void driverDelete(){
		Object[][] testingData = {
				//Positivos
				{"admin", "category1", null}, //Borrado normal
				
				//Negativos
				{"user1", "category1", IllegalArgumentException.class} //Falta de credenciales
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateDelete((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}
	
/*
	11. An actor who is authenticated as an administrator must be able to:
		- Manage the categories of services, which includes listing, creating, updating, deleting, and re-organising them in the category hierarchies.
*/
	@Test
	public void driverMove(){
		Object[][] testingData = {
				//Positivos
				{"admin", "category1", "category6", null}, //Movimiento normal
				
				//Negativos
				{"user1", "category1", "category6", IllegalArgumentException.class}, //Falta de credenciales
				{"admin", "category1", "category1", IllegalArgumentException.class}, //Movimiento a s� mismo
				{"admin", "category1", "category2", IllegalArgumentException.class}, //Movimiento a hijo
				{"admin", "category2", "category1", IllegalArgumentException.class}, //Movimiento sin efecto
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateMove((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
		}
	}

	protected void templateCreate(String username, String name, String description, String parentBeanId, Class<?> expected){
		Class<?>  caught;
		
		Category category;
		Category parent;
		
		caught = null;
		
		try{			
			super.authenticate(username);
			
			if(parentBeanId != null){
				parent = categoryService.findOne(super.getEntityId(parentBeanId));
				Assert.notNull(parent);
				
			} else{
				parent = null;
			}
			
			category = categoryService.create(parent);
			
			category.setName(name);
			category.setDescription(description);
			
			categoryService.save(category);
			categoryService.flush();
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
	
	protected void templateEdit(String username, String categoryBeanName, String name, String description, Class<?> expected){
		Class<?>  caught;
		
		Category category;
		
		caught = null;
		
		try{			
			super.authenticate(username);
			
			category = categoryService.findOne(super.getEntityId(categoryBeanName));
			Assert.notNull(category);
			
			category.setName(name);
			category.setDescription(description);
			
			categoryService.save(category);
			categoryService.flush();
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
	
	protected void templateDelete(String username, String categoryBeanId, Class<?> expected){
		Class<?>  caught;
		
		Category category;
		
		caught = null;
		
		try{			
			super.authenticate(username);
			
			category = categoryService.findOne(super.getEntityId(categoryBeanId));
			
			Assert.notNull(category);
			
			categoryService.delete(category);
			categoryService.flush();
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
	
	protected void templateMove(String username, String categoryBeanId, String newParentBeanId, Class<?> expected){
		Class<?>  caught;
		
		Integer categoryId;
		Integer newParentId;
		
		caught = null;
		
		try{			
			super.authenticate(username);
			
			categoryId = super.getEntityId(categoryBeanId);
			newParentId = super.getEntityId(newParentBeanId);
			
			Assert.notNull(categoryService.findOne(categoryId));
			Assert.notNull(categoryService.findOne(newParentId));
			
			categoryService.moveCategory(categoryId, newParentId);
			categoryService.flush();
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
}
