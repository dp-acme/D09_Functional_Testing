<?xml version="1.0" encoding="UTF-8"?>

<!-- 
 * security.xml
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 -->

<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:security="http://www.springframework.org/schema/security"	
	xsi:schemaLocation="
		http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd		
        http://www.springframework.org/schema/security http://www.springframework.org/schema/security/spring-security-3.2.xsd
    ">

	<!-- Security infrastructure -->

	<bean id="loginService" class="security.LoginService" />

	<bean id="passwordEncoder"
		class="org.springframework.security.authentication.encoding.Md5PasswordEncoder" />

	<!-- Access control -->

	<security:http auto-config="true" use-expressions="true">
		<security:intercept-url pattern="/" access="permitAll" /> 

		<security:intercept-url pattern="/favicon.ico" access="permitAll" /> 
		<security:intercept-url pattern="/images/**" access="permitAll" /> 
		<security:intercept-url pattern="/scripts/**" access="permitAll" /> 
		<security:intercept-url pattern="/styles/**" access="permitAll" /> 

		<security:intercept-url pattern="/views/misc/index.jsp" access="permitAll" /> 

		<security:intercept-url pattern="/security/login.do" access="permitAll" /> 
		<security:intercept-url pattern="/security/loginFailure.do" access="permitAll" /> 

		<security:intercept-url pattern="/welcome/index.do" access="permitAll" />
		<security:intercept-url pattern="/welcome/faq.do" access="permitAll" />
		<security:intercept-url pattern="/welcome/aboutUs.do" access="permitAll" />
		<security:intercept-url pattern="/welcome/termsAndConditions.do" access="permitAll" />

		<security:intercept-url pattern="/announcement/list.do" access="permitAll" />
		<security:intercept-url pattern="/announcement/create.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/announcement/delete.do" access="hasAnyRole('USER', 'ADMIN')" />
		
		<security:intercept-url pattern="/dashboard/display.do" access="hasRole('ADMIN')" />
		
		<security:intercept-url pattern="/user/display.do" access="permitAll()" />
		<security:intercept-url pattern="/user/list.do" access="permitAll()" />
		<security:intercept-url pattern="/user/create.do" access="isAnonymous()" />
		<security:intercept-url pattern="/manager/create.do" access="isAnonymous()" />
		
		<security:intercept-url pattern="/rendezvous/display.do" access="permitAll" />
		<security:intercept-url pattern="/rendezvous/list.do" access="permitAll" />
		<security:intercept-url pattern="/rendezvous/listRendezvousCategory.do" access="permitAll" />
		<security:intercept-url pattern="/rendezvous/user/list.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/rendezvous/user/edit.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/rendezvous/user/create.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/rendezvous/admin/delete.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/rendezvous/user/delete.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/rendezvous/user/publish.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/rendezvous/user/myRsvp.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/rendezvous/user/linkSimilars.do" access="hasRole('USER')" />
		
		
		<security:intercept-url pattern="/comment/user/edit.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/comment/admin/delete.do" access="hasRole('ADMIN')" />
		
		<security:intercept-url pattern="/question/list.do" access="permitAll" />
		<security:intercept-url pattern="/question/user/delete.do" access="hasRole('USER')"  />
		<security:intercept-url pattern="/question/user/create.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/question/user/edit.do" access="hasRole('USER')"  />
		
		<security:intercept-url pattern="/answer/user/create.do" access="hasRole('USER')"  />
		<security:intercept-url pattern="/answer/user/edit.do" access="hasRole('USER')"  />
		<security:intercept-url pattern="/answer/user/delete.do" access="hasRole('USER')"  />
		<security:intercept-url pattern="/answer/list.do" access="permitAll" />
		
		<security:intercept-url pattern="/configuration/display.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/configuration/edit.do" access="hasRole('ADMIN')" />
		
		<security:intercept-url pattern="/service/list.do" access="hasAnyRole('ADMIN', 'USER', 'MANAGER')" />
		<security:intercept-url pattern="/service/manager/edit.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/service/manager/delete.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/service/manager/create.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/service/manager/list.do" access="hasRole('MANAGER')" />
		<security:intercept-url pattern="/service/cancel.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/service/display.do" access="hasAnyRole('ADMIN', 'USER', 'MANAGER')" />
		
		<security:intercept-url pattern="/request/create.do" access="hasRole('USER')" />
		<security:intercept-url pattern="/request/list.do" access="hasRole('MANAGER')" />
		
		<security:intercept-url pattern="/category/list.do" access="permitAll" />
		<security:intercept-url pattern="/category/admin/save.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/category/admin/delete.do" access="hasRole('ADMIN')" />
		<security:intercept-url pattern="/category/admin/move.do" access="hasRole('ADMIN')" />

		<security:intercept-url pattern="/**" access="hasRole('NONE')" />

		<security:form-login 
			login-page="/security/login.do"
			password-parameter="password" 
			username-parameter="username"
			authentication-failure-url="/security/loginFailure.do" />

		<security:logout 
			logout-success-url="/" 
			invalidate-session="true" />
	</security:http>

</beans>