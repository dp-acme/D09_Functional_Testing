package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.RequestRepository;
import domain.Actor;
import domain.Administrator;
import domain.Request;
import domain.Service;
import domain.User;

@org.springframework.stereotype.Service
@Transactional
public class RequestService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private RequestRepository requestRepository;

	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ServiceService serviceService;
	
	// Constructor------------------------------------------------------------------------

	public RequestService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Request create(Service service) {
		Assert.notNull(service);
		Assert.isTrue(!service.isCancelled());
		
		// Comprobamos que el usuario logeado es un user
		actorService.checkIsUser();

		// Creamos el resultado
		Request result;
		result = new Request();
		
		// Inicializamos los atributos
		result.setService(service);

		return result;
	}

	public Request findOne(int requestId) {
		// Comprobamos que el objeto que le pasamos no es nulo
		Assert.isTrue(requestId != 0);
		
		// Creamos el objeto a devolver
		Request result;
		
		// Cogemos del repositorio el Request que le pasamos como parametro
		result = this.requestRepository.findOne(requestId);
		Assert.notNull(result);
		
		return result;
	}

	public Collection<Request> findAll() {
		// Creamos el objeto a devolver
		Collection<Request> result;
		
		// Cogemos del repositorio los Request
		result = this.requestRepository.findAll();
		
		return result;
	}

	public Request save(Request request) {
		Assert.notNull(request);
		Assert.isTrue(!request.getService().isCancelled());
		Assert.isTrue(!request.getRendezvous().isDeleted());
		Assert.isTrue(request.getRendezvous().getMoment().after(new Date()));
		Assert.isNull(getRequestFromRendezvousAndService(request.getRendezvous().getId(), request.getService().getId()));
		
		Actor actor = actorService.findPrincipal();
		if (actor instanceof User) {
			User user;
			user = (User) actor;
			
			Assert.isTrue(user.equals(request.getRendezvous().getCreator()));
		} else {
			// TODO: Si es admin y cancela el Service o el Rendezvous, elimina/cancela la request?
			Assert.isInstanceOf(Administrator.class, actor);
		}
		
		// Creamos el objeto a devolver
		Request result;

		// Actualizamos el request
		result = this.requestRepository.save(request);

		return result;
	}

	// Other business methods ---------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	public Request reconstruct(Request request, BindingResult binding, int serviceId) {
		Request result;
		Service service;
		
		service = serviceService.findOne(serviceId);
		Assert.notNull(service);
		
		result = request;
		result.setService(service);
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public Request getRequestFromRendezvousAndService(int rendezvousId, int serviceId) {
		Request result;
		
		result = requestRepository.getRequestFromRendezvousAndService(rendezvousId, serviceId);
		
		return result;
	}
	
	public Collection<Request> getRequestsFromRendezvous(int rendezvousId) {
		Collection<Request> result;
		
		result = requestRepository.getRequestsFromRendezvous(rendezvousId);
		
		return result;
	}
	
	public Collection<Request> getRequestsFromService(int serviceId) {
		Collection<Request> result;
		
		result = requestRepository.getRequestsFromService(serviceId);
		
		return result;
	}

	public Collection<Request> findRequestsByManager(int managerId) {
		Assert.isTrue(managerId != 0);
		
		Collection<Request> result;
		
		result = requestRepository.findRequestsByManager(managerId);
		
		return result;
	}

	public void flush()
	{
		requestRepository.flush();
	}
	
}
