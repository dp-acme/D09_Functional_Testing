
package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.AnnouncementRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Announcement;
import domain.Rendezvous;

@Service
@Transactional
public class AnnouncementService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private AnnouncementRepository	announcementRepository;

	// Supporting services ---------------------------------------------------

	@Autowired
	private RendezvousService		rendezvousService;

	@Autowired
	private ActorService		actorService;

	// Constructor ---------------------------------------------------

	public AnnouncementService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Announcement create(int rendezvousId) {
		Assert.isTrue(rendezvousId != 0);

		Announcement result;
		Rendezvous rendezvous;
		UserAccount principal;

		principal = LoginService.getPrincipal();

		rendezvous = this.rendezvousService.findOne(rendezvousId);
		Assert.notNull(rendezvous);
		Assert.isTrue(rendezvous.getCreator().getUserAccount().equals(principal));
		Assert.isTrue(!rendezvous.isDraft());

		result = new Announcement();
		result.setMoment(new Date(System.currentTimeMillis() - 1));
		result.setRendezvous(rendezvous);

		return result;
	}

	public Announcement findOne(int announcementId) {
		Assert.isTrue(announcementId != 0);

		Announcement result;

		result = this.announcementRepository.findOne(announcementId);

		return result;
	}

	public Collection<Announcement> findAll() {
		Collection<Announcement> result;

		result = this.announcementRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Announcement save(Announcement announcement) {
		Assert.notNull(announcement);

		Announcement result;
		Rendezvous rendezvous;
		UserAccount principal;

		principal = LoginService.getPrincipal();

		// Check if principal is the creator of the rendezvous associated with this announcement

		Assert.isTrue(announcement.getRendezvous().getCreator().getUserAccount().equals(principal));

		// Check if the rendezvous is not draft

		rendezvous = announcement.getRendezvous();

		Assert.isTrue(!rendezvous.isDraft());

		// Update the moment when it's created

		announcement.setMoment(new Date(System.currentTimeMillis() - 1));

		result = this.announcementRepository.save(announcement);

		// Add the reference of this announcement to the list of them from the rendezvous

		if (announcement.getId() == 0) {
			rendezvous.getAnnouncements().add(result);
			rendezvousService.refreshAnnouncements(rendezvous);
		}

		return result;
	}

	public void delete(Announcement announcement) {
		Assert.notNull(announcement);

		UserAccount principal;
		Authority admin;
		Rendezvous rendezvous;

		principal = LoginService.getPrincipal();

		// Check if principal is admin or the creator of the rendezvous associated with the announcement

		admin = new Authority();
		admin.setAuthority(Authority.ADMIN);

		Assert.isTrue(principal.getAuthorities().contains(admin) || announcement.getRendezvous().getCreator().getUserAccount().equals(principal));

		// Start deleting it

		rendezvous = announcement.getRendezvous();

		rendezvous.getAnnouncements().remove(announcement);
		this.rendezvousService.refreshAnnouncements(rendezvous);

		this.announcementRepository.delete(announcement);
	}
	
	@Autowired
	private Validator validator;
	
	public Announcement reconstruct(Announcement announcement, BindingResult binding) {
		Announcement result;
		
		result = announcement;
		
		announcement.setId(0);
		announcement.setVersion(0);
		
		validator.validate(result, binding);
		
		return result;
	}

	// Other business methods ---------------------------------------------------

	public Collection<Announcement> findFromRendezvous(int rendezvousId) {
		Assert.isTrue(rendezvousId != 0);

		Collection<Announcement> result;
		Rendezvous rendezvous;
		
		rendezvous = rendezvousService.findOne(rendezvousId);
		Assert.notNull(rendezvous);
		
		if (SecurityContextHolder.getContext().getAuthentication() != null && 
			SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
			!(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
			UserAccount principal;
			Actor actor;
			Authority userAuth;
			
			principal = LoginService.getPrincipal();
			actor = actorService.findByUserAccountId(principal.getId());
			
			userAuth = new Authority();
			userAuth.setAuthority(Authority.USER);
			
			if (principal.getAuthorities().contains(userAuth)) {
				Calendar userAdult = Calendar.getInstance();
				userAdult.setTime(actor.getBirthday());
				
				userAdult.set(userAdult.get(Calendar.YEAR) + 18, userAdult.get(Calendar.MONTH), userAdult.get(Calendar.DATE));
				
				if (rendezvous.isAdultsOnly())
					Assert.isTrue(userAdult.getTime().before(new Date()));
			}
					
			result = this.announcementRepository.findFromRendezvous(rendezvousId);
		} else {
			Assert.isTrue(!rendezvous.isAdultsOnly());
			
			result = this.announcementRepository.findFromRendezvous(rendezvousId);
		}

		return result;
	}

	public Collection<Announcement> findFromUserAccountOrderedByDate(int userAccountId) {
		Assert.isTrue(userAccountId != 0);

		Collection<Announcement> result;

		result = this.announcementRepository.findFromUserAccountOrderedByDate(userAccountId);

		return result;
	}

	public void flush() {
		announcementRepository.flush();
	}
	
}
