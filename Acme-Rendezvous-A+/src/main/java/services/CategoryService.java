package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.CategoryRepository;
import security.Authority;
import domain.Actor;
import domain.Category;
import forms.CategoryEditionForm;

@Service
@Transactional
public class CategoryService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	// Validator ---------------------------------------------------

	@Autowired
	Validator validator;
	
	// Supporting services ---------------------------------------------------

	@Autowired
	private ServiceService serviceService;

	@Autowired
	private ActorService actorService;
	
	// Constructor ---------------------------------------------------
	
	public CategoryService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Category create(Category parent){
		Category result;
		
		result = new Category();
		result.setParent(parent);
		result.setChildren(new ArrayList<Category>());
		
		return result;
	}
	
	public Category findOne(int categoryId) {
		Assert.isTrue(categoryId != 0);
		
		Category result;
		
		result = categoryRepository.findOne(categoryId);
		
		return result;
	}
	
	public Collection<Category> findAll() {		
		Collection<Category> result;
		
		result = categoryRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Category save(Category category) {
		Assert.notNull(category);
		
		Category result;
		Category parent;
		
		Actor principal;
		Authority auth;
		
		principal = actorService.findPrincipal();
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		
		Assert.isTrue(principal.getUserAccount().getAuthorities().contains(auth)); //Comprobar permisos
		
		if(category.getId() == 0){
			Assert.isTrue(category.getChildren().size() == 0); //Se debe crear sin hijos			

			if(category.getName() != null){
				if(category.getParent() != null){
					for(Category c : category.getParent().getChildren()){ //Evitar nombres repetidos de forma local
						Assert.isTrue(!category.getName().equals(c.getName()));						
					}
					
				} else{
					for(Category c : this.findParentCategories()){ //Evitar nombres repetidos sin padre
						Assert.isTrue(!category.getName().equals(c.getName()));
					}			
				}	
			}
		} else{						
			if(category.getName() != null){ //Si se est� cambiando el nombre
				if(category.getParent() != null){
					for(Category c : category.getParent().getChildren()){ //Evitar nombres repetidos de forma local
						Assert.isTrue(!category.getName().equals(c.getName()) || c.getId() == category.getId());						
					}
					
				} else{
					for(Category c : this.findParentCategories()){ //Evitar nombres repetidos sin padre						
						Assert.isTrue(!category.getName().equals(c.getName()) || c.getId() == category.getId());
					}			
				}	
			}
		}

		result = categoryRepository.save(category);
		
		if(category.getId() == 0 && category.getParent() != null){ //Si tiene padre y se estaba creando									
			parent = category.getParent();
			
			parent.getChildren().add(result);
			
			categoryRepository.save(parent); //Se actualiza la lista de hijos del padre
		}
		
		return result;
	}
	
	void deleteRec(Category category, Set<Category> categoriesToDelete){
		serviceService.removeCategoryFromServices(category); //Quitar la categor�a de todos los servicios
				
		for(Category c : category.getChildren()){
			deleteRec(c, categoriesToDelete);
		}
				
		category.setParent(null);
		category.setChildren(new ArrayList<Category>());
		
		category = categoryRepository.save(category);
		
		categoriesToDelete.add(category);
	}
	
	public void delete(Category category){		
		Actor principal;
		Authority auth;
		
		principal = actorService.findPrincipal();
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		
		Assert.isTrue(principal.getUserAccount().getAuthorities().contains(auth)); //Comprobar permisos

		Set<Category> categoriesToDelete;

		categoriesToDelete = new HashSet<>();
		
		if(category.getParent() != null){ //Actualizar hijos del padre
			Category parent;
			
			parent = category.getParent();
			parent.getChildren().remove(category);
			
			parent = categoryRepository.saveAndFlush(parent);
		}
		
		deleteRec(category, categoriesToDelete); //A�adir todas las categor�as a borrar
		
		categoryRepository.flush();
		
		categoryRepository.delete(categoriesToDelete);
	}
	
	// Other business methods ---------------------------------------------------
	
	public Category reconstruct(Category prunnedCategory, Integer parentId, BindingResult binding){
		Assert.notNull(prunnedCategory);

		Category result;
		
		result = prunnedCategory;
		result.setChildren(new ArrayList<Category>());
		
		if(parentId != null){ //Si tiene padre
			Category parent;
			
			parent = categoryRepository.findOne(parentId);
			Assert.notNull(parent);
			
			result.setParent(parent);
			
		}
		
		validator.validate(result, binding); //Validar objeto
		
		return result;
	}
	
	public Category reconstruct(CategoryEditionForm categoryEditionForm, Integer categoryId, BindingResult binding){
		Assert.notNull(categoryEditionForm);
		Assert.notNull(categoryId);

		validator.validate(categoryEditionForm, binding); //Validar objeto
		
		Category result;
		
		result = null;
		
		if(!binding.hasErrors()){
			result = categoryRepository.findOne(categoryId);
			Assert.notNull(result);
			
			result.setName(categoryEditionForm.getEditedName());
			result.setDescription(categoryEditionForm.getEditedDescription());		
		}	
		
		return result;
	}
	
	public Collection<Category> findParentCategories() {
		Collection<Category> result;
		
		result = categoryRepository.findParentCategories();
		Assert.notNull(result);
		
		return result;
	}
	
	private boolean hasChildId(Category category, Integer childId){
		boolean result;
		
		result = false;
		
		if(childId != null){
			result = category.getId() == childId;
			
			if(!result){
				for(Category c : category.getChildren()){
					result = hasChildId(c, childId);
					
					if(result){
						break;
					}
				}	
			}
		}
		
		return result;
	}
	
	public void moveCategory(Integer categoryId, Integer newParentId){
		Assert.notNull(categoryId);
		
		Actor principal;
		Authority auth;
		
		principal = actorService.findPrincipal();
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		
		Assert.isTrue(principal.getUserAccount().getAuthorities().contains(auth)); //Comprobar permisos
		
		Category category;
		
		category = categoryRepository.findOne(categoryId);
		
		Assert.notNull(category);
		Assert.isTrue(!hasChildId(category, newParentId)); //Comprobar que no se est� moviendo el padre a un hijo
		
		if(category.getParent() != null){ //Quitar la categor�a de la lista del padre
			Category parent;
			
			parent = category.getParent();
			
			if(newParentId != null){
				Assert.isTrue(parent.getId() != newParentId); //Se tiene que mover a un padre diferente	
			}
			
			parent.getChildren().remove(category);
			
			parent = categoryRepository.save(parent);
		}
				
		if(newParentId != null){ //Si el nuevo padre no es nulo se le asigna y se a�ade a su lista
			Category newParent;
			
			newParent = categoryRepository.findOne(newParentId);
			Assert.notNull(newParent);
						
			for(Category c : newParent.getChildren()){ //Evitar nombres repetidos de forma local
				Assert.isTrue(!c.getName().equals(category.getName()));
			}
			
			category.setParent(newParent);
			category = categoryRepository.save(category); //Se guarda el hijo
			
			newParent.getChildren().add(category);
			newParent = categoryRepository.save(newParent); //Se guarda el padre
		
		} else{ //Se est� moviendo a la ra�z (sin padre)
			for(Category c : this.findParentCategories()){ //Evitar nombres repetidos sin padre
				Assert.isTrue(!c.getName().equals(category.getName()));
			}
			
			category.setParent(null);
			
			category = categoryRepository.save(category);
		}
	}

	public void flush() {
		categoryRepository.flush();
	}
}
