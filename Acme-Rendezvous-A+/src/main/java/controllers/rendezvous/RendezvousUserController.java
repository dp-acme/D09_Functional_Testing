/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.rendezvous;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.RendezvousService;
import controllers.AbstractController;
import domain.Rendezvous;
import domain.User;

@Controller
@RequestMapping("/rendezvous/user")
public class RendezvousUserController extends AbstractController {

	// Services -----------------------------------------------------------

	@Autowired
	private RendezvousService rendezvousService;

	@Autowired
	private ActorService actorService;

	// Constructors -----------------------------------------------------------

	public RendezvousUserController() {
		super();
	}

	// List -------------------------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list() {
		// Creamos el objeto a devolver
		ModelAndView result;
		// Creamos una coleccion de rendezvous para alamcenar todos
		Collection<Rendezvous> rendezvous;

		User user;
		
		
		user = (User) actorService.findByUserAccountId(LoginService
				.getPrincipal().getId());

		// Cogemos los rendezvous del usuario logeado
		rendezvous = user.getMyRendezvouses();

		// //Recorremos y nos quedamos con los que deleted=false
		// for (Rendezvous r : myRendezvous) {
		// if (r.isDeleted() == false) {
		// rendezvous.add(r);
		// }
		// }
		Collection<User> users = new ArrayList<User>();

		for (Rendezvous r : rendezvous) {
			users.addAll(r.getUsers());
		}

		result = new ModelAndView("rendezvous/user/list");
		// Al modelo y la vista le a�adimos los siguientes atributos
		result.addObject("rendezvous", rendezvous);
		result.addObject("requestURI", "rendezvous/user/list.do");
		result.addObject("users", users);

		return result;
	}

	// List rendezvous asiste
	// -------------------------------------------------------------------------
	@RequestMapping("/myRsvp")
	public ModelAndView listRSVP() {
		// Creamos el objeto a devolver
		ModelAndView result;
		// Creamos una coleccion de rendezvous para alamcenar todos
		Collection<Rendezvous> rendezvous;
		// Cogemos el usuario logeado
		UserAccount principal;
		User user;

		principal = LoginService.getPrincipal();
		user = (User) actorService.findByUserAccountId(principal.getId());

		// Cogemos los rendezvous del usuario logeado
		rendezvous = user.getMyRSVPs();
		
		result = new ModelAndView("rendezvous/user/list");
		// Al modelo y la vista le a�adimos los siguientes atributos
		result.addObject("user", user);
		result.addObject("rendezvous", rendezvous);
		result.addObject("requestURI", "rendezvous/user/myRsvp.do");

		return result;
	}

	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView result;
		Rendezvous rendezvous;
		User user;

		user = (User) actorService.findByUserAccountId(LoginService
				.getPrincipal().getId());
		// Comprobamos
		Assert.notNull(user);
		rendezvous = rendezvousService.create();

		result = createEditModelAndView(rendezvous);
		result.addObject("rendezvous", rendezvous);
		result.addObject("adult", isAdult(user));

		return result;
	}

	// Edit------------------------------------------------------------
	@RequestMapping("/edit")
	public ModelAndView edit(@RequestParam int rendezvousId) {
		ModelAndView result;
		Rendezvous rendezvous;
		User user;

		user = (User) actorService.findByUserAccountId(LoginService
				.getPrincipal().getId());
		// Comprobamos
		Assert.notNull(user);

		rendezvous = rendezvousService.findOne(rendezvousId);

		Assert.notNull(rendezvous);
		Assert.isTrue(rendezvous.isDraft() == true);
		Assert.isTrue(rendezvous.getCreator().getUserAccount()
				.equals(LoginService.getPrincipal()));

		result = createEditModelAndView(rendezvous);

		return result;
	}

	
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value="rendezvous") Rendezvous rendezvous, BindingResult binding) {
		ModelAndView result;
		Rendezvous rendezvousRes;
		
		rendezvousRes = rendezvousService.reconstruct(rendezvous, binding);
		
		if (binding.hasErrors()) {
			result = createEditModelAndView(rendezvousRes);
		} else {
			try {
				rendezvousRes = this.rendezvousService.save(rendezvousRes);
				result = new ModelAndView("redirect:/rendezvous/display.do?rendezvousId="+rendezvousRes.getId());
			} catch (Throwable oops) {
				result = createEditModelAndView(rendezvousRes, "rendezvous.commit.error");
				if (rendezvousRes.getMoment().before(new Date()))
					result.addObject("pastDateError", "rendezvous.edit.pastDateError");
			}
		}

		return result;
	}

	// Delete--------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@ModelAttribute(value="rendezvous") Rendezvous rendezvous, BindingResult binding) {
		ModelAndView result;
		Rendezvous rendezvousRes;
		
		rendezvousRes = rendezvousService.findOne(rendezvous.getId());

		try {
			this.rendezvousService.delete(rendezvousRes);
			result = new ModelAndView("redirect:list.do");
		} catch (Throwable oops) {
			result = this.createEditModelAndView(rendezvousRes, "rendezvous.commit.error");
		}

		return result;
	}

	// Publicar
	@RequestMapping(value = "/publish", method = RequestMethod.GET)
	public ModelAndView publish(@RequestParam int rendezvousId) {
		ModelAndView result;
		Rendezvous rendezvous;

		rendezvous = this.rendezvousService.findOne(rendezvousId);

		// Assert.isTrue(rendezvous.isDraft());

		this.rendezvousService.publish(rendezvous);

		result = new ModelAndView("redirect:list.do");

		return result;
	}

	// LinkSimilars --------------------------------------------------------
	@RequestMapping("/linkSimilars")
	public ModelAndView linkSimilars(@RequestParam int rendezvousId) {
		Assert.isTrue(rendezvousId != 0);
		
		ModelAndView result;
		Rendezvous rendezvous;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();

		rendezvous = rendezvousService.findOne(rendezvousId);
		Assert.notNull(rendezvous);
		
		Assert.isTrue(rendezvous.getCreator().getUserAccount().equals(principal));

		result = createLinkSimilarsModelAndView(rendezvous);

		return result;
	}

	@RequestMapping(value = "/linkSimilars", method = RequestMethod.POST, params = "linkSimilars")
	public ModelAndView linkSimilars(@ModelAttribute(value="rendezvous") Rendezvous rendezvous, BindingResult binding) {
		ModelAndView result;

		rendezvous = rendezvousService.reconstructSimilars(rendezvous, binding);

		if (binding.hasErrors()) {
			result = createLinkSimilarsModelAndView(rendezvous);
		} else {
			try {
				this.rendezvousService.linkSimilars(rendezvous);
				result = new ModelAndView("redirect:/rendezvous/display.do?rendezvousId=" + rendezvous.getId());
			} catch (Throwable oops) {
				result = createLinkSimilarsModelAndView(rendezvous, "rendezvous.commit.error");
			}
		}

		return result;
	}
	
	private ModelAndView createEditModelAndView(Rendezvous rendezvous) {
		ModelAndView result;

		result = createEditModelAndView(rendezvous, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Rendezvous rendezvous,
			String messageCode) {
		ModelAndView result;
		
		result = new ModelAndView("rendezvous/user/edit");
		
		result.addObject("rendezvous", rendezvous);
		result.addObject("message", messageCode);
		result.addObject("adult", isAdult((User)actorService.findPrincipal()));

		return result;
	}
	
	private ModelAndView createLinkSimilarsModelAndView(Rendezvous rendezvous) {
		ModelAndView result;

		result = createLinkSimilarsModelAndView(rendezvous, null);

		return result;
	}

	private ModelAndView createLinkSimilarsModelAndView(Rendezvous rendezvous, String messageCode) {
		ModelAndView result;
		Collection<Rendezvous> rendezvouses;

		rendezvouses = rendezvousService.getRendezvousNoDraftNoDeleted();
		rendezvouses.remove(rendezvous);
		
		result = new ModelAndView("rendezvous/user/linkSimilars");

		result.addObject("rendezvous", rendezvous);
		result.addObject("rendezvouses", rendezvouses);
		result.addObject("message", messageCode);

		return result;
	}

	private boolean isAdult(User user) {
		Calendar userAdult = Calendar.getInstance();

		userAdult.setTime(user.getBirthday());

		userAdult.set(userAdult.get(Calendar.YEAR) + 18,
				userAdult.get(Calendar.MONTH), userAdult.get(Calendar.DATE));

		return userAdult.getTime().before(new Date());
	}

}
