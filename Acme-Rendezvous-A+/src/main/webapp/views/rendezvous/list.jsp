<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<%-- --%>
<jsp:useBean id="currentDate" class="java.util.Date" />

<display:table name="rendezvous" id="row" requestURI="${requestURI}" pagesize="5">

	<jstl:if test="${(row.deleted == false) && !(row.moment<currentDate)}">
		<jstl:set value="background-color: #BDECB6" var="style"></jstl:set>
	</jstl:if>

	<jstl:if test="${row.moment < currentDate}">
		<jstl:set value="background-color: #A18BC1" var="style"></jstl:set>
	</jstl:if>
	
	<jstl:if test="${row.deleted == true}">
		<jstl:set value="background-color: #D36E70" var="style"></jstl:set>
	</jstl:if>

	<display:column property="name" titleKey="rendezvous.list.name"
		style="${style}" />

	<display:column property="description"
		titleKey="rendezvous.list.description" style="${style}" />

	<spring:message code="rendezvous.list.dateFormat" var="formatDate" />
	<display:column property="moment" titleKey="rendezvous.list.moment"
		format="{0,date,${formatDate}}" style="${style}" />

	<spring:message code="rendezvous.list.users" var="usersHeader" />

	<display:column title="${usersHeader}" style="${style}">
		<jstl:forEach var="i" items="${row.getUsers()}">		
			<a href="user/display.do?userId=${i.getId()}">
				<jstl:out value="${i.getName()}" />
			</a>
			<br/>
		</jstl:forEach>
		<jstl:if test="${empty row.getUsers()}">
			<spring:message code="rendezvous.list.messageToEmptyUsers" />
		</jstl:if>
	</display:column>

	<display:column titleKey="rendezvous.list.creator" style="${style}">
		<a href="user/display.do?userId=${row.creator.id}"> <jstl:out
				value="${row.creator.name}" />
		</a>
	</display:column>

	<security:authentication property="principal" var="principal" />

	<spring:url var="urlDisplayRendezvous"
		value="rendezvous/display.do?rendezvousId=${row.getId()}" />

	<%--Mostramos la columna de display --%>
	<display:column title="" sortable="false" style="${style}">
		<jstl:if test="${row.deleted == false}">
			<a href="${urlDisplayRendezvous}"> <spring:message
					code="rendezvous.list.display" />
			</a>
		</jstl:if>
	</display:column>

	<!-- EDIT, solo mostraremos el edit cuando estemos en los rendezvous del usuario logado -->
	<display:column title="" sortable="false" style="${style}">
		<jstl:if test="${requestURI == 'rendezvous/user/list.do' && (row.draft==true) && (principal == row.creator.userAccount) && (row.deleted==false)}">
			<security:authorize access="hasRole('USER')">
				<a
					href="<spring:url value="rendezvous/user/edit.do?rendezvousId=${row.getId()}" />">
					<spring:message code="rendezvous.list.edit" />
				</a>
			</security:authorize>
		</jstl:if>
	</display:column>

	<!-- Publish, solo aparece si el draft esta a true y no esta borrado -->
	<display:column title="" sortable="false" style="${style}">
		<jstl:if test="${requestURI == 'rendezvous/user/list.do' && (row.draft==true) && (principal == row.creator.userAccount) && (row.deleted==false)}">
			<security:authorize access="hasRole('USER')">
				<a
					href="<spring:url value="rendezvous/user/publish.do?rendezvousId=${row.getId()}" />"
					onClick="javascript: return confirm('<spring:message code="rendezvous.list.ConfirmPublish" />');">
					<spring:message code="rendezvous.list.publish" />
				</a>
			</security:authorize>
		</jstl:if>
	</display:column>

</display:table>

<br />
<br />

<jstl:if test="${requestURI == 'rendezvous/user/list.do'}">
	<security:authorize access="hasRole('USER')">
		<a href="rendezvous/user/create.do"> <spring:message code="rendezvous.list.create" /></a>
	</security:authorize>
</jstl:if>



