<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt"	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<a href="configuration/edit.do">
	<spring:message code="configuration.display.edit" />
</a>

<br>

<p>
	<b><spring:message code = "configuration.display.bannerLabel" />:</b> <spring:url value="${configuration.banner}" />
	
	<jstl:if test="${configuration.banner != null}">
		<br/><img style="max-width:40%" class="banner" alt="Banner" src="${configuration.banner}">
	</jstl:if>
</p>

<p>
	<b><spring:message code = "configuration.display.welcomeMessageEnglishLabel" />: </b><jstl:out value="${configuration.welcomeMessageEnglish}" />
</p>

<p>
	<b><spring:message code = "configuration.display.welcomeMessageSpanishLabel" />: </b><jstl:out value="${configuration.welcomeMessageSpanish}" />
</p>

<p>
	<b><spring:message code = "configuration.display.businessName" />: </b><jstl:out value="${configuration.businessName}" />
</p>
