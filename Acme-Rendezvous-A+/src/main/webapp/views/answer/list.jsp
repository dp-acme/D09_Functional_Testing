<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:set var="uri" value="/answer/list.do" />

<spring:message code="answer.answer" var="answer" />
<spring:message code="answer.user" var="user" />

<display:table name="answers" id="row" requestURI="${uri}" pagesize="5">
	
	<display:column title="${user}" sortable="true">
			<jstl:out value="${row.user.name}" />
	</display:column>
	
	<display:column title="${answer}" sortable="true">
		<jstl:out value="${row.answer}" />
	</display:column>
</display:table>

<br/>
<input type="button" name="back" value="<spring:message code="answer.back" />" onclick="javascript: relativeRedir('question/list.do?rendezvousId=${rendezvousId}');" />

