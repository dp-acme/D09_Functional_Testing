<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<display:table name="services" id="row" requestURI="${requestURI}" pagesize="5">

	<jstl:if test="${row.cancelled}">
		<jstl:set value="background-color: #D36E70" var="style"></jstl:set>
	</jstl:if>

	<jstl:if test="${!row.cancelled}">
		<jstl:set value="background-color: #BDECB6" var="style"></jstl:set>
	</jstl:if>

	<display:column property="name" titleKey="service.list.name" style="${style}" />

	<display:column property="description" titleKey="service.list.description" style="${style}" />

	<display:column titleKey="service.list.manager" style="${style}" >
	<jstl:out value="${row.manager.name}"/>
	</display:column>

	<security:authentication property="principal" var="principal" />

	<spring:url var="urlDisplayService"
		value="service/display.do?serviceId=${row.getId()}" />

	<%--Mostramos la columna de display --%>
	<display:column title="" sortable="false" style="${style}">
		<jstl:if test="${row.cancelled == false}">
			<a href="${urlDisplayService}"> <spring:message
					code="service.list.display" />
			</a>
		</jstl:if>
	</display:column>

	<!-- EDIT, solo mostraremos el edit cuando estemos en los servicios del manager logado -->
	<display:column title="" sortable="false" style="${style}">
		<jstl:if
			test="${requestURI == 'service/manager/list.do' && (principal == row.manager.userAccount) && (row.cancelled==false)}">
			<security:authorize access="hasRole('MANAGER')">
				<a
					href="<spring:url value="service/manager/edit.do?serviceId=${row.getId()}" />">
					<spring:message code="service.list.edit" />
				</a>
			</security:authorize>
		</jstl:if>
	</display:column>

	<!-- Cancelar, solo aparece si el atributo cancelled = false -->
		<display:column title="" sortable="false" style="${style}">
	<security:authorize access="hasRole('ADMIN')">
			<jstl:if
				test="${requestURI == 'service/list.do'&& (row.cancelled==false)}">
				<a
					href="<spring:url value="service/cancel.do?serviceId=${row.getId()}" />"
					onClick="javascript: return confirm('<spring:message code="service.list.ConfirmCancelled" />');">
					<spring:message code="service.list.cancel" />
				</a>
			</jstl:if>
	</security:authorize>
		</display:column>
</display:table>
<br />
<br />

<jstl:if test="${requestURI == 'service/manager/list.do'}">
	<security:authorize access="hasRole('MANAGER')">
		<a href="service/manager/create.do"> <spring:message
				code="service.list.create" /></a>
	</security:authorize>
</jstl:if>


