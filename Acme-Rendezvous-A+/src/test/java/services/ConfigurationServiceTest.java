package services;


import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import domain.Configuration;

import services.ConfigurationService;
import utilities.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class ConfigurationServiceTest extends AbstractTest{

	@Autowired
	private ConfigurationService configurationService;
	
	
	@Test
	public void driver(){
		Object[][] testingData = {
				{"admin", "http://www.ev.us.es", "ACME", "En message", "Es message", null}, // Acción permitida
				{"admin", "noUrl", "ACME", "En message", "Es message", ConstraintViolationException.class}, //Restricción del modelo
				{"admin", "http://www.ev.us.es", "", "En message", "Es message", ConstraintViolationException.class},//Restricción del modelo
				{"admin", "http://www.ev.us.es", "ACME", "", "Es message", ConstraintViolationException.class},//Restricción del modelo
				{"admin", "http://www.ev.us.es", "ACME", "En message", "", ConstraintViolationException.class},//Restricción del modelo
				{"admin", "http://www.ev.us.es", "", "En message", "Es message", ConstraintViolationException.class},//Restricción del modelo
				{"admin", null, "ACME", "En message", "Es message", ConstraintViolationException.class},//Restricción del modelo
				{"admin", "http://www.ev.us.es", null, "En message", "Es message", ConstraintViolationException.class},//Restricción del modelo
				{"admin", "http://www.ev.us.es", "ACME", null, "Es message", ConstraintViolationException.class},//Restricción del modelo
				{"admin", "http://www.ev.us.es", "ACME", "En message", null, ConstraintViolationException.class},//Restricción del modelo
				{"admin", "http://www.ev.us.es", null, "En message", "Es message", ConstraintViolationException.class},//Restricción del modelo
				{"admin1", "http://www.ev.us.es", "ACME", "En message", "Es message", IllegalArgumentException.class}, //Usuario que no existe
				{"user1", "http://www.ev.us.es", "ACME", "En message", "Es message", ConstraintViolationException.class}//No es admin
		};
		
		for(int i = 0; i<testingData.length; i++){
			template((String) testingData[i][0],(String) testingData[i][1],(String) testingData[i][2],
					(String) testingData[i][3],(String) testingData[i][4], (Class<?>)testingData[i][5]);
		}
	}
	
	/*
	 * 12. Acme Rendezvous, Inc. is franchising their business. They require the
	 * following data to customise their system for particular franchisees: the
	 * name of the business, a banner, and a welcome message (which must be
	 * available in the languages in which the system's available). This
	 * information must be displayed appropriately in the header of the pages
	 * and the welcome page. Administrators must be allowed to change the
	 * previous data at runtime. 13. The system must be ready to be deployed to
	 * a pilot franchisee with the following data: 1. Name of business =
	 * Adventure meetups 2. Banner = https://tinyurl.com/adventure-meetup,
	 * 3. English welcome message = Your place to organise your adventure
	 * meetups! 4. Spanish welcome message = Tu sitio para organizar quedadas
	 * de aventura.
	 */
	protected void template(String username, String banner, String businessName, String welcomeEn, String welcomeEs, Class<?> expected){
		Class<?>  caught;
		Configuration configuration;

		caught = null;
		
		try{			
			super.authenticate(username);
			configuration = configurationService.findConfiguration();
			
			configuration.setBanner(banner);
			configuration.setBusinessName(businessName);
			configuration.setWelcomeMessageEnglish(welcomeEn);
			configuration.setWelcomeMessageSpanish(welcomeEs);
			
			configurationService.save(configuration);
			configurationService.flush();
		
			super.unauthenticate();	
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
	
}
