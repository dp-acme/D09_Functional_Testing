package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import services.AnnouncementService;
import utilities.AbstractTest;
import domain.Announcement;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class AnnouncementServiceTest extends AbstractTest{

	@Autowired
	private AnnouncementService announcementService;
	
/*
	15. An actor who is not authenticated must be able to:
		1. List the announcements that are associated with each rendezvous.
*/
	@Test
	public void driverListingUnauthenticated(){
		Object[][] testingData = {
				//Positivos
				{"rendezvous3", null},
				{"rendezvous5", null},
				
				//Negativos
				{"rendezvous1", IllegalArgumentException.class}, //Rendezvous adultsOnly
//				{"rendezvous4", IllegalArgumentException.class}, //Rendezvous deleted
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateListing((String) testingData[i][0], (Class<?>) testingData[i][1]);
		}
	}
	
/*
	16. An actor who is authenticated as a user must be able to:
		3. Create an announcement regarding one of the rendezvouses that he or she's created previously.
*/
	@Test
	public void driverCreate(){
		Object[][] testingData = {
				//Positivos
				{"rendezvous1", "user1", "title", "description", null},
				{"rendezvous3", "user2", "title", "description", null},

				//Negativos
				{"rendezvous4", "user2", "title", "description", IllegalArgumentException.class}, //Draft Rendezvous
				{"rendezvous1", "user2", "title", "description", IllegalArgumentException.class}, //Usuario no es creador de rendezvous
				{"rendezvous1", null, "title", "description", IllegalArgumentException.class}, //Usuario no logeado
				{"rendezvous1", "manager1", "title", "description", IllegalArgumentException.class}, //Manager, no usuario

				{"rendezvous1", "user1", "", "", ConstraintViolationException.class}, //Restricciones del dominio
				{"rendezvous1", "user1", "", "description", ConstraintViolationException.class},
				{"rendezvous1", "user1", "title", "", ConstraintViolationException.class},
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2],
						 (String) testingData[i][3], (Class<?>) testingData[i][4]);
		}
	}

	/*
	16. An actor who is authenticated as a user must be able to:
		5. Display a stream of announcements that have been posted to the rendezvouses that he or she's RSVPd.
		The announcements must be listed chronologically in descending order.
*/
	@Test
	public void driverListingUserAnnouncements(){
		Object[][] testingData = {
				//Positivos
				{"user1", null},
				{"user3", null},
				
				//Negativos
				//Implementado en controlador
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateListingUserAnnouncements((String) testingData[i][0], (Class<?>) testingData[i][1]);
		}
	}
	
/*
	17. An actor who is authenticated as an administrator must be able to:
		1. Remove an announcement that he or she thinks is inappropriate.
*/
	@Test
	public void driverDeleteAdmin(){
		Object[][] testingData = {
				//Positivos
				{"admin", "announcement1", null},
				
				//Negativos
				{"user3", "announcement1", IllegalArgumentException.class}, //No es admin, user
				{"manager1", "announcement1", IllegalArgumentException.class} //No es admin, manager
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateDeleteAdmin((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void templateListing(String rendezvousBeanId, Class<?> expected){
		Class<?>  caught;
		
		caught = null;
		
		try{			
			super.authenticate(null);
			
			announcementService.findFromRendezvous(super.getEntityId(rendezvousBeanId));
			
			super.unauthenticate();	
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
	
	protected void templateCreate(String rendezvousBeanId, String username, String title, String description, Class<?> expected){
		Class<?>  caught;
		
		Announcement announcement;
		
		caught = null;
		
		try{			
			super.authenticate(username);
			
			announcement = announcementService.create(super.getEntityId(rendezvousBeanId));
			Assert.notNull(announcement);
			
			announcement.setTitle(title);
			announcement.setDescription(description);
			
			announcementService.save(announcement);
			announcementService.flush();
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
	
	protected void templateListingUserAnnouncements(String userBeanId, Class<?> expected){
		Class<?>  caught;
		
		caught = null;
		
		try{			
			announcementService.findFromUserAccountOrderedByDate(super.getEntityId(userBeanId));
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
	
	protected void templateDeleteAdmin(String username, String announcementBeanId, Class<?> expected){
		Class<?>  caught;
		
		Announcement announcement;
		
		caught = null;
		
		try{			
			super.authenticate(username);
			
			announcement = announcementService.findOne(super.getEntityId(announcementBeanId));
			
			announcementService.delete(announcement);
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
}
