package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import services.RendezvousService;
import services.RequestService;
import services.ServiceService;
import utilities.AbstractTest;
import domain.Rendezvous;
import domain.Request;
import domain.Service;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class RequestServiceTest extends AbstractTest{

	@Autowired
	private RequestService requestService;
	
	@Autowired
	private ServiceService serviceService;
	
	@Autowired
	private RendezvousService rendezvousService;
	
/*
	4. An actor who is authenticated as a user must be able to:
		3. Request a service for one of the rendezvouses that he or she's created.
		He or she must specify a valid credit card in every request for a service.
		Optionally, he or she can provide some comments in the request. 
*/
	@Test
	public void driverCreate(){
		Object[][] testingData = {
				//Positivos
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 20, null, null},
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 20, "comments", null},
				
				//Negativos
				{"user2", "rendezvous1", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 20, null, 
					IllegalArgumentException.class}, //Rendezvous not from user
				{"user3", "rendezvous6", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 20, null, 
					IllegalArgumentException.class}, //Rendezvous deleted
				{"user1", "rendezvous2", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 20, null, 
					IllegalArgumentException.class}, //Rendezvous draft
				{"user2", "rendezvous3", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 20, null, 
					IllegalArgumentException.class}, //Rendezvous past
				{"user2", "rendezvous5", "service2", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 20, null, 
					IllegalArgumentException.class}, //Service cancelled
				{null, "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 20, null, 
					IllegalArgumentException.class}, //User not logged in
				{"user2", "rendezvous5", "service1", "0939", "holderName", "brandName", 123, 12, 20, null, 
					ConstraintViolationException.class}, //CreditCardNumber not valid
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "", "brandName", 123, 12, 20, null, 
					ConstraintViolationException.class}, //Domain constraints
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", null, 123, 12, 20, null, 
					ConstraintViolationException.class}, //Domain constraints
				{"user2", "rendezvous5", "service1", null, "holderName", "brandName", 123, 12, 20, null, 
					ConstraintViolationException.class}, //Domain constraints
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", null, 12, 20, null, 
					NullPointerException.class}, //Domain constraints
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, null, 20, null, 
					NullPointerException.class}, //Domain constraints
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, null, null, 
					NullPointerException.class}, //Domain constraints
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, 50, 20, null, 
					ConstraintViolationException.class}, //Domain constraints
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 120, null, 
					ConstraintViolationException.class}, //Domain constraints
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", 12, 12, 20, null, 
					ConstraintViolationException.class}, //Domain constraints
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", 1223, 12, 20, null, 
					ConstraintViolationException.class}, //Domain constraints
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, -12, 20, null, 
					ConstraintViolationException.class}, //Domain constraints
				{"user2", "rendezvous5", "service1", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, -20, null, 
					ConstraintViolationException.class}, //Domain constraints
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3],
					(String) testingData[i][4], (String) testingData[i][5], (Integer) testingData[i][6], (Integer) testingData[i][7],
					(Integer) testingData[i][8], (String) testingData[i][9], (Class<?>) testingData[i][10]);
		}
	}

	protected void templateCreate(String username, String rendezvousBeanId, String serviceBeanId, String creditCardNumber,
			String holderName, String brandName, Integer cvv, Integer expirationMonth, Integer expirationYear, String comments, Class<?> expected){
		Class<?>  caught;
		
		caught = null;
		
		try{			
			super.startTransaction();
			
			super.authenticate(username);
			
			Request request;
			Service service;
			Rendezvous rendezvous;
			
			service = serviceService.findOne(super.getEntityId(serviceBeanId));
			rendezvous = rendezvousService.findOne(super.getEntityId(rendezvousBeanId));
			
			request = requestService.create(service);
			
			request.setRendezvous(rendezvous);
			request.setBrandName(brandName);
			request.setHolderName(holderName);
			request.setCreditCardNumber(creditCardNumber);
			request.setComments(comments);
			request.setExpirationMonth(expirationMonth);
			request.setExpirationYear(expirationYear);
			request.setCvv(cvv);
			
			requestService.save(request);
			requestService.flush();
			
			super.unauthenticate();	
		}catch (Throwable e) {
			caught = e.getClass();
		}finally{
			rollbackTransaction();
		}
		
		checkExceptions(expected, caught);
	}
}
