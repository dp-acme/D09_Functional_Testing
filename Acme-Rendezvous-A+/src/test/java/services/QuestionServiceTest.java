package services;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import services.QuestionService;
import utilities.AbstractTest;
import domain.Question;
import domain.Rendezvous;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class QuestionServiceTest extends AbstractTest{

	@Autowired
	private QuestionService questionService;
	
	@Test
	public void driverCreate(){
		Object[][] testingData = {
				{"user2", "�Esto es una pregunta?","rendezvous4", null}, //Acci�n permitida
				{"user1", "�Esto es una pregunta?","rendezvous2", null}, //Acci�n permitida
				{"user2", "", "rendezvous4", ConstraintViolationException.class}, //Restricci�n del modelo
				{"user2", null, "rendezvous4", ConstraintViolationException.class}, //Restricci�n del modelo
				{"user2", "�Esto es una pregunta?","rendezvous5", IllegalArgumentException.class}, //Rendezvous final
				{"user3", "�Esto es una pregunta?","rendezvous6", IllegalArgumentException.class}, //Rendezvous borrado
				{"user1", "�Esto es una pregunta?","rendezvous4", IllegalArgumentException.class}, //No es el due�o
				{"manager1", "�Esto es una pregunta?","rendezvous4", IllegalArgumentException.class}, //No es el due�o
				{"admin", "�Esto es una pregunta?","rendezvous4", IllegalArgumentException.class}, //No es el due�o
				{"user10", "�Esto es una pregunta?","rendezvous4", IllegalArgumentException.class},//No existe el usuario
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateCreate((String) testingData[i][0],(String) testingData[i][1],(String) testingData[i][2], (Class<?>) testingData[i][3]);
		}
	}
	
	@Test
	public void driverDelete(){
		Object[][] testingData = {
				{"user2", "question4", null},	//Acci�n permitida
				{"user1", "question1", IllegalArgumentException.class},	//Rendezvous final
				{"user2", "question5", IllegalArgumentException.class},	//Rendezvous final
				{"user2", "question1", IllegalArgumentException.class},	//No es el due�o 
				{"manager1", "question1",IllegalArgumentException.class},//No es el due�o
				{"admin", "question1", IllegalArgumentException.class},// No es el due�o 
				{"user10", "question1", IllegalArgumentException.class},// No existe el usuario
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateDelete((String) testingData[i][0],(String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}
	
	@Test
	public void driverEdit(){
		Object[][] testingData = {
				{"user2", "question4", "Pregunta editada", null}, //Acci�n permitida
				{"user2", "question4", "", ConstraintViolationException.class}, //Restricci�n del modelo
				{"user2", "question4", null, ConstraintViolationException.class},//Restricci�n del modelo
				{"user1", "question1", "Pregunta editada",  IllegalArgumentException.class}, //Rendezvous final	
				{"user2", "question5", "Pregunta editada", IllegalArgumentException.class},	 //Rendezvous final	
				{"user2", "question1", "Pregunta editada", IllegalArgumentException.class},//No es el due�o
				{"manager1", "question1", "Pregunta editada", IllegalArgumentException.class},//No es el due�o
				{"admin", "question1", "Pregunta editada", IllegalArgumentException.class},//No es el due�o
				{"user10", "question1", "Pregunta editada",  IllegalArgumentException.class},//No existe	
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateEdit((String) testingData[i][0],(String) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
		}
	}
	


	/* The creator of a rendezvous may associate a number of questions with it, each of which
	must be answered when a user RSVPs that rendezvous.*/
	protected void templateCreate(String username, String question, String rendezvousBeanId, Class<?> expected){
		Class<?>  caught;
		Question result;
		int rendezvousId;
		
		caught = null;
		
		try{			
			super.authenticate(username);
			
			rendezvousId = super.getEntityId(rendezvousBeanId);
			result = questionService.create(rendezvousId);
			
			result.setText(question);
			
			questionService.save(result);
			questionService.flush();
			
			super.unauthenticate();	
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
	
	/*Manage the questions that are associated with a rendezvous that he or she's created
	previously. DELETE*/
	protected void templateDelete(String username, String questionBeanId, Class<?> expected){
		Class<?>  caught;
		Question question;
		int questionId;
		Rendezvous rendezvous;
		
		caught = null;
		
		try{			
			super.authenticate(username);
			
			questionId = super.getEntityId(questionBeanId);
			question = questionService.findOne(questionId);
			rendezvous = question.getRendezvous();
			
			questionService.delete(question);
			questionService.flush();
				
			Assert.isTrue(!rendezvous.getQuestions().contains(question));
			
			super.unauthenticate();	
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
	
	/*Manage the questions that are associated with a rendezvous that he or she's created
	previously. EDIT*/
	protected void templateEdit(String username, String questionBeanId, String questionText, Class<?> expected){
		Class<?>  caught;
		Question question;
		int questionId;
		Rendezvous rendezvous;
		
		caught = null;
		
		try{			
			super.authenticate(username);
			
			questionId = super.getEntityId(questionBeanId);
			question = questionService.findOne(questionId);
			rendezvous = question.getRendezvous();
			
			question.setText(questionText);
			
			questionService.save(question);
			questionService.flush();
				
			Assert.isTrue(rendezvous.getQuestions().contains(question));
			
			super.unauthenticate();	
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}
	
	
}
