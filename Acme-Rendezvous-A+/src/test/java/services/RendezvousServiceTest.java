package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Rendezvous;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/junit.xml" })
@Transactional
public class RendezvousServiceTest extends AbstractTest {

	@Autowired
	private RendezvousService rendezvousService;

	@Autowired
	private UserService userService;

	// ----------------------------------------------------------------------------------------------------------------------
	/*
	 * 5.An actor who is authenticated as a user must be able to: 2. Create a
	 * rendezvous, which hes implicitly assumed to attend. Note that a user may
	 * edit his or her rendezvouses as long as they arent saved them in final
	 * mode. Once a rendezvous is saved in final mode, it cannot be edited or
	 * deleted by the creator. CREATE
	 */

	@Test
	public void driverCreate() {

		Calendar futureDate;
		Calendar pastDate;
		Calendar invalidDate;

		futureDate = Calendar.getInstance();
		futureDate.set(2019, 2, 17, 21, 9);

		pastDate = Calendar.getInstance();
		pastDate.set(2016, 1, 10, 7, 35);

		invalidDate = Calendar.getInstance();
		invalidDate.set(0, 0, 0, 0, 0);

		Object testingData[][] = {
				// Test v�lido:
				{
						"user1",
						"Paris Secret",
						"Cita destinada a encontrar el amor",
						futureDate.getTime(),
						"8.9",
						"5.9",
						"https://www.recordrentacar.com/images/beforefooter/destinos/alquiler-coches-madrid.jpg",
						null },
//				// Test v�lido: Picture vac�a
				{ "user3", "Spanish Rendezvous",
						"Encuentro de espa�oles en Irlanda",
						futureDate.getTime(), "8.9", "5.9", "", null },
//				// Test inv�lido: Momento invalido
				{ "user3", "Spanish Rendezvous",
						"Encuentro de espa�oles en Irlanda",
						invalidDate.getTime(), "8.9", "5.9", "",
						IllegalArgumentException.class },
//				// Test inv�lido: Picture invalida
				{ "user1", "Tentud�a", "Cita en una camioneta con todo pagado",
						futureDate.getTime(), "8.9", "5.9", "urlInv�lida",
						ConstraintViolationException.class },
				// Test inv�lido: Momento no puede ser pasado
				{ "user3", "Spanish Rendezvous",
						"Encuentro de espa�oles en Irlanda",
						pastDate.getTime(), "8.9", "5.9", "",
						ConstraintViolationException.class },
				// Test inv�lido: Usuario no autenticado
				{
						null,
						"ParisSecret",
						"Cita destinada a encontrar el amor",
						futureDate.getTime(),
						"8.9",
						"5.9",
						"https://www.recordrentacar.com/images/beforefooter/destinos/alquiler-coches-madrid.jpg",
						IllegalArgumentException.class },
				// Test inv�lido: Admin intentando crear
				{
						"admin",
						"ParisSecret",
						"Cita destinada a encontrar el amor",
						futureDate.getTime(),
						"8.9",
						"5.9",
						"https://www.recordrentacar.com/images/beforefooter/destinos/alquiler-coches-madrid.jpg",
						IllegalArgumentException.class },
				// Test inv�lido: El nombre esta vac�o
				{
						"user3",
						" ",
						"Cita destinada a encontrar el amor",
						futureDate.getTime(),
						"8.9",
						"5.9",
						"https://www.recordrentacar.com/images/beforefooter/destinos/alquiler-coches-madrid.jpg",
						ConstraintViolationException.class },
				// Test inv�lido: La descripci�n esta vac�a
				{
						"user3",
						" Tumamt",
						"",
						futureDate.getTime(),
						"13.2",
						"1.4",
						"https://www.recordrentacar.com/images/beforefooter/destinos/alquiler-coches-madrid.jpg",
						ConstraintViolationException.class },

		};

		for (int i = 0; i < testingData.length; i++) {
			templateCreate((String) testingData[i][0],
					(String) testingData[i][1], (String) testingData[i][2],
					(Date) testingData[i][3], (String) testingData[i][4],
					(String) testingData[i][5], (String) testingData[i][6],
					(Class<?>) testingData[i][7]);
		}
	}

	protected void templateCreate(String username, String name,
			String description, Date moment, String latitude, String longitude,
			String picture, Class<?> expected) {
		Class<?> caught;
		caught = null;
		Rendezvous result;
		try {
			super.authenticate(username);

			result = rendezvousService.create();

			result.setName(name);
			result.setDescription(description);
			result.setMoment(moment);
			result.setPicture(picture);
			result.setLatitude(Double.valueOf(latitude));
			result.setLongitude(Double.valueOf(longitude));

			rendezvousService.save(result);
			rendezvousService.flush();

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	//
	// ----------------------------------------------------------------------------------------------------------------------

	/*
	 * 5.An actor who is authenticated as a user must be able to: 2. Create a
	 * rendezvous, which hes implicitly assumed to attend. Note that a user may
	 * edit his or her rendezvouses as long as they arent saved them in final
	 * mode. Once a rendezvous is saved in final mode, it cannot be edited or
	 * deleted by the creator. EDIT
	 */
	@Test
	public void driverEdit() {

		Calendar futureDate;
		Calendar pastDate;

		futureDate = Calendar.getInstance();
		futureDate.set(2019, 2, 17, 21, 9);

		pastDate = Calendar.getInstance();
		pastDate.set(2016, 1, 10, 7, 35);

		Object testingData[][] = {
				// Test v�lido:
				{
						"user1",
						"rendezvous2",
						"ParisSecret",
						"Cita destinada a encontrar el amor",
						futureDate.getTime(),
						"https://www.recordrentacar.com/images/beforefooter/destinos/alquiler-coches-madrid.jpg",
						null },
				// Test v�lido:
				{ "user1", "rendezvous2", "ParisSecret",
						"Cita destinada a encontrar el amor",
						futureDate.getTime(), "", null },
				// Test inv�lido: Nombre no puede ser blanco
				{ "user1", "rendezvous2", "",
						"Cita destinada a encontrar el amor",
						futureDate.getTime(), "https://www.informatica.us.es/",
						ConstraintViolationException.class },
				// Test inv�lido: Descripcion no puede ser vac�a
				{ "user1", "rendezvous2", "FirstDates", " ",
						futureDate.getTime(), null,
						ConstraintViolationException.class },
				// Test inv�lido: Momento inv�lido
				{ "user2", "rendezvous4", "FirstDates",
						"Cita disponible para todos los p�blicos",
						futureDate.getTime(), "https://www.youtube.com",
						ConstraintViolationException.class },
				// Test inv�lido: Momento nulo
				{ "user2", "rendezvous4", "FirstDates",
						"Cita disponible para todos los p�blicos",
						futureDate.getTime(),
						"http://www.wordreference.com/es/",
						ConstraintViolationException.class },
				// Test inv�lido: Rendezvous publicado
				{ "user1", "rendezvous1", "FirstDates",
						"Cita disponible para todos los p�blicos",
						futureDate.getTime(), null,
						IllegalArgumentException.class },
				// Test inv�lido: Usuario no logeado
				{ null, "rendezvous2", "FirstDates",
						"Cita disponible para todos los p�blicos",
						futureDate.getTime(), null,
						IllegalArgumentException.class },
				// Test inv�lido: Usuario logeado como admin
				{ "admin", "rendezvous2", "FirstDates",
						"Cita disponible para todos los p�blicos",
						futureDate.getTime(),
						"http://www.wordreference.com/es/",
						IllegalArgumentException.class },
				// Test inv�lido: Fecha pasada
				{ "user1", "rendezvous2", "FirstDates",
						"Cita disponible para todos los p�blicos",
						pastDate.getTime(), null,
						IllegalArgumentException.class },
				// Test inv�lido: Rendezvous borrado
				{ "user3", "rendezvous6", "Atrapa un mill�n",
						"Cita disponible para todos los p�blicos",
						pastDate.getTime(), null,
						IllegalArgumentException.class } };

		for (int i = 0; i < testingData.length; i++) {
			templateEdit((String) testingData[i][0],
					(String) testingData[i][1], (String) testingData[i][2],
					(String) testingData[i][3], (Date) testingData[i][4],
					(String) testingData[i][5], (Class<?>) testingData[i][6]);
		}
	}

	protected void templateEdit(String username, String rendezvousIdBean,
			String name, String description, Date moment, String picture,
			Class<?> expected) {
		Class<?> caught;
		Rendezvous rendezvous;
		int rendezvousId;
		caught = null;

		try {
			super.authenticate(username);

			rendezvousId = super.getEntityId(rendezvousIdBean);
			rendezvous = this.rendezvousService.findOne(rendezvousId);

			rendezvous.setName(name);
			rendezvous.setDescription(description);
			rendezvous.setPicture(picture);

			rendezvous.setMoment(moment);

			rendezvousService.save(rendezvous);
			rendezvousService.flush();

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	// ----------------------------------------------------------------------------------------------------------------------

	/*
	 * 5.An actor who is authenticated as a user must be able to: 2. Create a
	 * rendezvous, which hes implicitly assumed to attend. Note that a user may
	 * edit his or her rendezvouses as long as they arent saved them in final
	 * mode. Once a rendezvous is saved in final mode, it cannot be edited or
	 * deleted by the creator. DELETE
	 */

	@Test
	public void driverDelete() {

		Object testingData[][] = {
				// Test v�lido:
				{ "admin", "rendezvous1", null },
				// Test v�lido:
				{ "user1", "rendezvous2", null },
				// Test inv�lido: Rendezvous publicado
				{ "user1", "rendezvous1", IllegalArgumentException.class },
				// Test inv�lido: Rendezvous que no existe
				{ "user1", "rendezvous12323", NumberFormatException.class },
				// Test inv�lido: Manager
				{ "manager2", "rendezvous2", IllegalArgumentException.class },
				// Test inv�lido: Rendezvous no pertence al user logeado
				{ "user1", "rendezvous5", IllegalArgumentException.class },
				// Test inv�lido: Usuario no logeado
				{ null, "rendezvous4", IllegalArgumentException.class },
				// Test inv�lido: Rendezvous borrado
				{ "user3", "rendezvous6", IllegalArgumentException.class }, };

		for (int i = 0; i < testingData.length; i++) {
			templateDelete((String) testingData[i][0],
					(String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void templateDelete(String username, String rendezvousIdBean,
			Class<?> expected) {
		Class<?> caught;
		Rendezvous rendezvous;
		int rendezvousId;
		caught = null;

		try {
			super.authenticate(username);

			rendezvousId = super.getEntityId(rendezvousIdBean);
			rendezvous = this.rendezvousService.findOne(rendezvousId);

			rendezvousService.delete(rendezvous);

			rendezvousService.flush();

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * 5.An actor who is authenticated as a user must be able to: 2. Create a
	 * rendezvous, which hes implicitly assumed to attend. Note that a user may
	 * edit his or her rendezvouses as long as they arent saved them in final
	 * mode. Once a rendezvous is saved in final mode, it cannot be edited or
	 * deleted by the creator. PUBLICAR
	 */

	@Test
	public void driverPublish() {

		Object testingData[][] = {
				// Test v�lido:
				{ "user1", "rendezvous2", null },
				// Test v�lido:
				{ "user2", "rendezvous4", null },
				// Test inv�lido: Rendezvous publicado
				{ "user2", "rendezvous5", IllegalArgumentException.class },
				// Test inv�lido: Admin intentando publicar
				{ "admin", "rendezvous2", IllegalArgumentException.class },
				// Test inv�lido: Rendezvous que no existe
				{ "user1", "rendezvous100", NumberFormatException.class },
				// Test inv�lido: Usuario no autenticado
				{ null, "rendezvous2", IllegalArgumentException.class },
				// Test inv�lido: Rendezvous no pertence al user logeado
				{ "user1", "rendezvous4", IllegalArgumentException.class }, };

		for (int i = 0; i < testingData.length; i++) {
			templateDelete((String) testingData[i][0],
					(String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void templatePublish(String username, String rendezvousIdBean,
			Class<?> expected) {
		Class<?> caught;
		Rendezvous rendezvous;
		int rendezvousId;
		caught = null;

		try {
			super.authenticate(username);

			rendezvousId = super.getEntityId(rendezvousIdBean);
			rendezvous = this.rendezvousService.findOne(rendezvousId);

			rendezvousService.publish(rendezvous);
			rendezvousService.flush();

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * 3. List the rendezvouses in the system and navigate to the profiles of
	 * the corresponding creators and attendants.
	 */

	// No logeado

	@Test
	public void driverListRendezvousSystemNoAuth() {

		Object testingData[][] = {
				// Test v�lido:
				{ null, "rendezvous3", "user1", null },
				{ null, "rendezvous3", "user3", IllegalArgumentException.class } };

		for (int i = 0; i < testingData.length; i++) {
			templateListRendezvousSystemNoAuth((String) testingData[i][0],
					(String) testingData[i][1], (String) testingData[i][2],
					(Class<?>) testingData[i][3]);
		}
	}

	protected void templateListRendezvousSystemNoAuth(String username,
			String rendezvousBean, String attendantBean, Class<?> expected) {
		Class<?> caught;
		Collection<Rendezvous> rendezvouses;
		Rendezvous rendezvous;
		int rendezvousId;
		int attendantId;
		User attendant;
		caught = null;

		try {
			super.authenticate(username);

			rendezvousId = this.getEntityId(rendezvousBean);
			rendezvous = this.rendezvousService.findOne(rendezvousId);

			attendantId = this.getEntityId(attendantBean);
			attendant = this.userService.findOne(attendantId);

			// Rendezvous a mostrar cuando un usuario no esta logeado
			rendezvouses = this.rendezvousService
					.getRendezvousNoDraftNoAdultsOnly();

			for (Rendezvous r : rendezvouses) {
				if (r.equals(rendezvous)) {
					Assert.isTrue(rendezvous.getUsers().contains(attendant));
				}
			}

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * 1. Do the same as an actor who is not authenticated, but register to the
	 * system.
	 */
	// Logeado

	@Test
	public void driverListRendezvousSystemAuth() {

		Object testingData[][] = {

				// Test v�lido:
				{ "user2", "rendezvous3", "user1", null },
				// Test inv�lido: El rendezvous 3 no contiene al user 2, por
				// tanto no puedes ver el perfil
				{ "user1", "rendezvous3", "user2",
						IllegalArgumentException.class },

		};

		for (int i = 0; i < testingData.length; i++) {
			templateListRendezvousSystemAuth((String) testingData[i][0],
					(String) testingData[i][1], (String) testingData[i][2],
					(Class<?>) testingData[i][3]);
		}
	}

	protected void templateListRendezvousSystemAuth(String username,
			String rendezvousBean, String attendantBean, Class<?> expected) {
		Class<?> caught;
		Collection<Rendezvous> rendezvouses;
		Rendezvous rendezvous;
		int rendezvousId;
		int attendantId;
		User attendant;
		caught = null;

		try {
			super.authenticate(username);

			rendezvousId = this.getEntityId(rendezvousBean);
			rendezvous = this.rendezvousService.findOne(rendezvousId);

			attendantId = this.getEntityId(attendantBean);
			attendant = this.userService.findOne(attendantId);

			// Rendezvous a mostrar cuando un usuario esta logeado
			rendezvouses = this.rendezvousService.getRendezvousNoDraft();

			for (Rendezvous r : rendezvouses) {
				if (r.equals(rendezvous)) {
					Assert.isTrue(rendezvous.getUsers().contains(attendant));
				}
			}

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	/* 5. List the rendezvouses that he or shes RSVPd. */

	@Test
	public void driverListRendezvousRSVP() {

		Object testingData[][] = {

				// Test v�lido:
				{ "user1", "rendezvous3", null },
				// Test inv�lido:
				{ "user3", "rendezvous1", IllegalArgumentException.class },
		// // Test inv�lido:
		// { "user1", "rendezvous1", IllegalArgumentException.class },

		};

		for (int i = 0; i < testingData.length; i++) {
			templateListRendezvousRSVP((String) testingData[i][0],
					(String) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void templateListRendezvousRSVP(String username,
			String rendezvousBean, Class<?> expected) {
		Class<?> caught;
		Collection<Rendezvous> rendezvouses;
		Rendezvous rendezvous;

		int rendezvousId;

		User logedUser;
		int logedUserId;

		caught = null;

		try {
			super.authenticate(username);

			// Cogemos al usur logeado
			logedUserId = this.getEntityId(username);
			logedUser = this.userService.findOne(logedUserId);

			rendezvousId = this.getEntityId(rendezvousBean);
			rendezvous = this.rendezvousService.findOne(rendezvousId);

			// Rendezvous a mostrar cuando un usuario esta logeado
			rendezvouses = logedUser.getMyRSVPs();

			// Comprobamos si el rendezvous que le pasamos esta
			// contenido
			Assert.isTrue(rendezvouses.contains(rendezvous));

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * 20. 1. Display information about the users who have RSVPd a rendezvous,
	 * which, in turn, must show their answers to the questions that the creator
	 * has registered
	 */

	@Test
	public void driverDisplayInfoAboutUserWhoHaveRSVPd() {

		Object testingData[][] = {

				// Test v�lido:
				{ null, "rendezvous3", "user1", null },
				// Test inv�lido:
				{ null, "rendezvous1", "user3", IllegalArgumentException.class },
				// Test inv�lido:
				{ null, "rendezvous2", "user2", IllegalArgumentException.class },

		};

		for (int i = 0; i < testingData.length; i++) {
			templateDisplayInfoAboutUserWhoHaveRSVPd(
					(String) testingData[i][0], (String) testingData[i][1],
					(String) testingData[i][2], (Class<?>) testingData[i][3]);
		}
	}

	protected void templateDisplayInfoAboutUserWhoHaveRSVPd(String username,
			String rendezvousBean, String userBean, Class<?> expected) {
		Class<?> caught;
		Rendezvous rendezvous;

		int rendezvousId;

		User user;
		int userId;

		caught = null;

		try {
			super.authenticate(username);

			// Cogemos al usur logeado

			userId = this.getEntityId(userBean);
			user = this.userService.findOne(userId);

			rendezvousId = this.getEntityId(rendezvousBean);
			rendezvous = this.rendezvousService.findOne(rendezvousId);

			Assert.isTrue(rendezvous.getUsers().contains(user));

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * 4. Link one of the rendezvouses that he or shes created to other similar
	 * rendezvouses.
	 */

	@Test
	public void driverLinkOneRendezvousToOther() {

		Object testingData[][] = {

		// Test v�lido: 
		{ "user1", "rendezvous2", "rendezvous3", null },
		// Test inv�lido: 
		{ "user1", "rendezvous5", "rendezvous3",
				IllegalArgumentException.class },
		// Test inv�lido: Rendezvous6 borrado
		{ "user1", "rendezvous2", "rendezvous6",
				IllegalArgumentException.class },
//		// Test inv�lido: User 3 no es creador
//		{ "user3", "rendezvous2", "rendezvous3",
//				IllegalArgumentException.class },
		// Test inv�lido: Link rendezvous no publicado
//		{ "user1", "rendezvous2", "rendezvous",
//					IllegalArgumentException.class },
		};

		for (int i = 0; i < testingData.length; i++) {
			templateLinkOneRendezvousToOther((String) testingData[i][0],
					(String) testingData[i][1], (String) testingData[i][2],
					(Class<?>) testingData[i][3]);
		}
	}

	protected void templateLinkOneRendezvousToOther(String username,
			String rendezvous1Bean, String rendezvous2Bean, Class<?> expected) {
		Class<?> caught;

		Rendezvous rendezvous1;
		Rendezvous rendezvous2;

		caught = null;

		try {
			super.authenticate(username);

			rendezvous1 = this.rendezvousService.findOne(this
					.getEntityId(rendezvous1Bean));
			rendezvous2 = this.rendezvousService.findOne(this
					.getEntityId(rendezvous2Bean));

			List<Rendezvous> similars = (List<Rendezvous>) rendezvous1
					.getLinks();
			similars.add(rendezvous2);

			rendezvous1.setLinks(similars);
			rendezvous1 = rendezvousService.save(rendezvous1);

			Assert.isTrue(rendezvous1.getLinks().contains(rendezvous2));

			this.rendezvousService.flush();

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * 10. An actor who is not authenticated must be able to: 1. List the
	 * rendezvouses in the system grouped by category.
	 */
	@Test
	public void driverListRendezvousNoDraftNoDeletedByCategory() {

		Object testingData[][] = {

				// Test v�lido:
				{ null, "category1", "rendezvous1", null },
				// Test inv�lido: Cuando listamos los rendezvous por esa
				// categoria no esta includido el rendezvous que le pasamos
				{ null, "category2", "rendezvous2",
						IllegalArgumentException.class },

		};

		for (int i = 0; i < testingData.length; i++) {
			templateListRendezvousNoDraftNoDeletedByCategory(
					(String) testingData[i][0], (String) testingData[i][1],
					(String) testingData[i][2], (Class<?>) testingData[i][3]);
		}
	}

	protected void templateListRendezvousNoDraftNoDeletedByCategory(
			String username, String cagtegoryBean, String rendezvousBean,
			Class<?> expected) {
		Class<?> caught;
		Collection<Rendezvous> rendezvouses;
		Rendezvous rendezvous;

		int rendezvousId;

		int categoryId;

		caught = null;

		try {
			super.authenticate(username);

			categoryId = this.getEntityId(cagtegoryBean);

			rendezvousId = this.getEntityId(rendezvousBean);
			rendezvous = this.rendezvousService.findOne(rendezvousId);

			// Rendezvous a mostrar cuando un usuario esta logeado
			rendezvouses = this.rendezvousService
					.getRendezvousNoDraftNoDeletedByCategory(categoryId);

			// Comprobamos si el rendezvous que le pasamos esta
			// contenido
			Assert.isTrue(rendezvouses.contains(rendezvous));

			super.unauthenticate();
		} catch (Throwable e) {
			caught = e.getClass();
		}

		checkExceptions(expected, caught);
	}

}
