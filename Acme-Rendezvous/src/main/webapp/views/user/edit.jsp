<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<form:form action="${requestURI}" modelAttribute="userEditForm">
	
	<security:authorize access="isAnonymous()">
<!-- 		<input type="hidden" name="userAccount.authorities" value="USER"/> -->
		
		<acme:textbox code="user.edit.username" path="userAccount.username" required="required"/>
		<br>
		
		<acme:password code="user.edit.password" path="userAccount.password" required="required"/>
		<br>
	</security:authorize>
		
	
	<acme:textbox code="user.edit.name" path="name" required="required"/>
	<br>
	
	<acme:textbox code="user.edit.surname" path="surname" required="required"/>
	<br>
	
	<acme:textbox code="user.edit.email" path="email" required="required"/>
	<br>
	
	<acme:textbox code="user.edit.phone" path="phone"/>
	<br>
	
	<acme:textbox code="user.edit.address" path="address"/>
	<br>
	
	<acme:textbox code="user.edit.birthday" path="birthday" placeholderCode = "user.edit.placeHolder" required="required"/>
	<br>
	
	<jstl:if test="${requestURI == 'manager/create.do'}">
		<acme:textbox code="user.edit.vat" path="vat" required="required"/>
		<br>
	</jstl:if>
		
	<form:label path="termsAccepted">
			<a href ="welcome/termsAndConditions.do"><spring:message code="user.edit.termsAccepted" /></a>
	</form:label>
	<form:checkbox path="termsAccepted"/>
	<form:errors path="termsAccepted" cssClass="error" />
	<br>
	<br>
	<jstl:if test="${showMessageTerms !=null && showMessageTerms}">
			<p class="error"><spring:message code="user.edit.termsNotAccepted"/></p>
	</jstl:if>
	
	<acme:submit name="save" code="user.edit.save"/>
	<acme:cancel url="" code="user.edit.cancel"/>

</form:form>
