package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ServiceRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Administrator;
import domain.Category;
import domain.Manager;
import domain.Request;
import domain.Service;

@org.springframework.stereotype.Service
@Transactional
public class ServiceService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private ServiceRepository serviceRepository;

	// Supporting services ---------------------------------------------------
	@Autowired
	private ActorService actorService;

	@Autowired
	private ManagerService managerService;

	@Autowired
	private RequestService requestService;

	// Constructor ---------------------------------------------------

	public ServiceService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------
	public Service create() {
		// Comprobamos que el usuario logeado es un manager
		this.checkManager();
		// Si es asi lo cogemos
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Manager manager;
		manager = (Manager) this.actorService.findByUserAccountId(principal
				.getId());

		// Creamos el resultado
		Service result;
		result = new Service();

		// Creamos los atribtutos y relaciones
		boolean cancelled;
		Collection<Category> categories;

		// Inicializamos los atributos
		cancelled = false;
		categories = new ArrayList<Category>();

		// Asignamos atributos
		result.setManager(manager);
		result.setCancelled(cancelled);
		result.setCategories(categories);

		// Devolvemos resultado
		return result;
	}

	public Service findOne(int serviceId) {
		// Comprobamos que el objeto que le pasamos no es nulo
		Assert.isTrue(serviceId != 0);
		// Creamos el objeto a devolver
		Service result;
		// Cogemos del repositorio el rendezvous que le pasamos como parametro
		result = this.serviceRepository.findOne(serviceId);
		// Devolvemos el resultado
		return result;
	}

	public Collection<Service> findAll() {
		// Creamos el objeto a devolver
		Collection<Service> result;
		// Cogemos del repositorio los services
		result = this.serviceRepository.findAll();
		// Devolvemos el resultado
		return result;
	}

	public Service save(Service service) {
		// Comprobamos que el servicio que le pasamos no es nulo
		Assert.notNull(service);

		// Comprobamos que el usuario logeado es un manager
		this.checkManager();

		// Comprobamos que no esta cancelado
		Assert.isTrue(!service.isCancelled());

		// Si es un manager lo cogemos
		UserAccount principal;
		principal = LoginService.getPrincipal();
		Manager manager;

		manager = (Manager) this.actorService.findByUserAccountId(principal
				.getId());

		// Comprobamos que el manager es el mismo que el que esta logeado
		Assert.isTrue(service.getManager().equals(manager));

		// Si es la primera vez, al manager le a�ado el servicio que quiero
		// guardar
		if (service.getId() == 0) {
			manager.getServices().add(service);
			this.managerService.save(manager);
		}

		// Creamos el objeto a devolver
		Service result;

		// Actualizamos el servicio
		result = this.serviceRepository.save(service);

		// Devolvemos el resultado
		return result;
	}

	public void delete(Service service) {
		// Comprobamos que el service pasado como parametro no es nulo
		Assert.notNull(service);

		// Comprobamos que el usuario logeado es un manager
		this.checkManager();

		// Borrar un servicio cancelado
		Assert.isTrue(!service.isCancelled());

		// Si es asi lo cogemos
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Manager manager;
		manager = (Manager) this.actorService.findByUserAccountId(principal
				.getId());

		// Comprobamos que el manager es el mismo que quiere eliminar el service
		Assert.isTrue(manager.equals(service.getManager()));

		// Actualizamos referencias
		// Manager, no hace falta por que al ser bidireccional spring se encarga
		// manager.getServices().remove(service);
		// this.managerService.save(manager);

		// Request
		Collection<Request> requests;
		requests = this.serviceRepository.getRequestsOfService(service.getId());

		for (Request r : requests) {
			// Eliminamos esa request
			this.requestService.findAll().remove(r);
			this.requestService.save(r);
		}

		// Service
		this.serviceRepository.delete(service);

	}

	// Other methods----------------------------------------------------------
	private void checkManager() {
		// Comprueba que el usuario logeado es un user
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Actor actor;
		actor = this.actorService.findByUserAccountId(principal.getId());
		Assert.isInstanceOf(Manager.class, actor);
	}

	private void checkAdmin() {
		// Comprueba que el usuario logeado es un user
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Actor actor;
		actor = this.actorService.findByUserAccountId(principal.getId());
		Assert.isInstanceOf(Administrator.class, actor);
	}

	// Metodo que sea cancelar, el unico que puede cancelar es admin
	public void cancel(Service service) {
		// Comprobamos que el servicio tenga id distinto de 0
		Assert.isTrue(service.getId() != 0);

		// Comprobamos si no esta cancelado
		Assert.isTrue(!service.isCancelled());

		// Comprobamos que el usuario logeado es un admin
		this.checkAdmin();

		// Cambiamos el atributo cancelled a true
		service.setCancelled(true);

		this.serviceRepository.save(service);
	}

	public Collection<Request> getRequestsOfService(Integer serviceId) {
		Collection<Request> result;

		result = serviceRepository.getRequestsOfService(serviceId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Service> findServicesWithCategory(Integer categoryId) {
		Collection<Service> result;

		result = serviceRepository.findServicesWithCategory(categoryId);
		Assert.notNull(result);

		return result;
	}

	public void removeCategoryFromServices(Category category) { // Borrar una
																// categor�a de
																// todos los
																// servicios
		Collection<domain.Service> servicesWithCategory;

		servicesWithCategory = serviceRepository
				.findServicesWithCategory(category.getId());

		Assert.notNull(servicesWithCategory);

		for (Service service : servicesWithCategory) {
			service.getCategories().remove(category);
		}

		serviceRepository.save(servicesWithCategory);
	}

	@Autowired
	private Validator validator;

	public Service reconstruct(Service service, BindingResult binding) {
		if(service.getCategories() == null){
			service.setCategories(new ArrayList<Category>());
		}

		if (service.getId() == 0) {
			UserAccount principal;
			principal = LoginService.getPrincipal();
			
			Manager manager;
			manager = (Manager) this.actorService.findByUserAccountId(principal.getId());
			
			service.setManager(manager);
			service.setCancelled(false);
		} else {
			Service _service;

			_service = findOne(service.getId());
			
			service.setManager(_service.getManager());
			service.setCancelled(_service.isCancelled());
			service.setVersion(_service.getVersion());
		}
		
		while(service.getCategories().remove(null));
		
		validator.validate(service, binding);

		return service;
	}

	public void flush() {
		serviceRepository.flush();
	}

	public Collection<Service> getServicesByRendezvous(int rendezvousId) {
		Assert.isTrue(rendezvousId != 0);

		Collection<Service> result;

		result = serviceRepository.getServicesByRendezvous(rendezvousId);

		return result;
	}
}
