package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ConfigurationRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Configuration;

@Service
@Transactional
public class ConfigurationService {

	@Autowired
	private ConfigurationRepository configurationRepository;
	
	public ConfigurationService() {
		super();
	}
	
	public Configuration findConfiguration() {
		Configuration result;
		
		result = configurationRepository.findConfiguration();
		
		return result;		
	}
	
	public Configuration save(Configuration configuration) {
		Assert.notNull(configuration);
		
		Configuration result;
		Authority auth;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);
		
		Assert.isTrue(principal.getAuthorities().contains(auth));
		
		result = configurationRepository.save(configuration);
		
		return result;
	}
	
	public void flush(){
		configurationRepository.flush();
	}

	
	
	
}
