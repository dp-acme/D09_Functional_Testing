package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class UserServiceTest extends AbstractTest{

	@Autowired
	private UserService userService;
	
/*
 	1. The system must support two kinds of actors, namely: administrators and users. 
 	It must store the following information regarding them: their names, surnames, 
 	postal addresses (optional), phone numbers (optional), and email addresses.
	
	4. An actor who is not authenticated must be able to:
		1. Register to the system as a user.
*/
	@Test
	public void driverCreate(){
		Calendar cal;
		Calendar futureCal;
		
		cal = Calendar.getInstance();
		futureCal = Calendar.getInstance();
		cal.set(1998, 11, 19);
		futureCal.set(2030, 11, 12);
		Object[][] testingData = {
				
				//Positivos
				{null, "userTest", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", cal.getTime(), null},
				{null, "userTest1", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", cal.getTime(), null},
				
				//Negativos

				{null, "user", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", cal.getTime(), ConstraintViolationException.class}, 								// Nombre de usuario (username) demasiado corto (menos de 4 caracteres)
				{null, "user1234123412341234123412341234", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", cal.getTime(), ConstraintViolationException.class},		// Nombre de usuario (username) demasiado largo (m�s de 32 caracteres)
				{null, "userTest2", "pass", "nameTest", "surnameTest", "mail@test.test", "", "", cal.getTime(), ConstraintViolationException.class},									// Contrase�a (password) demasiado corta (menos de 4 caracteres)
				{null, "userTest2", "pass1234123412341234123412341234", "nameTest", "surnameTest", "mail@test.test", "", "", cal.getTime(), ConstraintViolationException.class},		// Contrase�a (password) demasiado larga (m�s de 32 caracteres)
				{null, "userTest2", "passwordTest", "", "surnameTest", "mail@test.test", "", "", cal.getTime(), ConstraintViolationException.class},									// Nombre(name) en blanco
				{null, "userTest2", "passwordTest", "nameTest", "", "mail@test.test", "", "", cal.getTime(), ConstraintViolationException.class},										// Apellido/s (surname) en blanco
				{null, "userTest2", "passwordTest", "nameTest", "surnameTest", "", "", "", cal.getTime(), ConstraintViolationException.class},											// Correo electr�nico (email) en blanco
				{null, "userTest2", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", futureCal.getTime(), ConstraintViolationException.class},						// Fecha de nacimiento (birthday) futura
				{null, "user1", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "", "", cal.getTime(), ConstraintViolationException.class},								// Nombre de usuario (username) ya registrado
				{"admin", "userTest3", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", cal.getTime(), ConstraintViolationException.class},		// Intento de registro ya logueado como admin
				{"user1", "userTest3", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", cal.getTime(), ConstraintViolationException.class},		// Intento de registro ya logueado como user
				{"manager1", "userTest3", "passwordTest", "nameTest", "surnameTest", "mail@test.test", "611111111", "addressTest", cal.getTime(), ConstraintViolationException.class}	// Intento de registro ya logueado como manager
				
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2],
						 (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5],
						 (String) testingData[i][6], (String) testingData[i][7], (Date) testingData[i][8],(Class<?>) testingData[i][9]);
		}
	}
/*
 4. An actor who is not authenticated must be able to:
		2. List the users of the system and navigate to their profiles, 
			which include personal data and the list of rendezvouses 
			that they've attended or are going to attend.
 */
	@Test
	public void driverFindUsers(){
		Object[][] testingData = {
				{"manager1", null},		//Listar como manager
				{"admin", null},		//Listar como admin
				{"user1", null},		//Listar como user
				{null, null},			//Listar como no autenticado
		};
		
		for(int i = 0; i<testingData.length; i++){
			findUsers((String) testingData[i][0],(Class<?>) testingData[i][1]);
		}
	}
	protected void findUsers(String username, Class<?> expected){
		Class<?>  caught;
		caught=null;
		
		try{
			super.authenticate(username);
			userService.findAll();
			super.unauthenticate();
		}catch (Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void templateCreate(String username, String usernameToBeSetted, String password, String name,String surname, String email, String phone, String address, Date birthday, Class<?> expected){
		Class<?>  caught;
		
		User user;
		UserAccount userAccount;
		Collection<Authority> auths;
		Authority auth;
	
		auth = new Authority();
		auths = new ArrayList<Authority>();
		auth.setAuthority(Authority.USER);
		auths.add(auth);
		userAccount = new UserAccount();
		caught = null;
		
		try{			
			super.authenticate(username);
			
			user = userService.create();
			Assert.notNull(user);

			user.setName(name);
			user.setSurname(surname);
			user.setEmail(email);
			user.setPhone(phone);
			user.setAddress(address);
			user.setBirthday(birthday);
			userAccount.setUsername(usernameToBeSetted);
			userAccount.setPassword(password);
			userAccount.setAuthorities(auths);
			user.setUserAccount(userAccount);
				
			userService.save(user);
			userService.flush();
			
			super.unauthenticate();	
			
		}catch (Throwable e) {
			caught = e.getClass();
		}
		
		checkExceptions(expected, caught);
	}	
}
