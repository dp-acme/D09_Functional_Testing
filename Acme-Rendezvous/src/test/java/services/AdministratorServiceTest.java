package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import utilities.AbstractTest;

import com.mchange.v1.util.UnexpectedException;

import domain.Manager;
import domain.Rendezvous;
import domain.Service;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class AdministratorServiceTest extends AbstractTest{

	@Autowired
	private AdministratorService administratorService;
	
	@Autowired
	private RendezvousService rendezvousService;
	
	@Autowired
	private ServiceService serviceService;

	@Autowired
	private ManagerService managerService;
	
/* ACME RENDEZVOUS	
  6. An actor who is authenticated as an administrator must be able to: 
  		3. Display a dashboard with the following information: 
  			-The average and the standard deviation of rendezvouses created per user. 
*/
	@Test
	public void testAvgSdRendezvousPerUser(){
		avgSdRendezvousPerUserTest("admin",null);
		avgSdRendezvousPerUserTest(null,IllegalArgumentException.class); 		//	El usuario no est� logueado
		avgSdRendezvousPerUserTest("user1",IllegalArgumentException.class);		// 	El usuario es un user
		avgSdRendezvousPerUserTest("manager1",IllegalArgumentException.class); 	//	El usuario es un manager
	}
	
/*		-The ratio of users who have ever created a rendezvous versus the users who have never created any rendezvouses.  		
*/
	@Test
	public void ratioCreatorsOverNonCreatorsTest(){
		ratioCreatorsOverNonCreatorsTest("admin",null);
		ratioCreatorsOverNonCreatorsTest(null,IllegalArgumentException.class);			//	El usuario no est� logueado
		ratioCreatorsOverNonCreatorsTest("user1",IllegalArgumentException.class);		// 	El usuario es un user
		ratioCreatorsOverNonCreatorsTest("manager1",IllegalArgumentException.class); 	//	El usuario es un manager
	}
	
/*		-The average and the standard deviation of users per rendezvous.  		
*/
	@Test
	public void avgSdUsersPerRendezvousTest(){
		avgSdUsersPerRendezvousTest("admin",null);
		avgSdUsersPerRendezvousTest(null,IllegalArgumentException.class);			//	El usuario no est� logueado
		avgSdUsersPerRendezvousTest("user1",IllegalArgumentException.class);		//	El usuario es un user
		avgSdUsersPerRendezvousTest("manager1",IllegalArgumentException.class); 	//	El usuario es un manager
	}
	
/*		-The average and the standard deviation of rendezvouses that are RSVPd per user. 		
*/
	@Test
	public void avgSdRSVPsPerUserTest(){
		avgSdRSVPsPerUserTest("admin",null);
		avgSdRSVPsPerUserTest(null,IllegalArgumentException.class);			//	El usuario no est� logueado
		avgSdRSVPsPerUserTest("user1",IllegalArgumentException.class);		// 	El usuario es un user
		avgSdRSVPsPerUserTest("manager1",IllegalArgumentException.class); 	//	El usuario es un manager
	}
	
/*	17. An actor who is authenticated as an administrator must be able to: 	
 		2. Display a dashboard with the following information:
			-The average and the standard deviation of announcements per rendezvous. 		
*/
	@Test
	public void avgSdAnnoucementsPerRendezvousTest(){
		avgSdAnnoucementsPerRendezvousTest("admin",null);
		avgSdAnnoucementsPerRendezvousTest(null,IllegalArgumentException.class);			//	El usuario no est� logueado
		avgSdAnnoucementsPerRendezvousTest("user1",IllegalArgumentException.class);			// 	El usuario es un user
		avgSdAnnoucementsPerRendezvousTest("manager1",IllegalArgumentException.class); 		//	El usuario es un manager
	}
	
/*	22. An actor who is authenticated as an administrator must be able to:
		1. Display a dashboard with the following information		
 			-The average and the standard deviation of the number of questions per rendezvous		
*/
	@Test
	public void avgSdQuestionsPerRendezvousTest(){
		avgSdQuestionsPerRendezvousTest("admin",null);
		avgSdQuestionsPerRendezvousTest(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		avgSdQuestionsPerRendezvousTest("user1",IllegalArgumentException.class);			// 	El usuario es un user
		avgSdQuestionsPerRendezvousTest("manager1",IllegalArgumentException.class); 		//	El usuario es un manager
	}

/*		-The average and the standard deviation of replies per comment.	
*/	
	@Test
	public void avgSdRepliesPerCommentTest(){
		avgSdRepliesPerCommentTest("admin",null);
		avgSdRepliesPerCommentTest(null,IllegalArgumentException.class);			//	El usuario no est� logueado
		avgSdRepliesPerCommentTest("user1",IllegalArgumentException.class);			// 	El usuario es un user
		avgSdRepliesPerCommentTest("manager1",IllegalArgumentException.class); 		//	El usuario es un manager
	}

/*		-The average and the standard deviation of the number of answers to the questions per rendezvous	
*/	
	@Test
	public void avgSdAnswersPerRendezvous(){
		avgSdAnswersPerRendezvous("admin",null);
		avgSdAnswersPerRendezvous(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		avgSdAnswersPerRendezvous("user1",IllegalArgumentException.class);			// 	El usuario es un user
		avgSdAnswersPerRendezvous("manager1",IllegalArgumentException.class); 		//	El usuario es un manager
	}
	
/* ACME RENDEZVOUS 2.0		
   11. An actor who is authenticated as an administrator must be able to: 
 		2. Display a dashboard with the following information: 
			- The average ratio of services in each category
*/	
	@Test
	public void avgServicesPerCategory(){
		avgServicesPerCategory("admin",null);
		avgServicesPerCategory(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		avgServicesPerCategory("user1",IllegalArgumentException.class);				// 	El usuario es un user
		avgServicesPerCategory("manager1",IllegalArgumentException.class); 			//	El usuario es un manager
	}

/*		-The average, the minimum, the maximum, and the standard deviation of services requested per rendezvous. 
*/
	@Test
	public void avgMaxMinSdRequestsPerRendezvous(){
		avgMaxMinSdRequestsPerRendezvous("admin",null);
		avgMaxMinSdRequestsPerRendezvous(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		avgMaxMinSdRequestsPerRendezvous("user1",IllegalArgumentException.class);				// 	El usuario es un user
		avgMaxMinSdRequestsPerRendezvous("manager1",IllegalArgumentException.class); 			//	El usuario es un manager
	}

/*		-The average number of categories per rendezvous. 
*/
	@Test
	public void avgCategoriesPerRendezvous(){
		avgCategoriesPerRendezvous("admin",null);
		avgCategoriesPerRendezvous(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		avgCategoriesPerRendezvous("user1",IllegalArgumentException.class);					// 	El usuario es un user
		avgCategoriesPerRendezvous("manager1",IllegalArgumentException.class); 				//	El usuario es un manager
	}

/* ACME RENDEZVOUS	
   6. An actor who is authenticated as an administrator must be able to: 
 		3. Display a dashboard with the following information:
			- The top-10 rendezvouses in terms of users who have RSVPd them.
*/	
	@Test
	public void get10RendezvousesOrderedByUsers(){
		get10RendezvousesOrderedByUsers("admin",null);
		get10RendezvousesOrderedByUsers(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		get10RendezvousesOrderedByUsers("user1",IllegalArgumentException.class);				// 	El usuario es un user
		get10RendezvousesOrderedByUsers("manager1",IllegalArgumentException.class); 			//	El usuario es un manager
	}
	

/*	17. An actor who is authenticated as an administrator must be able to: 	
 		2. Display a dashboard with the following information: 
 			-The rendezvouses that whose number of announcements is above 75% the average number of announcements per rendezvous. 
 */
	@Test
	public void getRendezvousWithOver75percentAnnoucements(){
		getRendezvousWithOver75percentAnnoucements("admin",null);
		getRendezvousWithOver75percentAnnoucements(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		getRendezvousWithOver75percentAnnoucements("user1",IllegalArgumentException.class);				// 	El usuario es un user
		getRendezvousWithOver75percentAnnoucements("manager1",IllegalArgumentException.class); 			//	El usuario es un manager
	}
	
/*		-The rendezvouses that are linked to a number of rendezvouses that is greater than the average plus 10%.
 */
	@Test
	public void getRendezvousLinkedOver10percentRendezvouses(){
		getRendezvousLinkedOver10percentRendezvouses("admin",null);
		getRendezvousLinkedOver10percentRendezvouses(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		getRendezvousLinkedOver10percentRendezvouses("user1",IllegalArgumentException.class);			// 	El usuario es un user
		getRendezvousLinkedOver10percentRendezvouses("manager1",IllegalArgumentException.class); 		//	El usuario es un manager
	}

/*	ACME RENDEZVOUS 2.0
   6. An actor who is authenticated as an administrator must be able to: 
		2. Display a dashboard with the following information: 
			-The best-selling services. 
*/
	@Test
	public void getBestSellingServices(){
		getBestSellingServices("admin",null);
		getBestSellingServices(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		getBestSellingServices("user1",IllegalArgumentException.class);				// 	El usuario es un user
		getBestSellingServices("manager1",IllegalArgumentException.class); 			//	El usuario es un manager
	}
	
/* 		-The managers who provide more services than the average
 */
	@Test
	public void getManagersWithMoreServicesThanAvg(){
		getManagersWithMoreServicesThanAvg("admin",null);
		getManagersWithMoreServicesThanAvg(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		getManagersWithMoreServicesThanAvg("user1",IllegalArgumentException.class);				// 	El usuario es un user
		getManagersWithMoreServicesThanAvg("manager1",IllegalArgumentException.class); 			//	El usuario es un manager
	}
	
/* 		-The managers who have got more services cancelled
 */
	@Test
	public void getManagersWithMoreServicesCancelled(){
		getManagersWithMoreServicesCancelled("admin",null);
		getManagersWithMoreServicesCancelled(null,IllegalArgumentException.class);				//	El usuario no est� logueado
		getManagersWithMoreServicesCancelled("user1",IllegalArgumentException.class);			// 	El usuario es un user
		getManagersWithMoreServicesCancelled("manager1",IllegalArgumentException.class); 		//	El usuario es un manager
	}


/* 	11. An actor who is authenticated as an administrator must be able to:
  		2. Display a dashboard with the following information: 
 			- The top-selling services. 
 */
	
	@Test
	public void getTopSellingServices(){
		getTopSellingServices("admin",null);
		getTopSellingServices(null,IllegalArgumentException.class);					//	El usuario no est� logueado
		getTopSellingServices("user1",IllegalArgumentException.class);				// 	El usuario es un user
		getTopSellingServices("manager1",IllegalArgumentException.class); 			//	El usuario es un manager
	}
	
	protected void avgSdRendezvousPerUserTest(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgSdRendezvousPerUser();
			expectedDouble = new Double[] {2.0,0.8164965805194777};
			
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void ratioCreatorsOverNonCreatorsTest(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.ratioCreatorsOverNonCreators();
			expectedDouble = -1.0;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgSdUsersPerRendezvousTest(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate("admin");
			
			testingData = administratorService.getAvgSdUsersPerRendezvous();
			expectedDouble = new Double[] {0.5,0.5};
			
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(null, caught);
	}
	

	protected void avgSdRSVPsPerUserTest(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgSdRSVPsPerUser();
			expectedDouble = new Double[] {1.0,0.8164965805194777 };
			
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgSdAnnoucementsPerRendezvousTest(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgSdAnnoucementsPerRendezvous();
			expectedDouble = new Double[] {0.8333,0.8975274681652429 };
			
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgSdQuestionsPerRendezvousTest(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgSdQuestionsPerRendezvous();
			expectedDouble = new Double[] {0.8333,0.6871842708554315 };
			
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgSdRepliesPerCommentTest(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgSdRepliesPerComment();
			expectedDouble = new Double[] {0.4286,0.49487165922291493 };
			
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgSdAnswersPerRendezvous(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgSdAnswersPerRendezvous();
			expectedDouble = new Double[] {0.3333,0.47140452067318056 };
			
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}

	protected void avgServicesPerCategory(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgServicesPerCategory();
			expectedDouble = 1.0;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgMaxMinSdRequestsPerRendezvous(String username, Class<?> expected){
		Class<?> caught;
		Double[] testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgMaxMinSdRequestsPerRendezvous();
			expectedDouble = new Double[] {0.5,0.,2.,0.7637626156077555};
			
			for(int i = 0; i<testingData.length; i++){
				compareDoubles(testingData [i], expectedDouble[i]);
			}
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void avgCategoriesPerRendezvous(String username, Class<?> expected){
		Class<?> caught;
		Double testingData, expectedDouble;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getAvgCategoriesPerRendezvous();
			expectedDouble = 1.1667;
			compareDoubles(testingData, expectedDouble);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
	}
	
	protected void get10RendezvousesOrderedByUsers(String username, Class<?> expected){
		Class<?> caught;
		Collection<?> testingData; 
		Collection<Rendezvous> expectedCollection;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.get10RendezvousesOrderedByUsers();
			expectedCollection = new ArrayList<Rendezvous>();
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous3")));
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous5")));
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous6")));
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous1")));
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous2")));
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous4")));


			compareCollections(testingData, expectedCollection, true);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void getRendezvousWithOver75percentAnnoucements(String username, Class<?> expected){
		Class<?> caught;
		Collection<?> testingData; 
		Collection<Rendezvous> expectedCollection;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getRendezvousWithOver75percentAnnoucements();
			expectedCollection = new ArrayList<Rendezvous>();
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous1")));
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous3")));
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous5")));

			compareCollections(testingData, expectedCollection, false);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void getRendezvousLinkedOver10percentRendezvouses(String username, Class<?> expected){
		Class<?> caught;
		Collection<?> testingData; 
		Collection<Rendezvous> expectedCollection;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getRendezvousLinkedOver10percentRendezvouses();
			expectedCollection = new ArrayList<Rendezvous>();
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous2")));
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous4")));
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous5")));
			expectedCollection.add(rendezvousService.findOne(super.getEntityId("rendezvous6")));


			compareCollections(testingData, expectedCollection, false);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void getBestSellingServices(String username, Class<?> expected){
		Class<?> caught;
		Collection<?> testingData; 
		Collection<Service> expectedCollection;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getBestSellingServices();
			expectedCollection = new ArrayList<Service>();
			expectedCollection.add(serviceService.findOne(super.getEntityId("service1")));

			compareCollections(testingData, expectedCollection, false);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void getTopSellingServices(String username, Class<?> expected){
		Class<?> caught;
		Collection<?> testingData; 
		Collection<Service> expectedCollection;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getTopSellingServices();
			expectedCollection = new ArrayList<Service>();
			expectedCollection.add(serviceService.findOne(super.getEntityId("service1")));
			expectedCollection.add(serviceService.findOne(super.getEntityId("service2")));
			expectedCollection.add(serviceService.findOne(super.getEntityId("service3")));

			compareCollections(testingData, expectedCollection, true);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void getManagersWithMoreServicesThanAvg(String username, Class<?> expected){
		Class<?> caught;
		Collection<?> testingData; 
		Collection<Manager> expectedCollection;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getManagersWithMoreServicesThanAvg();
			expectedCollection = new ArrayList<Manager>();
			expectedCollection.add(managerService.findOne(super.getEntityId("manager1")));

			compareCollections(testingData, expectedCollection, false);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	protected void getManagersWithMoreServicesCancelled(String username, Class<?> expected){
		Class<?> caught;
		Collection<?> testingData; 
		Collection<Manager> expectedCollection;
		
		caught = null;
		try{
			super.authenticate(username);
			
			testingData = administratorService.getManagersWithMoreServicesCancelled();
			expectedCollection = new ArrayList<Manager>();
			expectedCollection.add(managerService.findOne(super.getEntityId("manager1")));
			expectedCollection.add(managerService.findOne(super.getEntityId("manager2")));
			expectedCollection.add(managerService.findOne(super.getEntityId("manager3")));


			compareCollections(testingData, expectedCollection, true);
			
			super.unauthenticate();
			
		}catch(Throwable e) {
			caught = e.getClass();
		}
		checkExceptions(expected, caught);
		
	}
	
	
	
	
	protected void compareCollections(Collection<?> queryResult, Collection<?> expectedCollection, Boolean ordered){

		if(!(queryResult.containsAll(expectedCollection)&&queryResult.containsAll(expectedCollection))){
			throw new UnexpectedException();
		}
		if(ordered){
			Iterator<?> queryResultIt;
			
			queryResultIt = queryResult.iterator();
			for(Object obj: expectedCollection){
				if(!obj.equals(queryResultIt.next()))
					throw new UnexpectedException();
				
			}
		}
	}
	
	protected void compareDoubles(Double queryResult, Double expectedDouble){

		if(!queryResult.equals(expectedDouble)){
			throw new UnexpectedException();
		}
	}
}
